import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ErrorHandler } from '@angular/core';

import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';

import { SplashScreen } from '@ionic-native/splash-screen';

import { IonicStorageModule } from '@ionic/storage';

import { MyApp } from './app.component';

import {SettingsService} from "../providers/settings.service";
import {AuthService} from "../providers/auth.service";
import {HomeService} from "../providers/home.service";
import {CountryService} from "../providers/country.service";
import {Clipboard} from "@ionic-native/clipboard";
import {DatePipe} from "@angular/common";
import {Network} from "@ionic-native/network";
import {NativeGeocoder} from "@ionic-native/native-geocoder";
import { Geolocation } from '@ionic-native/geolocation';
import {AgmCoreModule, GoogleMapsAPIWrapper} from "@agm/core";
import {ProfileService} from "../providers/profile.service";
import {VUploaderModule} from "../v-uploader/v-uploader.module";
import {SocketService} from "../providers/socket.service";
import {HttpErrorHandlerService} from "../providers/http-error-handler.service";
import {NotesService} from "../providers/notes.service";
import {ConversationService} from "../providers/conversation.service";
import {ContentService} from "../providers/content.service";
import {LoadingService} from "../providers/loading.service";
import {Keyboard} from "@ionic-native/keyboard";
import {Globalization} from "@ionic-native/globalization";
import {Spherical} from "@ionic-native/google-maps";
import {OrganizationService} from "../providers/organization.service";
import {OfficeService} from "../providers/office.service";
import {CategoryService} from "../providers/category.service";
import {PaginationService} from "../providers/pagination.service";
import {VRatingModule} from "../v-rating/v-rating.module";
import {RatingService} from "../providers/rating.service";
import {FieldPermissionsService} from "../providers/field-permissions.service";
import {HelpersService} from "../providers/helpers.service";
import {FileHelperService} from "../providers/file-helper.service";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {TokenInterceptor} from "../interceptors/token.interceptor";
import {LocationHelperService} from "../providers/location-helper.service";
import {ServiceMatchService} from "../providers/service-match.service";
import {NotificationService} from "../providers/notification.service";
import {ContentUpdateService} from "../providers/content-update.service";
import {LocalNotifications} from "@ionic-native/local-notifications";
import { ReminderService } from '../providers/reminder-service';
import {OneSignal} from "@ionic-native/onesignal";
import {Diagnostic} from "@ionic-native/diagnostic";
import {LocationAccuracy} from "@ionic-native/location-accuracy";
import { Deeplinks } from '@ionic-native/deeplinks';
import {SocialSharing} from "@ionic-native/social-sharing";
import {LocalConversationService} from "../providers/local-conversation.service";
import {AttachmentService} from "../providers/attachment.service";
import {File} from "@ionic-native/file";
import {FileTransfer} from "@ionic-native/file-transfer";
import {PipesModule} from "../pipes/pipes.module";
import {ApiSettingsService} from "../providers/api-settings.service";
import {CommentService} from "../providers/comment.service";
import {ConfirmationService} from "../providers/confirmation.service";
@NgModule({
  declarations: [
    MyApp,
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    PipesModule,
    IonicStorageModule.forRoot(),
    VUploaderModule,
    VRatingModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBYYNMn5BbKMbHoQfLc1cRLjSsEyvwYMJg',
      libraries: ["places"]
    }),
    IonicModule.forRoot(MyApp,{tabsHideOnSubPages: true}),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true},
    Deeplinks,
    Network,
    Diagnostic,
    LocationAccuracy,
    NativeGeocoder,
    Geolocation,
    Globalization,
    GoogleMapsAPIWrapper,
    Keyboard,
    SplashScreen,
    SettingsService,
    ApiSettingsService,
    LocalNotifications,
    OneSignal,
    NotificationService,
    ReminderService,
    AuthService,
    FileHelperService,
    HttpErrorHandlerService,
    NotesService,
    HomeService,
    CategoryService,
    PaginationService,
    ContentService,
    ContentUpdateService,
    FieldPermissionsService,
    OrganizationService,
    OfficeService,
    RatingService,
    Spherical,
    CountryService,
    Clipboard,
    DatePipe,
    HelpersService,
    ProfileService,
    NativeGeocoder,
    File,
    FileTransfer,
    AttachmentService,
    ConversationService,
    LocalConversationService,
    SocketService,
    LoadingService,
    LocationHelperService,
    ServiceMatchService,
    SocialSharing,
    CommentService,
    ConfirmationService,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
  ]
})
export class AppModule { }
