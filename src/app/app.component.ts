import {Component, ViewChild, ChangeDetectorRef, OnInit} from '@angular/core';

import {Events, MenuController, Nav, Platform, App, ModalController} from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';

import { Storage } from '@ionic/storage';
import {SettingsService} from "../providers/settings.service";
import {AuthService} from "../providers/auth.service";
import {CountryService} from "../providers/country.service";
import {File} from "@ionic-native/file";
import {Diagnostic} from "@ionic-native/diagnostic";
import {SocketService} from "../providers/socket.service";
import {ProfileService} from "../providers/profile.service";
import {ContentService} from "../providers/content.service";
import {CategoryService} from "../providers/category.service";
import {Network} from "@ionic-native/network";
import {NotesService} from "../providers/notes.service";
import {LocationHelperService} from "../providers/location-helper.service";
import {ServiceMatchService} from "../providers/service-match.service";
import {HelpersService} from "../providers/helpers.service";
import {IContentType} from "../interfaces/IContentType";
import {ITab} from "../interfaces/ITab";
import {HomePage} from "../pages/home/home";
import {ContentIndexPage} from "../pages/content/index";
import {Subject} from "rxjs/Subject";
import {Observable} from "rxjs/Observable";
import {LoadingService} from "../providers/loading.service";
import {NotificationService} from "../providers/notification.service";
import {INotification} from "../interfaces/INotification";
import {Globalization} from "@ionic-native/globalization";
import {ConversationService} from "../providers/conversation.service";
import {Deeplinks} from "@ionic-native/deeplinks";
import {ProfileViewPage} from "../pages/profile/view/profile-view";
import {ConversationViewPage} from "../pages/conversation/view/conversation-view";
import {LocalConversationService} from "../providers/local-conversation.service";
import {IRole} from "../interfaces/IRole";
import {IMembershipType} from "../interfaces/IMembershipType";
import {IApiSettings} from "../interfaces/IApiSettings";
import {ApiSettingsService} from "../providers/api-settings.service";
import {IContentUpdate} from "../interfaces/IContentUpdate";
import {ContentUpdateService} from "../providers/content-update.service";
import {IConfirmation} from "../interfaces/IConfirmation";
import {IProfile} from "../interfaces/IProfile";
export interface PageInterface {
  title: string;
  name: string;
  icon: string;
  defaultParam?:string
  logsOut?: boolean;
  home?:boolean;
  index?: number;
  tabName?: string;
  tabComponent?: any;
}
export interface IDeepLink {
  path: string;
  args: any;
}
@Component({
  templateUrl: 'app.html',
  providers: [File, Diagnostic]
})
export class MyApp implements OnInit{
  @ViewChild(Nav) nav: Nav;
  appPages: PageInterface[] = [];
  loggedInPages: PageInterface[] = [
    { title: 'Profile', name: 'ProfileViewPage', icon: 'person'},
    { title: 'Conversations', name: 'ConversationIndexPage', icon: 'chatboxes'},
    { title: 'Organizations', name: 'OrganizationIndexPage', icon: 'briefcase'},
    { title: 'Reminders', name: 'RemindersPage', icon: 'alarm'},
  ];
  loggedOutPages: PageInterface[] = [
    { title: 'Login', name: 'LoginPage', icon: 'home',home: true},
  ];
  rootPage: string;
  loginIsActive:boolean = false;
  local_notification: boolean = false;
  opened_conversation: number = 0;
  toOpen: IDeepLink;
  constructor(
    public events: Events,
    public menu: MenuController,
    public platform: Platform,
    public storage: Storage,
    private file:File,
    public splashScreen: SplashScreen,
    public settings:SettingsService,
    public app:App,
    public country_ser:CountryService,
    private diagnostic: Diagnostic,
    public auth_ser: AuthService,
    public socket_ser:SocketService,
    private ref: ChangeDetectorRef,
    private location_ser:LocationHelperService,
    private profile_ser:ProfileService,
    private content_ser:ContentService,
    private category_ser:CategoryService,
    private network:Network,
    private note_ser:NotesService,
    private match_ser:ServiceMatchService,
    private helpers: HelpersService,
    private loading:LoadingService,
    private not_ser:NotificationService,
    private glob:Globalization,
    private conv_ser:ConversationService,
    private local_conv:LocalConversationService,
    private deeplinks: Deeplinks,
    private api_settings:ApiSettingsService,
    private con_up_ser:ContentUpdateService,
    private modalCtrl:ModalController,
  ) {}
  ngOnInit(){
    this.splashScreen.show();
    this.initApp().catch(console.log);
    this.listenToEvents().catch(console.log);
  }
  private async initApp() {
    await this.platform.ready().catch(e => console.log(e));
    //todo: add arabic support for entire app
    this.listenToLinks();
    //this.platform.setDir('rtl',true);
    // this.settings.getLanguage().then(lang => {
    //   console.log(lang);
    // },err => {
    //   console.log(err);
    // });
    this.network.onchange().subscribe(event => {
      if(event.type === 'online'){
        this.note_ser.showNote('Back online');
      }else if(event.type === 'offline'){
        this.note_ser.showNote("You're offline");
      }
    });
    const per_initialized = await this.storage.get('hasInitPermissions').catch(console.log);
    if(!per_initialized){
      this.rootPage = "PermissionsPage";
    }else{
      const logged_in = await this.auth_ser.hasLoggedIn().catch(console.log);
      if(logged_in){
        this.startApp().catch(console.log);
      }else{
        this.rootPage = "LoginPage";
      }
    }
    this.splashScreen.hide();
  }
  private async startApp(){
    let profile = <IProfile>{};
    let loop = 0;
    do{
      loop += 1;
      console.log('profile get loop:', loop);
      profile = <IProfile> await this.auth_ser.getCurrentProfile().catch(console.log);
    }while(!profile);
    console.log('profile:', profile);
    const loginPage =     { title: 'Logout', name: 'TabsPage', icon: 'log-out', logsOut: true };
    if(profile.isOffice){
      const packagePage = { title: 'Packages', name: 'PackagesPage', icon: 'alarm'};
      this.loggedInPages.push(packagePage);
      this.loggedInPages.push(loginPage);
    }else{
      this.loggedInPages.push(loginPage);
    }
    this.loading.showLoading(true,'Loading data..');
    this.getMatchingServices();
    setTimeout(() => {
      this.socket_ser.connect();
    },2000);
    await this.initPush().catch(console.log);
    await this.loadData().toPromise().catch(console.log);
    await this.local_conv.checkForNewMessages().catch(console.log);
    this.loading.showLoading(false);
    this.testPage().catch(console.log);
    setTimeout(() => {
      this.enableMenu(true);
    },1000)
  }
  private listenToLinks(){
    if(this.helpers.platform !== "browser"){
      this.deeplinks.route({
        '/profile/:id': "profile",
        '/service/:id': "service",
        'profile/:id/event': 'event',
        'profile/:id/news': 'news',
      }).subscribe(match => {
        this.toOpen = {
          path: match.$route,
          args: match.$args,
        };
        console.log('match object:', match);
      }, nomatch => {
        // nomatch.$link - the full link data
        console.error('Got a deeplink that didn\'t match', nomatch);
      });
    }
  }
  openPage(page: PageInterface) {
    let params = {};

    if (page.index) {
      params = { tabIndex: page.index };
    }

    if (this.nav.getActiveChildNavs().length && page.index != undefined) {
      this.nav.getActiveChildNavs()[0].select(page.index);
    }else if(page.logsOut){
      this.nav.setRoot("LoginPage");
    }else if(page.home){
      this.nav.setRoot("TabsPage");
    }else{
      if(page.title === 'Organization'){
        this.nav.push(page.name,{my_organizations: true}).catch(console.log);
      }else{
        this.nav.push(page.name,params).catch(console.log);
      }
    }

    if (page.logsOut === true) {
      // Give the menu time to close before changing to logged out
      this.auth_ser.logOut().subscribe(()=>{},err => {console.log(err)});
    }
  }
  private getMatchingServices(){
    // // todo: uncomment this:
    // this.platform.ready().then(() => {
    //   this.location_ser.getLocation().subscribe(location => {
    //     this.profile_ser.updateLocation(location).subscribe(up_res => {
    //       // this.match_ser.match().subscribe(services => {
    //       //   this.events.publish('service:matched',services)
    //       // });
    //     },err => {
    //       console.log("couldn't save location");
    //     });
    //   },err => {
    //     console.log(err);
    //   });
    // },err => {
    //   console.log(err);
    // })
  }
  async testPage(){
    //this.nav.push("ProfileViewPage", {id:1}).catch(console.log);
  }
  private async listenToEvents() {
    await this.platform.ready();
    this.platform.pause.subscribe(() => {
      this.local_notification = true;
    });
    this.platform.resume.subscribe(() => {
      this.local_notification = false;
    });
    this.events.subscribe('notifications:new',(not:INotification) => {
      if(this.local_notification){
        this.not_ser.sendLocalPush(not.data.text);
      }
    });
    this.events.subscribe('invisible_notifications:new',(not:INotification) => {
      if(not.type === 'confirmation_created'){
        (async() => {
          const profile = await this.auth_ser.getCurrentProfile().catch(console.log);
          if(!profile){
            return;
          }
          const update = <IContentUpdate>not.data.original.content_update;
          const hasOffice = update && update.office && update.office.author;
          if(hasOffice && update.status === 6 && update.office.author.id === profile.id){
            //if office receives a package, show panel to take appointment with the needy:
            update.status = 15;
            this.events.publish('content_update:resolved', update.previous_id);
            this.displayAppointmentPanel(update);
          }
        })().catch(console.log);
      }
    });
    //when user logs in
    this.events.subscribe('user:login', () => {
      this.startApp().catch(console.log);
    });
    this.events.subscribe('service:matched',(services) =>{
      //this.nav.push(ServiceMatchPage,{services:services})
    });
    //when user logs out
    this.events.subscribe('user:logout', () => {
      this.socket_ser.disconnect();
      this.enableMenu(false);
      this.loggedInPages = this.loggedInPages.filter(item => {
        return item.title !== 'Packages' && item.title !== "Logout";
      });
      console.log('logged in pages:', this.loggedInPages);
    });

    //when user signs up
    this.events.subscribe('user:signup', () => {
      this.enableMenu(true);
    });

    //when app requests for the login page:
    this.events.subscribe('goToLogin', () => {
      if(!this.loginIsActive){
        this.nav.setRoot("LoginPage");
        this.loginIsActive = true;
      }
    });
    this.events.subscribe('app:load',() => {
      this.splashScreen.show();
      this.loadData().subscribe(() => {}, console.log,() => {
        this.splashScreen.hide();
      });
    });

    // when user finishes granting permissions:
    this.events.subscribe('permissions:done',() => {
      this.createDirs().catch(console.log);
    });
    this.events.subscribe('loading:finished',()=>{
      if(this.toOpen){
        this.navigateFromExternal();
      }
    });
    // when user clicks reload app
    this.events.subscribe('app:reload', () => {
      console.log('restarting...');
      this.splashScreen.show();
      this.rootPage = "PermissionsPage";
      window.location.reload();
    });
    this.events.subscribe('conversation:entered', (id:number) => {
      this.opened_conversation = id;
    });
    this.events.subscribe('conversation:left', (id:number) => {
      this.opened_conversation = 0;
    });
    // socket events:
    this.socket_ser.onMessage.subscribe(message => {
      if(message.author && this.socket_ser.entered_conv !== message.conversation_id){
        this.note_ser.showNote("New message from " + message.author.name);
      }else{
        console.log('cannot display message note');
      }
    });
  }
  // async checkIfConfirmed(item:INotification){
  //   if(item.type === 'package_confirmation'){
  //     const update = <IContentUpdate>item.data.original;
  //     console.log('to check:', update);
  //     if(update.status === 7){
  //       const up_code = await this.not_ser.getSavedCode(update.previous_id,'update_code').catch(console.log);
  //       if(!up_code){
  //         return;
  //       }
  //       //
  //       await this.sendConfirmation(update).catch(console.log);
  //       // const conf = await this.not_ser.getConfirmation(update.parent_id);
  //       // if(!conf){
  //       //   await this.sendConfirmation(update).catch(console.log);
  //       // }
  //     }
  //   }
  // }
  // async sendConfirmation(update:IContentUpdate){
  //   const content_update: IContentUpdate = {
  //     previous_id: update.id,
  //     content_id: update.content_id,
  //     parent_id: update.id,
  //     direct: update.direct,
  //     status: 7,
  //     primary: true,
  //     actions_allowed: false,
  //     scheduled: update.scheduled,
  //     schedule: update.schedule,
  //     modified: false,
  //   };
  //   if(update.lat){
  //     content_update.lat = update.lat;
  //   }
  //   if(update.lng){
  //     content_update.lng = update.lng;
  //   }
  //   if(update.details){
  //     content_update.details = update.details;
  //   }
  //   if(update.parent_id){
  //     content_update.parent_id = update.parent_id;
  //   }
  //   if(update.office_id){
  //     content_update.office_id = update.office_id;
  //   }
  //   const res = await this.con_up_ser.save(update.content_id,content_update).toPromise().catch(console.log);
  //   if(!res){
  //     return;
  //   }
  //   await this.not_ser.saveConfirmation(update.parent_id).catch(console.log);
  //   if(update.office_id){
  //     const _update = <IContentUpdate>this.helpers.clone(update);
  //     _update.status = 15;
  //     this.displayAppointmentPanel(_update);
  //   }
  //   return res && res.success;
  // }
  displayAppointmentPanel(update:IContentUpdate){
    if(this.helpers.platform === 'browser'){
      let modal = this.modalCtrl.create("TakeActionPage",{update: update});
      modal.present().catch(console.log);
      console.log('TakeActionPage pushed');
    }else{
      this.nav.push("TakeActionPage",{update: update}).catch(console.log);
    }
  }
  navigateFromExternal(){
    let page = null;
    if(this.toOpen){
      switch (this.toOpen.path){
        case "profile":
          page = "ProfileViewPage";
          break;
        case "service":
        case 'event':
        case 'news':
          page =  'ContentViewPage';
          break;
      }
      if(page){
        setTimeout(() => {
          this.nav.push(page,this.toOpen.args).catch(console.log);
          console.log("navigated from deep link");
        },500)
      }
    }
  }
  async initPush(){
    const token = await this.not_ser.setUpPush().catch(console.log);
    const language = await this.glob.getPreferredLanguage().catch(console.log);
    console.log('device token:', token);
    if(token){
      let data = {
        token: token,
        ar: true,
        timezone: this.helpers.getTimeZoneName(),
      };
      if(language && language.value.match("ar")){
        data.ar = true;
      }
      await this.profile_ser.updateDevice(data).toPromise().catch(console.log);
    }
  }
  enableMenu(loggedIn: boolean) {
    this.menu.enable(loggedIn, 'loggedInMenu');
    this.menu.enable(!loggedIn, 'loggedOutMenu');
  }
  loadData():Observable<number>{
    return new Observable<number>((observer) => {
      let load_sub = new Subject<string>();
      let loaded = 0;
      let last = "notifications";
      let types: IContentType[] = [];
      let check_loaded = () => {
        observer.next(loaded);
        if(loaded === 7){
          observer.complete();
          this.events.publish('loading:finished',true);
        }
      };
      let loadCats = () => {
        this.category_ser.all().subscribe(res => {
          this.storage.set('categories',res)
            .then(() => {
              load_sub.next('countries');
            })
            .catch((err) => {
              console.log('failed to save categories:', err);
              load_sub.next('countries');
            });
        },err => {
          console.log('failed to fetch categories:', err);
          load_sub.next('countries');
        })
      };
      let loadCountries = () => {
        this.country_ser.getCountries().subscribe(countries => {
          this.storage.set('countries', countries)
            .then(() => {
              load_sub.next('types');
            })
            .catch(err => {
              console.log('failed to save countries:', err);
              load_sub.next('types');
            });

        },(err) => {
          console.log('failed to fetch countries:', err);
          load_sub.next('types');
        });
      };
      let loadTypes = () =>{
        this.content_ser.getTypes().subscribe((_types:IContentType[]) => {
          types = _types;
          this.storage.set('content_types',_types)
            .then(() => {
              load_sub.next('roles');
            })
            .catch((err) => {
              console.log('failed to save content types:',err);
              load_sub.next('roles');
            })
        },err =>{
          console.log('failed to fetch content types:',err);
          load_sub.next('roles');
        })
      };
      let loadRoles = () => {
        this.profile_ser.getRoles().subscribe((roles:IRole[]) => {
          this.storage.set('roles',roles)
            .then(() => {
              load_sub.next('membership_types');
            })
            .catch((err) => {
              console.log('failed to save roles:',err);
              load_sub.next('membership_types');
            })
        },err =>{
          console.log('failed to roles:',err);
          load_sub.next('membership_types');
        })
      };
      let loadMembershipTypes = () => {
        this.profile_ser.getMembershipTypes().subscribe((types:IMembershipType[]) => {
          this.storage.set('membership_types',types)
            .then(() => {
              load_sub.next('settings');
            })
            .catch((err) => {
              console.log('failed to save membership types:',err);
              load_sub.next('settings');
            })
        },err =>{
          console.log('failed to fetch membership types:',err);
          load_sub.next('settings');
        })
      };
      let loadSettings = () => {
        this.api_settings.getSettings().subscribe((settings:IApiSettings) => {
          this.settings.service_type_id = settings.service_type_id;
          this.settings.offered_service_type_id = settings.offered_service_type_id;
          this.settings.required_service_type_id = settings.required_service_type_id;
          this.settings.office_type_id = settings.office_type_id;
          this.settings.profile_type_id = settings.profile_type_id;
          this.settings.organization_type_id = settings.organization_type_id;
          this.storage.set('api_settings',settings)
            .then(() => {
              load_sub.next('notifications');
            })
            .catch((err) => {
              console.log('failed to save settings:',err);
              load_sub.next('notifications');
            })
        },err =>{
          console.log('failed to fetch settings:',err);
          load_sub.next('notifications');
        })
      };
      let loadNotCount = () => {
        this.not_ser.getCount().subscribe(res => {
          this.events.publish('notifications:count',res);
          let tabs = this.getTabs(types,res);
          this.nav.setRoot("TabsPage",{tabs:tabs}).catch(console.log);
          loaded +=1;
          check_loaded();
        },err => {
          loaded +=1;
          check_loaded();
          console.log('failed to fetch notifications count:',err);
        });
      };
      load_sub.subscribe((toLoad: string) => {
        console.log('to load:', toLoad);
        switch (toLoad){
          case 'categories':
            loadCats();
            break;
          case 'countries':
            loadCountries();
            break;
          case 'types':
            loadTypes();
            break;
          case 'roles':
            loadRoles();
            break;
          case 'membership_types':
            loadMembershipTypes();
            break;
          case 'settings':
            loadSettings();
            break;
          case 'notifications':
            loadNotCount();
            break;
        }
        if(toLoad !== last){
          loaded +=1;
          check_loaded();
        }
      });
      //@todo: uncomment this
      //load_sub.next('categories');
      // @todo: comment this
      // this is just for development, it loads stored data instead of from server:

      (async () => {
        const types = <IContentType[]>await this.storage.get('content_types').catch(console.log);
        if(types){
          let tabs = this.getTabs(types,0);
          this.nav.setRoot("TabsPage",{tabs:tabs}).catch(console.log);
        }
        const settings = <IApiSettings> await this.storage.get('api_settings').catch(console.log);
        console.log('settings',settings);
        if(settings){
          this.settings.service_type_id = settings.service_type_id;
          this.settings.offered_service_type_id = settings.offered_service_type_id;
          this.settings.required_service_type_id = settings.required_service_type_id;
          this.settings.office_type_id = settings.office_type_id;
          this.settings.profile_type_id = settings.profile_type_id;
          this.settings.organization_type_id = settings.organization_type_id;
        }
        loaded = 7;
        check_loaded();

      })().catch(console.log);
    });
  }
  private getTabs(types:IContentType[],not_count:number):ITab[]{
    let tabs:ITab[] = [
      {
        title: "Home",
        icon: "home",
        page: "home",
        name:"HomePage",
        type:null,
        tabBadge: null,
        tabBadgeStyle: null,
      }
    ];
    for(let type of types){
      if(type.tab){
        let tab:ITab = {
          title: type.type + 's',
          icon: type.icon,
          name:"ContentIndexPage",
          page: type.type,
          type: type,
          tabBadge: null,
          tabBadgeStyle: null,
        };
        if(this.isTypeProfile(type)){
          tab.title = "Members";
          tab.name = "ProfileIndexPage";
        }
        tabs.push(tab);
      }else{
        console.log('type: ' + type.type + " is not a tab");
      }
    }
    console.log('notifications count:',not_count);
    tabs.push({
      title: "Notifications",
      icon: "notifications",
      page: "notifications",
      name: "NotificationsPage",
      type:null,
      tabBadge: not_count > 0 ? not_count: null,
      tabBadgeStyle: 'danger',
    });
    console.log("tabs:", tabs);
    return tabs;
  }
  isTypeProfile(type:IContentType): boolean{
    let name = type.type.toLowerCase();
    if(name.match('profile') || name.match('user')){
      return true;
    }
    return false;
  }
  isActive(page: PageInterface) {
    this.ref.markForCheck();
    let childNav = this.nav.getActiveChildNavs()[0];

    // Tabs are a special case because they have their own navigation
    if (childNav) {
      if (childNav.getSelected() && childNav.getSelected().root === page.tabComponent) {
        return 'primary';
      }
      return;
    }
    if (this.nav.getActive() && this.nav.getActive().name === page.name) {
      return 'primary';
    }
    return;
  }
  async createDirs(){
    const base = this.settings.localStorageFolder;
    let storageDir = null;
    if(this.helpers.platform === 'android'){
      storageDir = this.file.externalRootDirectory;
    }else if(this.helpers.platform === "ios"){
      storageDir = this.file.documentsDirectory;
    }
    if(!storageDir){
      return false;
    }
    console.log('storage dir:', storageDir);
    const receivedPath = storageDir + base;
    const sentPath = storageDir + base + '/' + 'Sent';
    const baseDir = await this.file.createDir(storageDir,base,false).catch(console.log);
    if(!baseDir){
      return false;
    }
    const sentDir = await this.file.createDir(storageDir + "/" + base, 'Sent',false).catch(console.log);
    if(!sentDir){
      return false;
    }
    // Create folder for sent files:
    await this.file.createDir(sentPath,'Audio',false).catch(console.log);
    await this.file.createDir(sentPath,'Images',false).catch(console.log);
    await this.file.createDir(sentPath,'Files',false).catch(console.log);

    // Create folder for received files:
    await this.file.createDir(receivedPath,base + ' Audio',false).catch(console.log);
    await this.file.createDir(receivedPath,base + ' Images',false).catch(console.log);
    await this.file.createDir(receivedPath,base + ' Files',false).catch(console.log);
    console.log('all folders created');
  }
}
