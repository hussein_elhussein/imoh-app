import { NgModule } from '@angular/core';
import { VRatingComponent } from './components/v-rating/v-rating';
import {VRatingService} from "./providers/vrating.service";
import {Ionic2RatingModule} from "ionic2-rating";
import {BrowserModule} from "@angular/platform-browser";
import {IonicModule} from "ionic-angular";
import {CommonModule} from "@angular/common";
@NgModule({
	declarations: [VRatingComponent],
	imports: [
		CommonModule,
		IonicModule,
		Ionic2RatingModule,
	],
	exports: [VRatingComponent],
	providers:[VRatingService],
	entryComponents:[]

})
export class VRatingModule {}
