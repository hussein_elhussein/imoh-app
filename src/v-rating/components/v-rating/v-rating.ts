import {Component, EventEmitter, Input, Output} from '@angular/core';
import {AlertController, NavController} from "ionic-angular";

/**
 * Generated class for the VRatingComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'v-rating',
  templateUrl: 'v-rating.html'
})
export class VRatingComponent {
  @Output() onRate = new EventEmitter<any>();
  @Output() onResultClick = new EventEmitter<any>();
  @Input() result:number;
  @Input() raters:number;
  @Input() can_rate:boolean;
  @Input() stars_only:boolean;
  rate_value:string;
  rate_text:string;
  constructor(private alertCtrl:AlertController,public navCtrl:NavController) {
    this.rate_text = "";
    this.raters = 5;
    this.can_rate = true;
    this.stars_only = false;
  }
  onChange(value):void{
    this.display_text(value);
  }
  display_text(value:number):void{
    switch (value){
      case 1:
        this.rate_text = 'Bad!';
        break;
      case 2:
        this.rate_text = "I don't like !";
        break;
      case 3:
        this.rate_text = "It's OK";
        break;
      case 4:
        this.rate_text = "It's good!";
        break;
      case 5:
        this.rate_text = "Fantastic!";
        break;
    }
  }
  showRatingPanel() {
    let alert = this.alertCtrl.create({
      title: 'Rate',
      subTitle: "",
      buttons: [
        {
          text: 'SUBMIT',
          role: null,
          handler: (data) => {
            let rating = {comment:data.comment,rating:this.rate_value};
            this.onRate.emit(rating);
          }
        }
      ]
    });
    alert.addInput({
      type: 'text',
      name:'comment',
      placeholder: 'Type your comment here..',
    });
    alert.present();
  }
  getRateValue():string{
    return Number(this.result).toFixed()
  }
  resultClicked():void{
    this.onResultClick.emit(true);
  }
}
