import {IVFormField} from "./IVFormField";

export interface IVFile{
  name:string;
  blob:any;
  fields?:IVFormField[];
}