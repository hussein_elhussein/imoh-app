export class IVError{
  name: string;
  details: string;
}