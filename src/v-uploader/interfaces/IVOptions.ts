import {IVFormField} from './IVFormField';
import {EventEmitter} from '@angular/core';
import {IVUpHeader} from './IVUpHeader';

export interface IVOptions{
  url: string;
  field: string;
  max_retries?: number;
  unique_id?:string;
  allowedExtensions?: string[];
  queueSize?:number;
  autoUpload?: boolean;
  uploadEvent?:EventEmitter<any>;
  /**
   * Maximum file size (in megabyte).
   */
  maxSize?:number;
  fields?:IVFormField[];
  headers?:IVUpHeader[];
  inline_token?: string;
  tokenHeader?: string;
  //todo: add those classes:

  /**
   * Delay between each chunk upload request (in milliseconds).
   */
  throttle?:number;

  /**
   * The size to be uploaded in each chunk (in kb).
   * Note: for php you have to set
   */
  chunk_size?:number;
}
