import {IVUploadingFile} from './IVUploadingFile';

export interface IVQueue{
  files: IVUploadingFile[];
  done: boolean;
}
