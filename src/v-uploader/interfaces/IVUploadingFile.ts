import {IVError} from './IVError';
import {IVChunk} from './IVChunk';
import {IVFormField} from "./IVFormField";

export interface IVUploadingFile{
  id?:string;
  name:string;
  size?:number;
  done?:boolean;
  progress?: number;
  error?: string;
  response?:string;
  errors?:IVError[];
  blob: any;
  chunks?:IVChunk[];
  retries?:number;
  fields?:IVFormField[];
}
