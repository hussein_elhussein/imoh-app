export interface IVChunk{
  id:string;
  front_file_id:string;
  back_file_id?:string;
  data:string;
  start:number;
  end:number;
  size:number;
  data_type:string;
  bytes_uploaded?:number;
  uploaded?:boolean;
  tried?:number;
  error?:any;
  header: string;
}
