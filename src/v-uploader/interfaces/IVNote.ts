export interface IVNote{
  title?: string;
  details: string;
  displayed?:boolean;
}
