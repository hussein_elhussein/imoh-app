import {Component, Input} from '@angular/core';


@Component({
    selector: 'ionUp-progress',
    templateUrl: 'progress.component.html',
})
export class Progress{
    @Input('progress') progress;
    constructor(){}
}