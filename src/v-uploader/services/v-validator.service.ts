import {Injectable} from '@angular/core';
import {IVUploadingFile} from '../interfaces/IVUploadingFile';
import {IVOptions} from '../interfaces/IVOptions';
import {IVError} from '../interfaces/IVError';

@Injectable()
export class VValidatorService {
  constructor() {}

  public validate(files: IVUploadingFile[],options:IVOptions):IVUploadingFile[]{
    for(let i=0; i < files.length; i++){
      const errors: IVError[] = [];
      //validate extension:
      const valid_ext = this.validateExtension(files[i],options.allowedExtensions);
      if(!valid_ext){
        let allowed = "";
        options.allowedExtensions.forEach(ext => {
          allowed = allowed + ext + ",";
        });
        allowed = allowed.slice(0,-1);
        const err: IVError = {
          name: 'Extension not allowed.',
          details: "The allowed extensions are: " + allowed + '.',
        };
        errors.push(err);
      }
      //validate size:
      if(options.maxSize){
        const valid_size = this.validateSize(files[i],options.maxSize);
        if(!valid_size){
          const err: IVError = {
            name: '',
            details:'',
          };
          if(valid_size === null){
            err.name = 'File is empty.';
            err.details = "Please choose a valid file.";
          }else{
            err.name = 'File size is too big.';
            err.details = "The maximum allowed size is " + options.maxSize + "MB.";
          }
          errors.push(err);
        }
      }
      if(options.queueSize){
        const valid_queue = this.validateQueue(files,options);
        if(!valid_queue){
          const err: IVError = {
            name: 'Files exceeded the maximum allowed number.',
            details: "The maximum allowed number of files is   " + (options.queueSize > 1)? " file": 'files.',
          };
          errors.push(err);
        }
        files[i].errors = errors;
      }
      files[i].errors = errors;
    }
    return files;
  }
  private validateExtension(file: IVUploadingFile,allowed_ext: Array<string> = []):boolean{
    if(allowed_ext && allowed_ext.length){
      const split = file.blob.name.split('.');
      const ext = split[split.length-1];
      if(!split.length){
        return null;
      }
      const allowed = allowed_ext.filter(item => {
        return item === ext;
      });
      return allowed.length > 0;
    }else{
      return true;
    }
  }
  private validateSize(file:IVUploadingFile, max_size: number):boolean{
    const file_size = file.blob.size;
    const max = max_size * 1048576;
    if(!file.blob.size){
      return null;
    }
    return !(file_size > max);
  }

  private validateQueue(files:IVUploadingFile[], options:IVOptions):boolean{
    return files.length <= options.queueSize;
  }
}
