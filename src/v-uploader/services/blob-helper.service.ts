import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
@Injectable()
export class BlobHelper{
  constructor(){}

  convertToBlob(b64Data, contentType):any{
    contentType = contentType || '';
    let sliceSize = 512;

    let byteCharacters = atob(b64Data);
    let byteArrays = [];

    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      let slice = byteCharacters.slice(offset, offset + sliceSize);

      let byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      let byteArray = new Uint8Array(byteNumbers);

      byteArrays.push(byteArray);
    }

    let blob:any = new Blob(byteArrays, {type: contentType});
    let name = null;
    if(contentType === 'image/jpeg'){
      name = 'file.jpg';
    }else if(contentType === 'image/png'){
      name = 'file.png';
    }
    blob.name = name;
    return blob;

  }
  pathToBlob(path):Observable<any>{
    return Observable.create(observer => {
      let ext = path.split('.')[1];
      let splits = path.split('/');
      let name = splits[splits.length -1];
      (<any>window).resolveLocalFileSystemURL(path, (res) => {
        res.file((resFile) => {
          let reader = new FileReader();
          reader.readAsArrayBuffer(resFile);
          reader.onloadend = (evt: any) => {
            let fileBlob:any;
            if(ext == 'jpg'){
              fileBlob = new Blob([evt.target.result], { type: 'image/jpeg'});
            }else{
              fileBlob = new Blob([evt.target.result], { type: ext});
            }
            fileBlob.name = name;
            observer.next(fileBlob);
            observer.complete();
          }
        })
      })
    });
  }
  getFileContentAsBase64(path,callback){
    (<any>window).resolveLocalFileSystemURL(path, (res) =>{
      res.file(function(file) {
        var reader = new FileReader();
        reader.onloadend = function(e) {
          var content = this.result;
          callback(content);
        };
        // The most important point, use the readAsDatURL Method from the file plugin
        reader.readAsDataURL(file);
      });
    },(err)=>{
      console.log(err.message);
    });
  }
  getBase64(file):Promise<any>{
    return new Promise<any>((resolve,reject) => {
      const reader = new FileReader();
      reader.onloadend = function(e) {
        const content = this.result;
        resolve(content);
      };
      reader.onerror =  function (e){
        console.log('read error');
        console.log(e);
        reject(e);
      };
      reader.onabort = function(e){
        console.log('read aborted');
        console.log(e);
        reject(e);
      };
      // The most important point, use the readAsDatURL Method from the file plugin
      reader.readAsDataURL(file);
    });
  }
}
