import {Injectable} from '@angular/core';
import {IVOptions} from '../interfaces/IVOptions';
import {IVFormField} from '../interfaces/IVFormField';
import {IVUploadingFile} from '../interfaces/IVUploadingFile';
import {VValidatorService} from './v-validator.service';
import {IVFile} from "../interfaces/IVFile";
import {IVError} from "../interfaces/IVError";
import {Subject} from "rxjs/Subject";
import {Observable} from "rxjs/Observable";

@Injectable()
export class VUploaderService{
  options:IVOptions;
  fields: IVFormField[];
  private uploadingFiles: IVUploadingFile[];
  onUpload: Subject<IVUploadingFile[]>;
  onProgress: Subject<IVUploadingFile>;
  onUploadError: Subject<IVUploadingFile>;
  onUploadErrorT: Subject<IVUploadingFile>;
  onFileFinish: Subject<IVUploadingFile>;
  onAllFinished: Subject<any>;
  onFileAdd: Subject<IVUploadingFile>;
  onValidationError: Subject<IVUploadingFile>;
  waitingToRetry: boolean;
  constructor(private validator:VValidatorService){
    this.uploadingFiles = [];
    this.onFileFinish = new Subject<IVUploadingFile>();
    this.onAllFinished = new Subject<any>();
    this.onUpload = new Subject<IVUploadingFile[]>();
    this.onProgress = new Subject<IVUploadingFile>();
    this.onUploadError = new Subject<IVUploadingFile>();
    this.onFileAdd = new Subject<IVUploadingFile>();
    this.onValidationError = new Subject<IVUploadingFile>();
    this.waitingToRetry = false;
    this.onUploadErrorT = new Subject<IVUploadingFile>();
  }

  add(files: IVFile[], options: IVOptions):void{
    this.options = options;
    this.fields = options.fields;
    let v_files: IVUploadingFile[] = [];
    files.forEach(file => {
      let uploadingFile = <IVUploadingFile>{
        name:file.name,
        size: file.blob.size,
        blob: file.blob,
        progress:0,
        done: false,
        error: null,
        retries: 0,
      };
      if(file.fields){
        uploadingFile.fields = file.fields;
      }
      v_files.push(uploadingFile);
    });
    let validated_files  = this.validator.validate(v_files,options);
    let passed_files = validated_files.filter((item) => {
      if(item.errors && item.errors.length){
        this.onValidationError.next(item);
      }else{
        let file_id = this.generateID();
        while(this.fileExist(file_id)){
          file_id = this.generateID();
        }
        item.id = file_id;
        return item;
      }
    });
    if(passed_files && passed_files.length){
      passed_files.forEach((item => {
        this.uploadingFiles.push(item);
        this.onFileAdd.next(item);
        if(this.options.autoUpload){
          this.uploadFile(item);
        }
      }));
    }

  }
  start():void{
    if(this.uploadingFiles.length){
      this.uploadingFiles.forEach(file => {
        if(!file.error){
          this.uploadFile(file);
        }
      });
    }else{
      console.log('no files to upload');
    }
  }
  retry(){
    let max_retries = this.options.max_retries ? this.options.max_retries : 3;
    this.uploadingFiles.forEach(file => {
      if(file.retries < max_retries){
        file.error = null;
        file.retries += 1;
        console.log('file retries: ', file.retries);
      }
    });
    this.uploadingFiles = this.uploadingFiles.filter(file => {
      return file.retries < max_retries;
    });
    if(this.uploadingFiles.length){
      console.log('files to upload: ', this.uploadingFiles.length);
      this.start();
    }
  }
  retryWhen(event:Subject<any>){
    if(!this.waitingToRetry){
      this.waitingToRetry = true;
      let sub = event.subscribe(() => {
        this.uploadingFiles.forEach(file => {
          file.retries = 0;
        });
        this.retry();
        this.waitingToRetry = false;
        sub.unsubscribe();
      })
    }
  }
  queueIsEmpty():boolean{
    return this.uploadingFiles.length > 0;
  }
  private uploadFile(file: IVUploadingFile): void {
    let xhr = new XMLHttpRequest();
    let form = this.initForm(file);
    if(this.options){
      if(this.options.unique_id){
        file.name = this.options.unique_id;
      }
    }
    xhr.upload.onprogress = (e: ProgressEvent)=>{
      this.uploadProgress(e, file);
    };
    xhr.upload.onloadstart = () => {
      this.loadStart(file);
    };
    xhr.onload = (e) =>{
      if(xhr.status === 200){
        this.uploadComplete(file, e);
      }else{
        console.log('onLoad emitted');
        this.uploadFailed(xhr.statusText,e,file);
      }
    };
    xhr.onerror = (e) => {
      this.uploadFailed(xhr.statusText,e,file);
    };
    xhr.onabort = () => {
      this.uploadCanceled(file);
    };
    xhr.open('POST', this.options.url, true);
    xhr.withCredentials = true;
    if(this.options) {
      if(this.options.headers) {
        this.options.headers.forEach(header => {
          xhr.setRequestHeader(header.key,header.value);
        });
      }
      if(this.options.tokenHeader) {
        xhr.setRequestHeader('Authorization', 'Bearer ' + this.options.tokenHeader);
      }
    }
    xhr.send(form);
  }
  private initForm(file:IVUploadingFile):FormData{
    let form = new FormData();
    form.append(this.options.field, file.blob, file.blob.name);
    if(this.fields && this.fields.length) {
      for(let i =0; i < this.fields.length; i++){
        form.append(this.fields[i].name,this.fields[i].value);
      }
    }
    if(file.fields && file.fields.length){
      for(let i =0; i < file.fields.length; i++){
        form.append(file.fields[i].name,file.fields[i].value);
      }
    }
    if(this.options) {
      if(this.options.inline_token){
        form.append('token', this.options.inline_token);
      }
    }
    return form;
  }

  private uploadCanceled(file):void{
    let error = 'upload canceled by the user';
    console.log(error);
  }
  private loadStart(file: any):void{
    //this.updateUploadingList(file);
  }
  private uploadProgress(event, file: IVUploadingFile):void{
    let progress = null;
    if (event.lengthComputable) {
      let completed = (event.loaded / event.total) * 100;
      progress = completed.toFixed(1);
    }
    file.progress = progress;
    this.onProgress.next(file);
    this.updateUploadingList(file);
  }

  private uploadComplete(file:IVUploadingFile,e):void{
    let res = JSON.parse(e.target.responseText);
    file.response = res;
    file.progress = 100;
    file.done = true;
    this.onFileFinish.next(file);
    this.updateUploadingList(file);
  }
  private uploadFailed(error:string, event, file: IVUploadingFile):void{
    file.error = error;
    this.updateUploadingList(file);
    let retries = this.options.max_retries? this.options.max_retries: 3;
    if(file.retries < retries -1){
      this.retry();
    }else{
      console.log('onUploadError emitted');
      this.onUploadError.next(file);
    }
  }
  private updateUploadingList(file: IVUploadingFile):void{
    this.uploadingFiles = this.uploadingFiles.filter(item =>{
      return item.id !== file.id;
    });
    this.uploadingFiles.push(file);
    if(file.done){
      this.uploadingFiles = this.uploadingFiles.filter(item =>{
        return item.id !== file.id;
      });
    }
    if(!this.uploadingFiles.length){
      this.onAllFinished.next(true);
    }
  }
  private fileExist(id:string):boolean{
    this.uploadingFiles.forEach(item => {
      if(item.id === id){
        return true;
      }
    });
    return false;
  }
  generateID():string{
    let text = "";
    let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( let i=0; i < 5; i++ ){
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    }

    return text;
  }


}
