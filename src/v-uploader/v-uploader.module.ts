import { NgModule } from '@angular/core';
import {CommonModule} from "@angular/common";
import {VUploaderService} from "./services/v-uploader.service";
import {BlobHelper} from "./services/blob-helper.service";
import {Progress} from "./components/progress/progress.component";
import {VValidatorService} from "./services/v-validator.service";

@NgModule({
  declarations: [Progress],
  imports: [CommonModule],
  exports:[Progress],
  providers:[BlobHelper,VValidatorService,VUploaderService]


})
export class VUploaderModule {}
