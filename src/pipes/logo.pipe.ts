import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'logo',
  pure: false,
})
export class LogoPipe implements PipeTransform {
  constructor(){

  }
  transform() {
    return '/assets/imgs/logo.png';

  }
}