import { Pipe, PipeTransform } from '@angular/core';
import {SettingsService} from "../providers/settings.service";

@Pipe({
  name: 'getImage',
  pure: false,
})
export class GetImagePipe implements PipeTransform {
  constructor(private settings:SettingsService){

  }
  transform(url) {
    if(url){
      return this.settings.storage_url + url;
    }
    return 'assets/imgs/default.png';
  }
}