import {NgModule} from "@angular/core";
import {GetImagePipe} from "./get-image.pipe";
import {LogoPipe} from "./logo.pipe";
import {SafeUrlPipe} from "./safe-url.pipe";
import {TimeAgoPipe} from "./time-ago.pipe";
import {VCalendarPipe} from "./vcalendar.pipe";
import {VFormatPipe} from "./vformat.pipe";

@NgModule({
  declarations:[GetImagePipe,LogoPipe,SafeUrlPipe,TimeAgoPipe,VCalendarPipe,VFormatPipe],
  exports:[GetImagePipe,LogoPipe,SafeUrlPipe,TimeAgoPipe,VCalendarPipe,VFormatPipe]
})
export class PipesModule {}