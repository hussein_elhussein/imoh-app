import {ChangeDetectorRef, NgZone, Pipe, PipeTransform} from '@angular/core';
import {HelpersService} from "../providers/helpers.service";
import set = Reflect.set;

@Pipe({
  name: 'vFormat',
  pure: true,
})
export class VFormatPipe implements PipeTransform {
  constructor(private helpers: HelpersService, private zone:NgZone, private changeDect:ChangeDetectorRef){

  }
  transform(date: any, args?: string){
    let out = null;
    this.zone.runOutsideAngular(() => {
      out = this.helpers.fromUnixNoFormat(date).format(args);
    });
    return out;
  }
}