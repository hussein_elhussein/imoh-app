import { Pipe, PipeTransform } from '@angular/core';
import {HelpersService} from "../providers/helpers.service";

@Pipe({
  name: 'timeAgo',
  pure: true,
})
export class TimeAgoPipe implements PipeTransform {
  constructor(private helpers: HelpersService){

  }
  transform(date: any): string{
    return this.helpers.fromUnixNoFormat(date).fromNow();
  }
}