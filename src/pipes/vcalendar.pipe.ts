import {ChangeDetectorRef, NgZone, Pipe, PipeTransform} from '@angular/core';
import {HelpersService} from "../providers/helpers.service";
import set = Reflect.set;

@Pipe({
  name: 'vCalendar',
  pure: true,
})
export class VCalendarPipe implements PipeTransform {
  constructor(private helpers: HelpersService, private zone:NgZone, private changeDect:ChangeDetectorRef){

  }
  transform(date: any){
    let out = null;
    this.zone.runOutsideAngular(() => {
      out = this.helpers.fromUnixNoFormat(date).calendar();
    });
    return out;
  }
}