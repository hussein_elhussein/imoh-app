// src/app/auth/token.interceptor.ts
import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor() {}
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let token = <string> localStorage.getItem('token');
    let headers =  null;
    if(token){
      headers =  {
        Authorization: 'Bearer ' + token,
        'X-Requested-With': 'XMLHttpRequest',
      };
    }else{
      headers = {'X-Requested-With': 'XMLHttpRequest'}
    }
    request = request.clone({
      setHeaders: headers
    });
    return next.handle(request);
  }
}