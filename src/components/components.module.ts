import {NgModule} from "@angular/core";
import {CategoriesFilter} from "./categories-filter/categories-filter";
import {ContentUpdateNotificationComponent} from "./content-update-notification/content-update-notification";
import {IonicModule} from "ionic-angular";
import {VRatingModule} from "../v-rating/v-rating.module";
import { ShareComponent } from './share/share';
import {AttachPreviewPage} from "../pages/attach-preview/attach-preview";
import {FormSection} from "./form-section/form-section";
import {PipesModule} from "../pipes/pipes.module";

@NgModule({
  declarations:[
    CategoriesFilter,
    ContentUpdateNotificationComponent,
    ShareComponent,
    FormSection
  ],
  imports:[
    VRatingModule,
    PipesModule,
    IonicModule
  ],
  exports:[
    CategoriesFilter,
    ContentUpdateNotificationComponent,
    ShareComponent,
    FormSection
  ],
})
export class ComponentsModule{}