import {Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {ActionSheetController, Events, NavController, NavParams, Slides, ViewController} from 'ionic-angular';
import {ISection} from "../../interfaces/ISection";
import {IField} from "../../interfaces/IField";
import {HelpersService} from "../../providers/helpers.service";
import {IFieldOptions} from "../../interfaces/IFieldOptions";
import {File, FileEntry, IFile} from "@ionic-native/file";
import {ChangeHistory, IFFile} from "../../interfaces/IFFile";
import {Camera} from "@ionic-native/camera";
import {StatusBar} from "@ionic-native/status-bar";
import {FileChooser} from "@ionic-native/file-chooser";
import {IFImage} from "../../interfaces/IFImage";
import {NotesService} from "../../providers/notes.service";
import {BlobHelper} from "../../v-uploader/services/blob-helper.service";
import {LoadingService} from "../../providers/loading.service";
import {IContent} from "../../interfaces/IContent";
import {SettingsService} from "../../providers/settings.service";
import {Observable} from "rxjs/Observable";
import {IVFile} from "../../v-uploader/interfaces/IVFile";
import {IVOptions} from "../../v-uploader/interfaces/IVOptions";
import {IContentType} from "../../interfaces/IContentType";
import {VUploaderService} from "../../v-uploader/services/v-uploader.service";
import {AuthService} from "../../providers/auth.service";
import {IFieldVal} from "../../interfaces/IFieldVal";
import {Subject} from "rxjs/Subject";
import {FileHelperService} from "../../providers/file-helper.service";
import {ICategory} from "../../interfaces/ICategory";
import {LocationHelperService} from "../../providers/location-helper.service";
import {Subscription} from "rxjs/Subscription";


@Component({
  selector: 'form-section',
  templateUrl: 'form-section.html',
  providers: [StatusBar,Camera,File, FileChooser]
})
export class FormSection implements OnInit, OnDestroy{
  @Input() available_types: IContentType[];
  @Input() selected_type:IContentType;
  @Input() content:IContent;
  @Input() display_titles: boolean;
  @Output() onCollectFinish: EventEmitter<any>;
  @ViewChild(Slides)
  slidesCon: Slides;
  _platform: string;
  new_content:IContent;
  selected_images: IFImage[];
  selected_files: IFFile[];
  filled_fields: any[];
  uploaded: boolean;
  filled_sections:any[];
  section_is_valid: boolean;
  isSliding:boolean;
  selected_category: ICategory;
  selected_sub_cat:ICategory;
  search_sub: Subscription;
  searchFieldsReady: Array<boolean>;
  constructor(
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public navCtrl:NavController,
    public helpers:HelpersService,
    private actionSheetCtrl:ActionSheetController,
    private camera:Camera,
    private natFile:File,
    private notes_ser:NotesService,
    private blob_ser:BlobHelper,
    private fileChooser: FileChooser,
    private loading_ser:LoadingService,
    private settings:SettingsService,
    private up_ser:VUploaderService,
    private auth_ser:AuthService,
    private file_helper:FileHelperService,
    private events:Events,
    private location_ser:LocationHelperService) {
    this._platform = this.helpers.platform;
    this.onCollectFinish = new EventEmitter<any>();
    this.selected_files = [];
    this.selected_images = [];
    this.filled_fields = [];
    this.uploaded = false;
    this.filled_sections = [];
    this.section_is_valid = false;
    this.isSliding = false;
    this.searchFieldsReady = [];
    this.new_content = {
      profile_id: null,
      content_type_id: null,
      type: null,
      title: '',
      featured: false,
      lat: null,
      lng: null,
      quantity: 1,
    };
  }
  ngOnInit(){
    if(this.content){
      //console.log('form: selected type:', this.selected_type);
      this.populateFields();
    }
    this.prepareSections();
    this.listenToEvents();
  }
  ngOnDestroy(){
    if(this.search_sub){
      this.search_sub.unsubscribe();
    }
  }
  listenToEvents(){
    this.search_sub = this.location_ser.onPlaceSearch.subscribe(res => {
      for(let sec of this.selected_type.sections){
        for(let field of sec.fields){
          if(field.id === res.id){
            field.hidden_val = res.coords;
            field.value = res.coords.lat.toString() + " " + res.coords.lng.toString();
          }
        }
      }
    });
  }
  populateFields(){
    this.new_content.id = this.content.id;
    this.new_content.status = this.content.status;
    this.new_content.featured = this.content.featured;
    if(this.content.categories){
      //search for selected parent category:
      for(let cat of this.content.categories){
        if(!cat.parent_id && cat.children){
          this.selected_category = cat;
        }
      }
      //search for selected child category:
      for(let cat of this.content.categories){
        if(cat.parent_id && !cat.children){
          this.selected_sub_cat = cat;
        }
      }
    }
    let files_to_fetch = [];
    let files_sub = new Subject();
    let started_fetching = false;
    let fetchFile = (field:IField) => {
      if(!started_fetching){
        started_fetching = true;
        files_sub.subscribe((field:IField) => {
          console.log('fetching file: ', field);
          let change_history: ChangeHistory[] = [
            {
              uploaded: true,
              blob_name: this.content.title,
            }
          ];
          let selected_img: IFImage = {
            field: field,
            change_history: change_history,
            blob: null,
            image: null,
          };
          this.file_helper.getFile(field.value).subscribe(file => {
            selected_img.image = file.url;
            selected_img.blob = file.blob;
            this.selected_images.push(selected_img);
            files_to_fetch = files_to_fetch.filter(item => {
              return item.id !== field.id;
            });

            //fetch next file:
            if(files_to_fetch.length){
              files_sub.next(files_to_fetch[0]);
            }
          },err => {
          });
        });
        // start the loop
        files_sub.next(field);
      }else{
        files_to_fetch.push(field);
        console.log('new field to fetch');
      }
    };
    let selectFile = (field:IField) => {
      let change_history: ChangeHistory[] = [
        {
          uploaded: true,
          blob_name: this.content.title,
        }
      ];
      if(field.type === 'image'){
        fetchFile(field);
      }else{
        let selected_file: IFFile = {
          field: field,
          change_history: change_history,
          blob: null,
        };
        this.selected_files.push(selected_file);
      }
    };
    for(let field_val of this.content.values){
      for(let section of this.selected_type.sections){
        for(let field of section.fields){
          if(field_val.field_id === field.id){
            field.value = field_val.value;
            if(field.type === 'image' || field.type === 'file'){
              selectFile(field);
            }
            this.fieldValChanged(field);
          }
        }
      }
    }
  }
  validateFields(){
    const invalid_fields = [];
    for(let section of this.selected_type.sections){
      for(let field of section.fields){
        switch (field.type){
          case "location":
            if(!field.hidden_val){
              invalid_fields.push(field);
            }
        }
      }
    }
    if(invalid_fields.length){
      for(let field of invalid_fields){
        this.notes_ser.showNote("Field " + field.label + " is invalid");
      }
    }
    return invalid_fields.length === 0;
  }
  fieldValChanged(field: IField):void{
    let found = false;
    this.filled_fields.forEach((item,index) => {
      let remove = false;
      if(item === field.id) {
        found = true;
        switch (field.type){
          case "text":
          case "text_area":
            if(!field.value.length){
              remove = true;
            }
            break;
          case "location":
            if(!field.hidden_val){
              remove = true;
            }
            break;
        }
      }
      if(remove){
        this.filled_fields.splice(index,1);
      }
    });
    if(!found){
      this.filled_fields.push(field.id);
    }
  }
  gotoNextSlide(section: ISection,index:number):void{
    let section_found = false;
    if(this.filled_sections.length){
      for(let item of this.filled_sections){
        if(item === section.id){
          section_found = true;
        }
      }
    }
    if(!section_found){
      this.filled_sections.push(section.id);
    }
    this.slidesCon.lockSwipeToNext(false);
    this.slidesCon.slideNext();
    if(this.isLastSection(index) && !this.isSliding){
      this.finish();
    }
  }
  async finish(){
    const files_valid = this.validateFileFields();
    const field_valid = this.validateFields();
    if(!files_valid || !field_valid){
      return;
    }
    let up_err = false;
    await this.uploadFiles().toPromise().catch(err => {
      up_err = true;
      this.notes_ser.showNote(err);
    });
    if(up_err){
      return;
    }
    const contentObj = this.getContentObj();
    this.onCollectFinish.emit(contentObj);
  }
  isLastSection(sec_index:number):boolean{
    return this.selected_type.sections.length === sec_index +1;
  }
  slideChanged():void{
    this.slidesCon.lockSwipeToNext(!this.isSectionFilled());
  }
  openMap(field:IField){
    this.events.subscribe('map:result',(res) => {
      field.hidden_val = res;
      field.value = res.lat.toString() + " " + res.lng.toString();
      this.fieldValChanged(field);
      this.events.unsubscribe('map:result');
    });

    if(field.value){
      this.navCtrl.push("MapPage",{coords: field.value}).catch(console.log);
    }else{
      this.navCtrl.push("MapPage").catch(console.log);
    }
  }
  loadSearch(field:IField){
    const id = field.type + '_' + field.id;
    let timeout = 0;
    let interval = setInterval(() => {
      let el = <HTMLInputElement>document.getElementById(id).getElementsByTagName('input')[0];
      if(el){
        clearInterval(interval);
        const sub = this.location_ser.placeSearch(el,field.id).subscribe(res => {
          this.searchFieldsReady[field.id] = true;
          sub.unsubscribe();
        },err => {
          console.log(err);
        });
      }
      timeout += 100;
      if(timeout > 5000){
        timeout = 0;
        clearInterval(interval);
      }
    },100);
  }
  isSectionFilled():boolean{
    let slide_index = this.slidesCon.getActiveIndex();
    let filled = false;
    for(let item of this.filled_sections){
      if(item +1 === slide_index){
        filled = true;
      }
    }
    if(slide_index === 0){
      return true;
    }else{
      return filled;
    }
  }
  prepareSections(){
    if(!this.selected_type.sections){
      this.notes_ser.showNote("Currently you can't change your profile type");
      console.log('Content type does not have sections');
      return false;
    }
    for(let section of this.selected_type.sections){
      // include only fields with 'add' permission;
      section.fields = section.fields.filter(field => {
        return field.add;
      });

      for(let field of section.fields){
        if(!this.content && field.type === 'checkbox'){
          field.value = false;
        }
        // Parse the fields details (options) object:
        if(field.options){
          let options = JSON.parse(field.options);
          if(typeof options !== "object"){
            options = JSON.parse(options);
          }
          field.options = options;
        }
        if(field.type === 'location'){
          this.loadSearch(field);
        }
      }
    }
    // split sections:
    return true;
  }
  getFieldOptions(field:IField,target_prop: string = 'options'):Array<IFieldOptions>{
    let result:IFieldOptions[] = [];
    if(field.options && field.options.hasOwnProperty(target_prop)){
      for(let key in field.options[target_prop]) {
        if(field.options[target_prop].hasOwnProperty(key)){
          result.push({
            key: key,
            value: field.options[target_prop][key]
          });
        }
      }
    }
    return result;
  }
  isMultipleSelect(field:IField):boolean{
    const keys = Object.keys(field.options);
    for(let key of keys){
      if(key === 'multiple' && field.options[key]){
        return true;
      }
    }
    return false;
  }
  showImageActionSheet(field?:IField) {
    console.log('file clicked');
    if(this.helpers.platform === 'browser'){
      // if it's browser, trigger click event for the user to select file:
      this.triggerClick(field);
    }else{
      let actionSheet = this.actionSheetCtrl.create({
        title: 'Select Image Source',
        buttons: [
          {
            text: 'Load from Library',
            handler: () => {
              this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY, field);
            }
          },
          {
            text: 'Use Camera',
            handler: () => {
              this.takePicture(this.camera.PictureSourceType.CAMERA, field);
            }
          },
          {
            text: 'Cancel',
            role: 'cancel'
          }
        ]
      });
      actionSheet.present().catch(console.log);
    }

  }
  addFile(field:IField){
    if(this.helpers.platform === 'browser'){
      this.triggerClick(field);
    }else{
      this.selectNativeFile(field).catch(console.log);
    }
  }
  takePicture(sourceType, field?:IField) {
    // Create options for the Camera Dialog
    //options are correct
    var options = {
      quality: 75,
      destinationType: this.camera.DestinationType.DATA_URL,
      mediaType: sourceType,
      sourceType:sourceType,
      encodingType: this.camera.EncodingType.JPEG,
      saveToPhotoAlbum: false,
      correctOrientation: true,
    };


    // Get the data of an image
    this.camera.getPicture(options).then((imageData) => {
      let img_blob = this.blob_ser.convertToBlob(imageData,'image/jpeg');
      let img_str = "data:image/jpeg;base64," + imageData;
      this.insertImage(img_blob,img_str,"jpeg",field);
    }, (err) => {
      this.notes_ser.showNote('Error while selecting image: ' + err.message);
    });
  }
  triggerClick(field?:IField){
    let el = null;
    if(field){
      const id = field.type + '_' + field.id;
      el = document.getElementById(id);
    }else{
      el = document.getElementById('profile_avatar');
    }
    let timeout = 0;
    const interval = setInterval(() => {
      timeout += 100;
      if(el){
        el.click();
        clearInterval(interval);
      }
      if(timeout === 5000){
        console.log('failed to get file element after 5 seconds');
        clearInterval(interval);
      }
    },100)
  }
  async selectNativeFile(field:IField){
    if(this._platform === "browser"){return}
    const uri = await this.fileChooser.open().catch(console.log);
    if(!uri){
      return false;
    }
    const fileEntry = <FileEntry>await this.natFile.resolveLocalFilesystemUrl(uri).catch(console.log);
    if(!fileEntry){
      return false;
    }
    const file = <IFile> await new Promise<IFile>((resolve, reject) => {
      fileEntry.file(resolve,reject)})
      .catch(console.log);
    if(!file){
      return false;
    }
    const ext = file.type.split('/')[1];
    if(this.isImage(ext)){
      const base64 = await this.blob_ser.getBase64(file).catch(console.log);
      this.insertImage(file,base64,ext,field);
    }else{
      this.insertFile(file,ext,field);
    }
    this.loading_ser.showLoading(false).catch(console.log);
  }
  imageIsSelected(field?:IField):boolean{
    if(field){
      let filtered = this.selected_images.filter(item => {
        return item.field.id === field.id;
      });
      return filtered.length > 0;
    }else if(this.isProfile()){
      return !!this.new_content.avatar_file;
    }else{
      return false;
    }
  }
  fileIsSelected(field:IField):boolean{
    const file_fields = this.selected_files.concat(this.selected_images);
    const found = file_fields.filter(item => {
      return item.field && item.field.id === field.id;
    });
    return found.length > 0;
  }
  getSelectedImage(field?:IField):string{
    if(field){
      let filtered = this.selected_images.filter(item => {
        return item.field.id === field.id;
      });
      return filtered[0].image;
    }else if(this.isProfile()){
      if(this.new_content.avatar_file){
        return this.new_content.avatar;
      }else{
        return this.settings.storage_url + this.new_content.avatar;
      }
    }
  }
  getSelectedFile(field:IField):IFFile{
    const file_fields = this.selected_files.concat(this.selected_images);
    const found = file_fields.filter(item => {
      return item.field && item.field.id === field.id;
    });
    return found[0];
  }
  //for browser only when the file changes:
  fileChanged(event, field?: IField){
    const isset = event.target.files && event.target.files.length;
    if(this._platform === "browser" && isset){
      const file = event.target.files[0];
      const ext = file.name.split('.').reverse()[0];

      if(this.isImage(ext)){
        this.blob_ser.getBase64(file).then((data) => {
          this.insertImage(file,data,ext,field)
        }).catch(console.log);
      }else if(field){
        this.insertFile(file,ext,field)
      }
    }else{
      console.log('platform:', this._platform);
    }
  }
  insertImage(img_blob: any, data_str: any,ext:string, field?: IField){
    if(field){
      let change_history = <ChangeHistory[]> {};
      change_history = [];
      let image_field: IFImage = {
        blob: img_blob,
        image: data_str,
        field: field,
        change_history: change_history,
        ext: ext,
      };
      this.selected_images = this.selected_images.filter(item => {
        if(item.field.id === field.id){
          image_field.change_history = item.change_history
        }
        return item.field.id !== field.id;
      });
      image_field.change_history.push({
        blob_name: img_blob.name,
        uploaded: false,
      });

      //validate file:
      if(this.extensionValid(image_field)){
        this.selected_images.push(image_field);
      }else{
        this.notes_ser.showNote('File is not valid!');
      }
    }else if(this.isProfile()){
      this.new_content.avatar = data_str;
      this.new_content.avatar_file = img_blob;
    }
  }
  insertFile(blob: any, ext: string, field: IField){
    let change_history = <ChangeHistory[]> {};
    change_history = [];
    let file_field: IFFile = {
      change_history: change_history,
      blob: blob,
      field: field,
      ext: ext,
    };
    this.selected_files = this.selected_files.filter(item => {
      if(item.field.id === field.id){
        file_field.change_history = item.change_history
      }
      return item.field.id !== field.id;
    });
    file_field.change_history.push({
      blob_name: blob.name,
      uploaded: false,
    });

    //validate file:
    if(this.extensionValid(file_field)){
      this.selected_files.push(file_field);
    }else{
      this.notes_ser.showNote('File is not valid!');
    }
  }
  validateFileFields():boolean{
    let invalid_fields = this.getInvalidFields();
    if(invalid_fields.length){
      let err_string = "Please select files for ";
      invalid_fields.forEach((invalid_f,index,arr) => {
        if((index +1) === invalid_fields.length){
          if(invalid_fields.length > 1){
            err_string += "and " + invalid_f.label + ' fields.';
          }else{
            err_string = 'Please select a file for ' + invalid_f.label + ' field.';
          }
        }else{
          err_string += invalid_f.label + ", ";
        }
      });
      this.notes_ser.showNote(err_string);
      console.log('todo: go to field section');
      return false;
    }else{
      return true;
    }
  }
  fieldsToBlob(fields: Array<any>):Array<IVFile>{
    let files_blobs: IVFile[] = [];
    fields.forEach(file_field => {
      let file = <IVFile>{
        name: file_field.field.id.toString(),
        blob: file_field.blob,
      };
      if(file_field.field.isPublic){
        file.fields = [
          {
            name: "visibility",
            value: "public",
          }
        ];
      }
      files_blobs.push(file);
    });
    return files_blobs;
  }
  getInvalidFields(uploaded: boolean = false):IField[]{
    let s_fields: Array<any> = [];
    s_fields = s_fields.concat(this.selected_images);
    s_fields = s_fields.concat(this.selected_files);
    let invalid_fields = [];
    for(let section of this.selected_type.sections){
      for(let field of section.fields){
        let invalid = false;
        if(field.type === 'file' || field.type === 'image'){
          let founded = false;
          for(let s_field of s_fields){
            if(s_field.field.id === field.id){
              founded = true;
              // if the file uploaded check if the url received
              if(uploaded){
                if(!field.value.length){
                  invalid = true;
                  console.log('field does not have value');
                }
              }else{
                // check if field doesn't have selected file:
                invalid = !s_field.blob;
                console.log('field does not have blob');
              }
            }
          }
          // check required & doesn't have value
          if(field.required && !founded){
            invalid = true;
          }
        }
        if(invalid){
          invalid_fields.push(field);
        }
      }
    }
    return invalid_fields;
  }
  getNonUploadedFields(): IVFile[]{
    let files = [];
    files = files.concat(this.selected_images);
    files = files.concat(this.selected_files);
    let non_uploaded = files.filter(file => {
      return !file.change_history[file.change_history.length -1].uploaded;
    });

    return this.fieldsToBlob(non_uploaded);
  }
  uploadFiles():Observable<any>{
    return new Observable<any>(observer => {
      let files_blobs: IVFile[] = this.getNonUploadedFields();
      //if we have files to upload, upload them, otherwise skip
      if(files_blobs.length){
        this.loading_ser.showLoading(true,'Uploading files').catch(console.log);
        //init uploader
        let options = <IVOptions>{};
        options.autoUpload = false;
        options.maxSize = 3;
        options.url = this.settings.baseUrl + 'file/upload';
        options.tokenHeader = this.auth_ser.getToken();
        options.field = 'file_field';
        this.up_ser.onFileFinish.subscribe(uploaded_file => {
          this.updateFileField(uploaded_file);
        });
        let up_err = this.up_ser.onUploadError.subscribe(file => {
          this.loading_ser.showLoading(false).catch(console.log);
          console.log('onUploadError received');
          observer.error('failed to upload ' + file.name + ': ' + file.error);
          up_err.unsubscribe();
        });
        let up_finished = this.up_ser.onAllFinished.subscribe(res => {
          this.uploaded = true;
          this.loading_ser.showLoading(false).catch(console.log);
          up_finished.unsubscribe();
          observer.next();
          observer.complete();
        });
        this.up_ser.add(files_blobs,options);
        this.up_ser.start();
      }else{
        observer.next();
        observer.complete();
      }
    });
  }
  updateFileField(file: any, uploaded: boolean = true){
    let field_id = Number(file.name);
    if(field_id){
      this.selected_type.sections.forEach(section => {
        section.fields.forEach(field => {
          if(field.id === field_id){
            console.log('updated field:', field.name);
            field.value = file.response;
          }
        })
      });
      // update selected files fields:
      this.selected_images.forEach(s_field => {
        if(s_field.field.id === field_id){
          s_field.field.value = file.response;
          if(uploaded){
            s_field.change_history[s_field.change_history.length -1].uploaded = true;
          }
        }
      });
      this.selected_files.forEach(s_field => {
        if(s_field.field.id === field_id){
          s_field.field.value = file.response;
          s_field.change_history[s_field.change_history.length -1].uploaded = true;
        }
      });
    }else if(this.isProfile() && file.name === "avatar"){
      this.new_content.avatar = file.response;
    }
  }
  extensionValid(file:IFFile | IFImage):boolean{
    let extensions = this.getFieldOptions(file.field,'allowed_extensions');
    let valid = true;

    // if field has allowed extensions, validate against them:
    if(extensions.length){
      let founded = extensions.filter(ext => {
        return ext.value === file.ext;
      });
      if(!founded.length){
        valid = false;
      }
    }else if(file.field.type === 'image' && !this.isImage(file.ext)){
      valid = false;
    }
    return valid;
  }
  getContentObj():IContent{
    let fields_values: IFieldVal[] = [];
    let getValueId = (field_id: number) => {
      if(this.content){
        let founded = this.content.values.filter(value => {
          return value.field_id === field_id;
        });
        if(founded.length){
          return founded[0].id
        }else{
          return null;
        }
      }else{
        return null;
      }
    };

    for(let section of this.selected_type.sections){
      for(let field of section.fields){
        let include_field = true;
        const str = field.type === 'text' || field.type === 'text_area';
        if(str){
          if(!field.value.length){
            include_field = false;
          }
        }
        if(include_field){
          let field_value = <IFieldVal>{
            id: getValueId(field.id),
            field_id: field.id,
            value: field.hidden_val? field.hidden_val: field.value,
          };
          const date_types = [
            'date',
            'start_date',
            'end_date',
            'timestamp'
          ];
          let isDate = false;
          for(let dType of date_types){
            if(dType === field.type){
              isDate = true;
            }
          }
          if(isDate){
            let val = null;
            if(field.type === 'date'){
              val = this.helpers.toUnix(field.value);
            }else{
              let val = this.helpers.ionDateToUnix(field.value);
            }
            const rev = this.helpers.fromUnix(val);
            console.log('date value:', rev);
            field_value.value = val;
          }
          fields_values.push(field_value);
        }
      }
    }
    this.new_content.content_type_id = this.selected_type.id;
    let content_obj = <IContent>{};
    // check if content variables are set
    const keys = Object.keys(this.new_content);
    for(let key of keys){
      if(this.new_content[key] && this.new_content[key] !== undefined){
        content_obj[key] = this.new_content[key];
      }
    }
    content_obj.values = fields_values;
    return content_obj
  }
  isTypeDate(field: IField){
    const date_types = [
      'date',
      'start_date',
      'end_date',
      'timestamp'
    ];
    let found = date_types.filter(item => {
      return item === field.type;
    });
    return found.length > 0;
  }
  isImage(ext:string):boolean{
    return ext === "jpg" || ext === 'jpeg' || ext === 'png' || ext === 'bmp';
  }
  isFileField(field:IField):boolean{
    return field.type === 'image' || field.type === 'file';
  }
  cleanName(field:IField):string{
    return field.name.replace(' ','_').toLowerCase();
  }
  canFloat(field:IField):boolean{
    if(field.type === 'checkbox'){
      return false;
    }else{
      return true;
    }
  }
  isProfile():boolean{
    if(this.selected_type.parent_id){
      return this.selected_type.parent_id === this.settings.profile_type_id;
    }else{
      return this.selected_type.id === this.settings.profile_type_id;
    }
  }
}
