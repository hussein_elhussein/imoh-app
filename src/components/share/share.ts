import {Component, Input} from '@angular/core';
import {SocialSharing} from "@ionic-native/social-sharing";
export interface ShareObject {
  message?: string;
  subject?: string;
  file?: any;
  url?: string;
}

@Component({
  selector: 'share',
  templateUrl: 'share.html',
  styles: [`    
    :host ion-fab { position: fixed; bottom: 12%;right:5%;}
  `]
})
export class ShareComponent {
  @Input()
  shareable: ShareObject;
  constructor(private socialSharing:SocialSharing) {}
  share(){
    if(this.shareable){
      console.log('share clicked');
      this.socialSharing
        .share(this.shareable.message,this.shareable.subject,this.shareable.file,this.shareable.url)
        .then(() => {
          console.log('shared');
        })
        .catch(console.log);
    }else{
      console.log('shareable object not initialized');
    }
  }
}
