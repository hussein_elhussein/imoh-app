import {Component, Input, OnInit} from '@angular/core';
import {INotification} from "../../interfaces/INotification";
import {IContentUpdate} from "../../interfaces/IContentUpdate";
import {IContentType} from "../../interfaces/IContentType";
import {AlertController, Events, ModalController, NavController} from "ionic-angular";
import {LoadingService} from "../../providers/loading.service";
import {ContentUpdateService} from "../../providers/content-update.service";
import {IProfile} from "../../interfaces/IProfile";
import {TakeActionPage} from "../../pages/take-action/take-action";
import {HelpersService} from "../../providers/helpers.service";
import {SocketService} from "../../providers/socket.service";

@Component({
  selector: 'content-update-notification',
  templateUrl: 'content-update-notification.html'
})
export class ContentUpdateNotificationComponent implements OnInit{
  @Input()
  types:IContentType[];

  @Input()
  item: INotification;

  @Input()
  profile: IProfile;
  constructor(private alertCtrl:AlertController,
              private loading_ser:LoadingService,
              private con_up_ser:ContentUpdateService,
              private helpers:HelpersService,
              private nav:NavController,
              private modalCtrl:ModalController,
              private events: Events,
              private socket_ser:SocketService) {
  }
  ngOnInit(){
    this.events.subscribe('notifications:update',data => {
      if(data.id){
        if(this.item.data.original.id === data.id){
          this.item.data.original.actions_allowed = data.actions_allowed;
        }
      }
    });
  }
  isOffered():boolean{
    return this.con_up_ser.isOffered(this.item.data.original,this.types);
  }
  showAlert(accept: boolean) {
    const prompt = this.alertCtrl.create({
      title: 'Are you sure?',
      message: accept? "Are you sure you want to accept?": "Are you sure you want to reject?",
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Yes',
          handler: data => {
            this.accept(accept);
          }
        }
      ]
    });
    prompt.present().catch(console.log);
  }
  accept(accept: boolean = true){
    (async () => {
      const update = this.initUpdateObj(accept);
      if(update.office_id && !this.isOfficeOwner() && update.status !== 16){
        setTimeout(() => {
          this.displayAppointmentPanel();
        },1000);
        return;
      }
      const save_res = await this.con_up_ser.save(update.content_id,update).toPromise().catch(console.log);
      if(!save_res){
        this.loading_ser.showLoading(false).catch(console.log);
        console.log("Failed to save content update:", save_res);
        return;
      }
      this.item.data.original.children_count = 1;
      this.item.data.original.actions_allowed = false;
      this.loading_ser.showLoading(false).catch(console.log);
      this.nav.pop().catch(console.log);
    })();
  }
  initUpdateObj(accept:boolean = true):IContentUpdate{
    //this.loading_ser.showLoading(true,'Please wait..').catch(console.log);
    const _update = <IContentUpdate>this.item.data.original;
    const content_update: IContentUpdate = {
      previous_id: _update.id,
      content_id: _update.content_id,
      parent_id: _update.id,
      direct: _update.direct,
      status: null,
      actions_allowed: false,
      details: _update.details,
      scheduled: _update.scheduled,
      schedule: _update.schedule,
      modified: false,
      lat: _update.lat,
      lng:_update.lng,
    };
    if(_update.parent_id){
      content_update.parent_id = _update.parent_id;
    }
    if(_update.office){
      content_update.office_id = _update.office.id;
    }
    if(accept){
      // if it's a meeting request:
      if(_update.status === 15){
        content_update.status = 16;
      }else{
        content_update.status = this.isOfficeOwner()? 13: 2;
      }
    }else{
      // if it's a meeting request:
      if(_update.status === 15){
        content_update.status = 17;
      }else{
        content_update.status = this.isOfficeOwner()? 14: 4;
      }
    }
    return content_update;
  }
  edit(){
    if(this.helpers.platform === 'browser'){
      let modal = this.modalCtrl.create("TakeActionPage",{update: this.item.data.original});
      modal.present().catch(console.log);
    }else{
      this.nav.push("TakeActionPage",{update: this.item.data.original}).catch(console.log);
    }
  }
  displayAppointmentPanel(){
    const update = <IContentUpdate>this.helpers.clone(this.item.data.original);
    update.status = 12;
    if(this.helpers.platform === 'browser'){
      let modal = this.modalCtrl.create("TakeActionPage",{update: update});
      modal.present().catch(console.log);
      console.log('TakeActionPage pushed');
    }else{
      this.nav.push("TakeActionPage",{update: update}).catch(console.log);
    }
  }
  isDirect():boolean{
    return this.con_up_ser.isDirect(this.item.data.original,this.types);
  }
  isAccepted():boolean{
    return this.con_up_ser.isAccepted(this.item.data.original);
  }
  isResolved():boolean{
    return this.con_up_ser.isResolved(this.item.data.original);
  }
  getStatusText():string{
    return this.con_up_ser.getStatusText(this.item.data.original,this.types,this.profile);
  }
  isAuthor():boolean{
    return this.con_up_ser.isAuthor(this.profile,this.item.data.original);
  }
  isOfficeOwner():boolean{
    const update = <IContentUpdate>this.item.data.original;
    const officeStat = update.status === 12 || update.status === 13;
    if(this.profile && officeStat){
      return this.profile.id === update.office.profile_id;
    }
    return false;
  }

}

