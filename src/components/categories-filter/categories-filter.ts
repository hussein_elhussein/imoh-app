import { Component } from '@angular/core';
import { NavParams, ViewController } from 'ionic-angular';
import {ICategory} from "../../interfaces/ICategory";


@Component({
  selector: 'page-categories-filter',
  templateUrl: 'categories-filter.html'
})
export class CategoriesFilter {
  cats:ICategory[];
  selected_parents:ICategory[];
  selected_childs:ICategory[];
  selected_cats:ICategory[];

  constructor
  (
    public navParams: NavParams,
    public viewCtrl: ViewController
  )
  {
    this.cats =  navParams.get('categories');
    this.selected_parents = navParams.get('selected_cats');
    this.selected_childs = [];
    this.selected_cats = [];
  }
  resetFilters() {
    this.selected_parents = [];
    this.selected_childs = [];
    this.selected_cats = [];
  }

  applyFilters() {
    this.selected_parents.forEach(item => {
      this.selected_cats.push(item);
    });
    this.selected_childs.forEach(item => {
      this.selected_cats.push(item);
    });

    this.dismiss({
      "categories":this.selected_cats,
    });
  }

  dismiss(data?: any) {
    this.viewCtrl.dismiss(data);
  }
}
