import {NgModule} from "@angular/core";
import {IonicModule, IonicPageModule} from "ionic-angular";
import {MapPage} from "./map";

@NgModule({
  declarations:[MapPage],
  imports:[
    IonicPageModule.forChild(MapPage),
    IonicModule
  ],
  exports:[MapPage]
})
export class MapPageModule{}