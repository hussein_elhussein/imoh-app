import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {Events, NavParams, ViewController} from "ionic-angular";
import {Storage} from "@ionic/storage";
import {HelpersService} from "../../providers/helpers.service";
import {LocationHelperService} from "../../providers/location-helper.service";
import {ILocation} from "../../interfaces/ILocation";
import {IonicPage} from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-map',
  templateUrl: 'map.html'
})
export class MapPage implements OnInit{
  @ViewChild('map')
  mapElement: ElementRef;
  @Input()
  embed: boolean;
  @Input()
  lat?: number;
  @Input()
  lng?: number;
  @Input()
  zoom?: number;
  @Input()
  disable_drag: boolean;
  @Input()
  height?: string;
  map_ready: boolean;
  constructor(
    private navParams: NavParams,
    private viewCtrl: ViewController,
    private storage:Storage,
    private helpers:HelpersService,
    private location_ser:LocationHelperService,
    private events: Events) {
  }
  ngOnInit(){
    if(this.embed){
      this.loadMap();
    }else{
      this.disable_drag = this.navParams.get('disable_drag');
      if(this.navParams.get('coords')){
        let coords = this.navParams.get('coords');
        this.lat = coords.lat;
        this.lng = coords.lng;
      }
    }
  }
  ionViewDidLoad(){
    this.loadMap();
  }
  ionViewDidLeave(){
    this.location_ser.removeMap();
  }
  loadMap(){
    let doLoad = async () => {
      let el = this.mapElement.nativeElement;
      if(!this.lat){
        const location = <ILocation> await this.location_ser.getLocation().toPromise().catch(console.log);
        if(location){
          let lat = location.coords.latitude;
          let lng = location.coords.longitude;
          this.lat = lat;
          this.lng = lng;
        }
      }
      let zoom = this.zoom? this.zoom: 12;
      await this.location_ser.createMap(el,
        this.lat,
        this.lng,
        false,
        true,
        null,
        !this.disable_drag,zoom).toPromise().catch(console.log);
      this.map_ready = true;
      if(!this.disable_drag){
        this.listenToMapEvents();
      }
    };
    let interval = setInterval(() => {
      if(this.mapElement !== undefined){
        clearInterval(interval);
        doLoad().catch(console.log);
      }
    },100)
  }
  listenToMapEvents():void{
    this.location_ser.onMarkerDragEnd.subscribe((res) => {
      this.lat = res.lat;
      this.lng = res.lng;
    });
    this.location_ser.onMapClick.subscribe(res => {
      this.lat = res.lat;
      this.lng = res.lng;
      this.location_ser.updateMarker(res.lat,res.lng);
    },err => {
      console.log('onMapClick error:', err);
    });
  }
  sendLocation(){
    this.events.publish("map:result",{lat: this.lat,lng: this.lng});
    this.viewCtrl.dismiss();
  }
  cancel(){
    this.viewCtrl.dismiss();
  }
}
