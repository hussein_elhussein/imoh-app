import { Component } from '@angular/core';
import {IonicPage, NavParams, ViewController} from 'ionic-angular';
import {ICategory} from "../../../interfaces/ICategory";
import {IContentType} from "../../../interfaces/IContentType";

@IonicPage()
@Component({
  selector: 'page-content-filter',
  templateUrl: 'filter.html'
})
export class ContentFilterPage {
  cats:ICategory[];
  selected_cats:ICategory[];
  content_types: IContentType[];
  selected_type: number;
  constructor
  (
    public navParams: NavParams,
    public viewCtrl: ViewController
  )
  {
    this.cats =  navParams.get('categories');
    this.selected_cats = navParams.get('selected_cats');
    this.content_types =  navParams.get('content_types');
    this.selected_type = navParams.get('selected_type');
    console.log(this.content_types);
  }
  resetFilters() {
    this.selected_cats = [];
    this.selected_type = null;
  }

  applyFilters() {
    let data = [];
    data['categories'] = this.selected_cats;
    if(this.selected_type){
      data['selected_type'] = this.selected_type;
    }
    this.dismiss(data);
  }

  dismiss(data?: any) {
    console.log('selected_type: ', this.selected_type);
    this.viewCtrl.dismiss(data);
  }
}
