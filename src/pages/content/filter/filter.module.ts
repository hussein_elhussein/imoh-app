import {NgModule} from "@angular/core";
import {IonicModule, IonicPageModule} from "ionic-angular";
import {ContentFilterPage} from "./filter";
@NgModule({
  declarations: [ContentFilterPage,],
  imports:[
    IonicPageModule.forChild(ContentFilterPage),
    IonicModule
  ]
})
export class ContentFilterPageModule {}