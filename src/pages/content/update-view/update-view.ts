import {AfterViewInit, Component, ElementRef, OnInit} from '@angular/core';
import {ActionSheetController, AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {ContentUpdateService} from "../../../providers/content-update.service";
import {IContentUpdate} from "../../../interfaces/IContentUpdate";
import {IContentType} from "../../../interfaces/IContentType";
import {HelpersService} from "../../../providers/helpers.service";
import {Storage} from "@ionic/storage";
import {MapPage} from "../../map/map";
import {IProfile} from "../../../interfaces/IProfile";

@IonicPage()
@Component({
  selector: 'page-update-view',
  templateUrl: 'update-view.html',
})
export class UpdateViewPage implements OnInit,AfterViewInit{
  id: number;
  item:IContentUpdate;
  types:IContentType[];
  profile:IProfile;
  is_direct: boolean;
  offered:boolean;
  loading: boolean;
  browser:boolean;
  map_height: number;
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private con_up_ser:ContentUpdateService,
              private helpers: HelpersService,
              private storage:Storage,
              private actionSheetCtrl:ActionSheetController,
              private elementRef:ElementRef,
              private alertCtrl:AlertController) {
    this.item = this.navParams.get('update');
    this.types = this.navParams.get('types');
    this.profile = this.navParams.get('profile');
    this.loading = true;
    this.browser = this.helpers.platform === 'browser';
  }
  ngOnInit(){
    this.initPage();
  }
  ngAfterViewInit(){
    this.getMapHeight();
  }
  initPage(){
    this.loadContentType();
    this.loading = false;
  }
  loadContentType(){
    for(let type of this.types){
      if(type.id === this.item.content.content_type_id){
        this.is_direct = this.isDirect();
        this.offered = this.con_up_ser.isOffered(this.item,this.types);
      }
    }
  }
  getStatusText():string{
    return this.con_up_ser.getStatusText(this.item,this.types,this.profile);
  }
  getSchedule():string{
    return this.helpers.fromUnixNoFormat(this.item.schedule.toString()).calendar();
  }
  showAlert() {
    const prompt = this.alertCtrl.create({
      title: 'Are you sure?',
      message: "Are you sure you want to cancel this meeting?",
      buttons: [
        {
          text: 'No',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.cancel();
          }
        }
      ]
    });
    prompt.present();
  }
  cancel(){

  }
  isDirect(): boolean{
    return this.con_up_ser.isDirect(this.item,this.types);
  }
  isExpired():boolean{
    let schedule = this.helpers.toIso(this.item.schedule.toString());
    return this.helpers.isBefore(schedule);
  }
  isAccepted():boolean{
    return this.con_up_ser.isAccepted(this.item);
  }
  openMap(){
    let coords = {
      lat: this.item.lat,
      lng: this.item.lng,
    };
    if(!this.is_direct){
      coords.lat = this.item.office.lat;
      coords.lng = this.item.office.lng;
    }
    this.navCtrl.push(MapPage,{coords: coords,disable_drag: true});
  }
  presentActions():void{
    let sheetContent = {
      title: 'Actions',
      buttons: [
        {
          text: 'Accept',
          icon: 'checkmark',
          handler: () => {

          }
        },
        {
          text: 'Reject',
          icon: 'trash',
          handler: () => {
            //this.navCtrl.push(ServicesAddPage, {service: this.service});
          }
        },
        {
          text: 'Edit',
          icon: 'create',
          handler: () => {
            //this.navCtrl.push(ServicesAddPage, {service: this.service});
          }
        },
        {
          text: 'Cancel',
          icon: 'close-circle',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    };
    let actionSheet = this.actionSheetCtrl.create(sheetContent);
    actionSheet.present();
  }
  getMapHeight(){
    let el_height = this.elementRef.nativeElement.offsetHeight;
    this.map_height = this.helpers.percent(el_height,65);
    console.log('map height: ', this.map_height);
  }
}
