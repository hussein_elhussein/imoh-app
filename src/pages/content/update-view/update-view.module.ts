import {NgModule} from "@angular/core";
import {IonicModule, IonicPageModule} from "ionic-angular";
import {UpdateViewPage} from "./update-view";
import {MapPageModule} from "../../map/map.module";
@NgModule({
  declarations: [UpdateViewPage],
  imports:[
    IonicPageModule.forChild(UpdateViewPage),
    IonicModule,
    MapPageModule,
  ]
})
export class UpdateViewPageModule {}