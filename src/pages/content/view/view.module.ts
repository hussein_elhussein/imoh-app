import {NgModule} from "@angular/core";
import {IonicModule, IonicPageModule} from "ionic-angular";
import {ContentViewPage} from "./view";
import {PipesModule} from "../../../pipes/pipes.module";
@NgModule({
  declarations: [ContentViewPage],
  imports:[
    IonicPageModule.forChild(ContentViewPage),
    PipesModule,
    IonicModule,
  ]
})
export class ContentViewPageModule {}