import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ActionSheetController, App, IonicPage, ModalController, NavController, NavParams} from "ionic-angular";
import {SettingsService} from "../../../providers/settings.service";
import {Spherical} from "@ionic-native/google-maps";
import {ContentService} from "../../../providers/content.service";
import {ProfileService} from "../../../providers/profile.service";
import {HelpersService} from "../../../providers/helpers.service";
import {IAuthor} from "../../../interfaces/IAuthor";
import {GoogleMapsAPIWrapper, MapsAPILoader} from "@agm/core";
import {LocationHelperService} from "../../../providers/location-helper.service";
import {IContent} from "../../../interfaces/IContent";
import {Storage} from "@ionic/storage";
import {IContentType} from "../../../interfaces/IContentType";
import {ICategory} from "../../../interfaces/ICategory";
import {LoadingService} from "../../../providers/loading.service";
import {IContentUpdate} from "../../../interfaces/IContentUpdate";
import {IRole} from "../../../interfaces/IRole";
declare var google;

@IonicPage()
@Component({
  selector: 'page-content-view',
  templateUrl: 'view.html'
})
export class ContentViewPage implements OnInit{
  @ViewChild('map') mapElement: ElementRef;
  content:IContent;
  platform: string;
  map_ready: boolean;
  content_type:IContentType;
  types: IContentType[];
  categories: ICategory[];
  loading: boolean;
  roles:IRole[];
  constructor(
    private navCtrl:NavController,
    private navParams:NavParams,
    private settings:SettingsService,
    private spherical: Spherical,
    private ser:ContentService,
    private profile_ser:ProfileService,
    private app:App,
    private actionSheetCtrl:ActionSheetController,
    private helpers:HelpersService,
    private gmap_loader:MapsAPILoader,
    private gmap_browser:GoogleMapsAPIWrapper,
    private location_ser:LocationHelperService,
    private loading_ser:LoadingService,
    private modalCtrl: ModalController,
    private storage:Storage,
  ) {
    this.content = this.navParams.get('content');
    this.map_ready = false;
    this.categories = [];
    this.types = [];
    this.roles = [];
    this.loading = true;
  }
  ngOnInit(){
  }
  ionViewDidLoad() {
    this.platform = this.helpers.platform;
    if(this.content){
      this.initPage().catch(console.log);
    }
  }
  ionViewDidLeave(){
    this.location_ser.removeMap();
  }
  async initPage(){
    const types = await this.storage.get('content_types').catch(console.log);
    if(!types){
      console.log('Failed to load content types');
      return;
    }
    this.types = this.helpers.flattenObjects(types,'children');
    this.roles = await this.storage.get('roles').catch(console.log);
    const target_type = this.types.filter(item => {
      return item.id === this.content.content_type_id;
    })[0];
    this.content_type = target_type;
    if(target_type.show_map){
      setTimeout(() => {
        this.loadMap();
      },200)
    }
    const categories = await this.storage.get('categories').catch(console.log);
    if(!categories){
      console.log('failed to load categories');
      return;
    }
    this.categories = categories;
    this.loading = false;
  }
  loadMap():void{
    let el = this.mapElement.nativeElement;
    let lat = this.content.lat;
    let lng = this.content.lng;
    this.location_ser.createMap(el,lat,lng).subscribe(_map => {
      this.map_ready = true;
      console.log('map created');
    },err => {
      console.log(err);
    });
  }
  takeAction():void{
    //todo: fix taking action
    // let modal = this.modalCtrl.create(TakeActionPage,{service: this.service});
    // modal.present();
  }
  getServiceTitle():string{
    //todo: fix getting title
    return '';
    //return this.ser.getServiceTitle(this.service);
  }
  getServiceBody():string{
    //todo: fix getting body
    return '';
    //return this.ser.getServiceBody(this.service);
  }
  hasCategories(): boolean{
    const doHave = this.content.categories && this.content.categories.length;
    return !!doHave;
  }
  getCategory(id: number):ICategory{
    let found = this.categories.filter(item => {
      return item.id === id;
    });
    return found[0];
  }
  getName():string{
    if(this.content.author){
      return this.profile_ser.getName(this.content.author);
    }else{
      return;
    }
  }
  getFullURL(url:string):string{
    return this.settings.storage_url + url;
  }
  getDefaultImage(){
    return '/assets/imgs/default.png';
  }
  presentActions():void{
    let sheetContent = {
      title: 'Actions',
      buttons: [
        {
          text: 'Edit Service',
          icon: 'create',
          role: 'destructive',
          handler: () => {
            this.navCtrl.push("ContentEditAddPage", {content: this.content})
              .catch(console.log);
          }
        },
        {
          text: 'Cancel',
          icon: 'close-circle',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    };
    let actionSheet = this.actionSheetCtrl.create(sheetContent);
    actionSheet.present();
  }
  getUpdateText(update:IContentUpdate):string{
    let text;
    switch (update.status){
      case 1:
        text = "Waiting for acceptance";
        break;
      case 2:
        text =  "Accepted";
        break;
      case 3:
        text =  "Rejected";
        break;
      case 4:
        text =  "Agreeing on a meeting";
        break;
      case 5:
        text =  "Edited";
        break;
      case 6:
        text = "Organization received";
        break;
      case 7:
        text = "Received";
        break;
      case 8:
        text = "Delivered";
        break;
      case 9:
        text = "Received";
        break;
      default:
        text = "Unknown update";
    }
    return text;
  }
  getRole(author:IAuthor):string{
    let target_type_id = null;
    if(author.isOffice){
      target_type_id = this.settings.organization_type_id;
    }else{
      target_type_id = author.content_type_id;
    }
    for(let type of this.types){
      if(type.id === target_type_id){
        for(let role of this.roles){
          if(role.id === type.role_id){
            return role.display_name;
          }
        }
        return type.type;
      }
    }
  }
  isService(): boolean{
    if(this.content_type.id === this.settings.service_type_id){
      return true;
    }else if(this.content_type.parent_id === this.settings.service_type_id){
      return true;
    }else{
      return false;
    }
  }


}
