import {NgModule} from "@angular/core";
import {IonicModule, IonicPageModule} from "ionic-angular";
import {ContentIndexPage} from "./index";
import {ComponentsModule} from "../../../components/components.module";
@NgModule({
  declarations: [ContentIndexPage],
  imports:[
    ComponentsModule,
    IonicPageModule.forChild(ContentIndexPage),
    IonicModule,
  ],
})
export class ContentIndexPageModule {}