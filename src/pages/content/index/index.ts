import {Component, forwardRef, OnInit} from '@angular/core';
import {
  ActionSheetController,
  Events, IonicPage,
  LoadingController,
  ModalController,
  NavController,
  NavParams, Platform, PopoverController
} from "ionic-angular";
import {ContentService} from "../../../providers/content.service";
import {IService} from "../../../interfaces/IService";
import {SettingsService} from "../../../providers/settings.service";
import {Storage} from "@ionic/storage";
import {IPagination} from "../../../interfaces/IPagination";
import {ProfileService} from "../../../providers/profile.service";
import {ILocationAuthor} from "../../../interfaces/ILocationAuthor";
import {LoadingService} from "../../../providers/loading.service";
import {ICategory} from "../../../interfaces/ICategory";
import {PaginationService} from "../../../providers/pagination.service";
import {IContentType} from "../../../interfaces/IContentType";
import {ICountry} from "../../../interfaces/ICountry";
import {LocationHelperService} from "../../../providers/location-helper.service";
import {IContent} from "../../../interfaces/IContent";
import {NotesService} from "../../../providers/notes.service";
import {HelpersService} from "../../../providers/helpers.service";
import {NotificationService} from "../../../providers/notification.service";
import {IProfile} from "../../../interfaces/IProfile";
import {AuthService} from "../../../providers/auth.service";
import {IContentFilter} from "../../../interfaces/IContentFilter";
@IonicPage()
@Component({
  selector: 'page-content-index',
  templateUrl: 'index.html'
})
export class ContentIndexPage implements OnInit{
  language:string;
  sheet:any;
  categories: ICategory[];
  selected_categories: ICategory[];
  selected_type: number;
  contents: IContent[];
  pagination: IPagination;
  filtered: boolean;
  backUpContents: IContent[];
  searchInput: string;
  shouldShowCancel:boolean;
  country:ICountry;
  type: IContentType;
  profile: IProfile;
  loading: boolean;
  conFilter: IContentFilter;
  constructor(
    private plaform: Platform,
    private actionSheetCtrl:ActionSheetController,
    private nav:NavController,
    private navParams: NavParams,
    private settings:SettingsService,
    private content_ser:ContentService,
    public storage:Storage,
    private profile_ser:ProfileService,
    public events:Events,
    public loading_ser:LoadingService,
    public pag_ser:PaginationService,
    private modalCtrl:ModalController,
    private location_ser:LocationHelperService,
    private note_ser:NotesService,
    private helpers: HelpersService,
    private popOver:PopoverController,
    private not_ser:NotificationService,
    private auth_ser:AuthService,
    ) {
    this.categories = [];
    this.selected_categories = [];
    this.selected_type = null;
    this.backUpContents = [];
    this.contents = [];
    this.filtered = false;
    this.searchInput = '';
    this.shouldShowCancel = false;
    this.type = navParams.get('type');
    this.loading = true;
  }
  ngOnInit(){
    // todo: user can't take action to his own services
    this.initPage().catch(console.log);
    this.events.subscribe('navigate:content', (id) => {
      this.navigateToContent(id).catch(console.log)
    })
  }
  async navigateToContent(id: number){
    await this.nav.popAll().catch(console.log);
    this.getItems(null,() => {
      this.loading = false;

      setTimeout(() => {
        this.viewContent(id);
      },1000);
    });
  }
  async initPage(){
    console.log('waiting for platform.ready');
    await this.plaform.ready().catch(console.log);
    console.log('waiting for profile');
    const profile = await this.auth_ser.getCurrentProfile().catch(console.log);
    if(profile){
      this.profile = profile;
    }
    await this.loadTypeChildren().catch(console.log);
    if(this.requiresLocation()){
      console.log('waiting for location');
      const loc = await this.location_ser.getLocation().toPromise().catch(console.log);
      if(loc){
        this.country = loc.country;
      }else{
        this.note_ser.showNote('Unable to get location!');
      }
    }
    console.log('waiting for categories');
    const categories = <ICategory[]> await this.storage.get('categories').catch(console.log);
    if(categories){
      this.categories = categories;
    }
    this.getItems(null, () => {
      this.loading = false;
    });
  }
  async loadTypeChildren(){
    const types = <IContentType[]>await this.storage.get('content_types').catch(console.log);
    this.type.children = types.filter(item => {
      return item.parent_id && item.parent_id === this.type.id;
    });

    for(let child of this.type.children){
      for(let type of types){
        if(type.parent_id === child.id){
          this.type.children.push(type);
        }
      }
    }
    console.log('children types:', this.type.children);
  }
  isAuthor(item:IContent):boolean{
    if(!this.profile){
      return false;
    }
    return item.author.id === this.profile.id;
  }
  requiresLocation():boolean{
    const type = this.type.type.toLowerCase();
    return !!type.match('service');
  }
  getItems(refresher?, callback?):void{
    console.log('getting items');
    let finish = () => {
      if(refresher){
        refresher.complete();
      }
      if(callback){
        callback();
      }
    };
    let initItems = (pagination:IPagination) => {
      if(this.filtered){
        for(let item of pagination.data){
          this.backUpContents.push(item);
        }
        this.contents = this.filterByCats(this.selected_categories,this.backUpContents);
      }else{
        for(let item of pagination.data){
          this.contents.push(item);
        }
        this.backUpContents = this.contents;
      }
      this.contents = pagination.data;
      this.pagination = pagination;
      this.pagination.data = null;
      finish();
    };
    const types_ids = [];
    for(let type of this.type.children){
      types_ids.push(type.id);
    }
    const filter = <IContentFilter>{
      contentTypes: types_ids,
    };
    if(types_ids.length){
      filter.pagination = true;
      filter.minimal = false;
      if(this.country && this.country.name){
        filter.location = {};
        filter.location.country = <ICountry>{};
        filter.location.country.name = this.country.name;
        this.content_ser.getFilteredContent(filter)
          .subscribe((pag: IPagination) => initItems(pag),finish);
      }else{
        this.content_ser.getFilteredContent(filter)
          .subscribe((res:IPagination) => initItems(res),finish);
      }
      this.conFilter = filter;
    }else{
      console.log('content type does not have children');
      finish();
    }

  }
  addContent(){
    let showErr = () => {
      console.log('Content type is not defined:', this.type);
      this.note_ser.showNote("You can't add content");
    };
    if(this.type){
      if(this.type && this.type.children){
        this.presentActionSheet(this.type.children);
        console.log('types:', this.type);
      }else{
        showErr();
      }
    }else{
      showErr();
    }
  }
  presentActionSheet(types?: IContentType[]) {
    if(types.length > 1){
      let options = {
        title: 'Post a service',
        buttons: null,
      };
      let buttons = [];
      for(let type of types){
        let button = {
            text: type.type,
            handler: ()=> this.doAddConent(type)
          };
        buttons.push(button);
      }
      let button = {
        text: 'Cancel',
        role: 'cancel',
        handler: () => {}
      };
      buttons.push(button);
      options.buttons = buttons;
      this.sheet = this.actionSheetCtrl.create(options);
      this.sheet.present();
    }else{
      this.doAddConent(types[0]);
    }
  }
  loadNext(infiniteScroll):Promise<any>{
    return new Promise((resolve,reject) => {
      if(this.pagination.meta.last_page != this.pagination.meta.current_page){
        this.pag_ser.next(this.pagination.links.next,this.conFilter).subscribe(res => {
          for(let item of res.data){
            this.contents.push(item);
          }
          this.pagination = res;
          this.pagination.data = null;
          resolve();
          infiniteScroll.complete();
        },err => {
          infiniteScroll.complete();
          console.log(err);
        });
      }else{
        resolve();
        infiniteScroll.complete();
      }
    });
  }
  viewContent(id: number):void{
    this.loading_ser.showLoading(true,'Loading service..').catch(console.log);
    this.content_ser.getContent(id).subscribe(content => {
      this.loading_ser.showLoading(false).catch(console.log);
      this.nav.push("ContentViewPage",{content:content}).catch(console.log);
    }, err => {
      this.loading_ser.showLoading(false).catch(console.log);
    });
  }
  doAddConent(type: IContentType):void{
    this.nav.push("ContentEditAddPage",{type: type}).catch(console.log);
  }
  takeAction(id: number):void{
    if(this.helpers.platform === 'browser'){
      let modal = this.modalCtrl.create("TakeActionPage",{id: id});
      modal.present().catch(console.log);
    }else{
      this.nav.push("TakeActionPage",{id: id}).catch(console.log);
    }

  }
  getCategory(id: number):ICategory{
    let found = this.categories.filter(item => {
      return parseInt(item.id.toString()) === id;
    });
    return found[0];
  }
  getFullURL(url:string):string{
    return this.settings.storage_url + url;
  }
  getServiceTitle(service:IService):string{
    //todo: fix getting service title:
    return '';
    //return this.ser.getServiceTitle(service);
  }
  getServiceBody(service:IService):string{
    return '';
    //todo: fix getting service body:
    //return this.ser.getServiceBody(service);
  }
  getName(author:ILocationAuthor):string{
    return this.profile_ser.getName(author);
  }
  isResolved(content:IContent){
    return content.status === 10;
  }
  presentFilter() {
    let fetched_types = [];
    let present = () => {
      // this.filtered = false;
      // let modal = this.modalCtrl.create(ContentFilterPage, {
      //   "categories":this.categories,
      //   'selected_cats': this.selected_categories,
      //   'content_types': fetched_types,
      //   'selected_type': this.selected_type,
      // });
      // modal.present();
      // modal.onWillDismiss((data) => {
      //   this.doFilter(data);
      // });
    };
    this.storage.get('content_types')
      .then(types => {
        let flatten = <IContentType[]>this.helpers.flattenObjects(types,'children');
        fetched_types = flatten.filter(item => {
          return item.parent_id == this.type.id;
        });
        present();
      })
      .catch(err => {
        console.log(err);
        present();
      });
  }
  doFilter(data:any){
    this.selected_categories = [];
    this.selected_type = null;
    let cats_filtered = false;
    let types_filtered = false;
    if(data){
      if(data.categories && data.categories.length){
        this.selected_categories = data.categories;
        this.contents = this.filterByCats(data.categories,this.backUpContents);
        this.filtered = true;
        cats_filtered = true;
      }
      if(data.selected_type){
        this.selected_type = data.selected_type;
        let target_services = cats_filtered? this.contents: this.backUpContents;
        this.contents = this.filterByTypes(data.selected_type,target_services);
        this.filtered = true;
        types_filtered = true;
      }
      if(!cats_filtered && !types_filtered){
        this.filtered = false;
        this.contents= this.backUpContents;
      }
    }else{
      this.filtered = false;
      this.contents= this.backUpContents;
    }
  }
  filterByCats(selected_cats:ICategory[],contents:IContent[]):IContent[]{
    if(selected_cats && selected_cats.length){
      let catExist = (cat: ICategory,cats:ICategory[]) => {
        if(cats && cats.length){
          let filtered = cats.filter(item => {
            if(item.children){
              let filtered_children = item.children.filter(child => {
                return child.id === cat.id;
              });
              return filtered_children.length > 0;
            }else{
              return item.id === cat.id;
            }
          });
          return filtered.length > 0;
        }else{
          return false;
        }
      };
      let filtered = contents.filter(content => {
        let content_cats = content.categories;
        let include = false;
        for(let cat of content_cats){
          include = catExist(cat,selected_cats);
        }
        //if category is not found, try to match its children:
        if(!include){
          for(let cat of content_cats){
            for(let child of cat.children){
              include = catExist(child,selected_cats);
            }
          }
        }
        return include;
      });
      console.log('filtered services: ', filtered);
      return filtered;
    }
  }
  filterByTypes(selected_type: number,contents:IContent[]):IContent[]{
    return contents.filter(item => {
      return item.content_type_id === selected_type;
    });
  }
  onSearchInput():void{
    if (this.searchInput && this.searchInput.trim() != '') {
      this.contents = this.contents.filter((item) => {
        let anInput = this.searchInput.toLowerCase();
        let title = item.title.toLowerCase();
        let ser_type = item.type.toLowerCase();
        let author_name = item.author.name.toLowerCase();
        return (
          title.indexOf(anInput) > -1
          ||
          ser_type.indexOf(anInput) > -1
          ||
          author_name.indexOf(anInput) > -1
        );
      });
      this.shouldShowCancel = true;
    }else{
      this.shouldShowCancel = false;
      this.contents = this.backUpContents;
    }
  }
  onSearchCancel(){
    this.contents = this.backUpContents;
  }

}
