import {NgModule} from "@angular/core";
import {IonicModule, IonicPageModule} from "ionic-angular";
import {ContentEditAddPage} from "./edit-add";
import {PipesModule} from "../../../pipes/pipes.module";
import {ComponentsModule} from "../../../components/components.module";
@NgModule({
  declarations: [
    ContentEditAddPage
  ],
  imports:[
    PipesModule,
    ComponentsModule,
    IonicPageModule.forChild(ContentEditAddPage),
    IonicModule
  ]
})
export class ContentEditAddPageModule {}