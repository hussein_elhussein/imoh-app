import {Component, EventEmitter, NgZone, OnInit, Output} from '@angular/core';
import {Events, IonicPage, NavController, NavParams, Platform} from 'ionic-angular';
import {Storage} from "@ionic/storage";
import {IContentType} from "../../../interfaces/IContentType";
import {ICategory} from "../../../interfaces/ICategory";
import {IContent} from "../../../interfaces/IContent";
import {StatusBar} from "@ionic-native/status-bar";
import {AuthService} from "../../../providers/auth.service";
import {LoadingService} from "../../../providers/loading.service";
import {SettingsService} from "../../../providers/settings.service";
import {ContentService} from "../../../providers/content.service";
import {NotesService} from "../../../providers/notes.service";
import {HelpersService} from "../../../providers/helpers.service";
import {LocationHelperService} from "../../../providers/location-helper.service";
import {IGroup} from "../../../interfaces/IGroup";
import {ProfileService} from "../../../providers/profile.service";
import {IProfile} from "../../../interfaces/IProfile";
import {IField} from "../../../interfaces/IField";
import {IFieldVal} from "../../../interfaces/IFieldVal";
@IonicPage({name:"ContentEditAddPage"})
@Component({
  selector: 'page-content-edit-add',
  templateUrl: 'edit-add.html',
  providers: [StatusBar]
})
export class ContentEditAddPage implements OnInit{
  main_type:IContentType;
  content: IContent;
  new_content:IContent;
  categories: ICategory[];
  original_cats: ICategory[];
  children_groups: Array<IGroup>;
  available_groups: Array<IGroup>;
  matched_type: IContentType;
  types: IContentType[];
  selected_category: ICategory;
  selected_sub_cat: ICategory;
  @Output()
  item = EventEmitter;
  type_title: string;
  _platform: string;
  default_fields:IField[];
  constructor(
    public navCtrl: NavController,
    public storage: Storage,
    private ser:ContentService,
    private loading_ser:LoadingService,
    private navParams: NavParams,
    public platform:Platform,
    public events:Events,
    private settings:SettingsService,
    private auth_ser: AuthService,
    public notes_ser:NotesService,
    public helpers:HelpersService,
    private location_ser:LocationHelperService,
    private profile_ser:ProfileService,
    private zone:NgZone) {
    this.content = navParams.get('content');
    this.categories = [];
    this.children_groups = [];
    this.available_groups = [];
    this.main_type = this.navParams.get('type');
    this.type_title = "";
    this.default_fields = [];
    this.matched_type = null;
  }

  ionViewDidLoad() {}
  ngOnInit(){
    console.log('edit-add page');
    this.initPage().catch(console.log);
  }
  ionViewWillEnter(){
    //this.status_bar.hide();
    //this.status_bar.overlaysWebView(false);
  }
  ionViewWillLeave(){
    //this.status_bar.show();
    //this.status_bar.overlaysWebView(false);
  }

  async initPage(){
    await this.loading_ser.showLoading(true,'Loading data').catch(console.log);
    await this.initTypes().catch(console.log);
    // check if current type has categories and load them:
    await this.loadCategories().catch(console.log);
    await this.loading_ser.showLoading(false).catch(console.log);
    // separate service types categories:
    //prevent user from swiping to next without selecting category:
  }
  showErr(){
    this.notes_ser.showNote("Currently you can't add service with the selected type");
    this.loading_ser.showLoading(false).catch(console.log);
  }
  async saveContent(content_obj:IContent):Promise<any>{
    await this.loading_ser.showLoading(true,'Saving...').catch(console.log);
    if(this.isContent()){
      console.log('saving normal content');
      const location = await this.location_ser.getLocation().toPromise().catch(console.log);
      if(!location){
        this.loading_ser.showLoading(false).catch(console.log);
        this.notes_ser.showNote("We couldn't locate you, your location is required");
        console.log("couldn't get location");
        return;
      }
      content_obj.location = location;
      content_obj.lat = location.coords.latitude;
      content_obj.lng = location.coords.longitude;
      let save_res = null;
      if(content_obj.id){
        save_res = await this.ser.update(content_obj).toPromise().catch(console.log);
      }else{
        save_res = await this.ser.save(content_obj).toPromise().catch(console.log);
      }
      await this.loading_ser.showLoading(false).catch(console.log);
      if(save_res){
        this.events.publish('navigate:content',save_res.id);
      }else{
        console.log(save_res);
      }

    }else{
      console.log('saving profile');
      let profile = <IProfile>{};
      let attributes = Object.keys(content_obj);
      for(let attr of attributes){
        profile[attr] = content_obj[attr];
      }
      profile.values = content_obj.values;
      const up_res = await this.profile_ser.update(profile).toPromise().catch(console.log);
      await this.loading_ser.showLoading(false).catch(console.log);
      if(up_res){
        this.navCtrl.pop().catch(console.log);
      }else{
        console.log(up_res);
      }
    }
  }
  async initTypes(){
    const types = <IContentType[]> await this.storage.get('content_types').catch(console.log);
    if(!types){
      console.log('failed to load types');
      await this.loading_ser.showLoading(false).catch(console.log);
      return;
    }
    this.types = types;
    // if main type not received as param, get it from 'content':
    if(this.content){
      const target_type_id = this.content.content_type_id;
      this.main_type = types.filter(item => {
        return item.id === target_type_id;
      })[0];
      this.matched_type = this.prepareType();
    }
    this.main_type.children = [];
    // check if has children:
    for(let type of this.types){
      if(type.parent_id === this.main_type.id){
        this.main_type.children.push(type);
      }
    }
  }
  typeHasCategories():boolean{
    let has_cats = false;
    if(this.main_type.categories && this.main_type.categories.length){
      has_cats = true;
    }else if(this.main_type.children && this.main_type.children.length){
      for(let child of this.main_type.children){
        if(child.categories && child.categories.length){
          has_cats = true;
        }
      }
    }
    return has_cats;
  }

  // == CATEGORIES ==
  async loadCategories(){
    if(!this.typeHasCategories()){
      return;
    }
    const categories = await this.storage.get('categories').catch(console.log);
    if(categories){
      this.original_cats = categories;
    }else{
      return;
    }
    this.categories = this.helpers.unflatten(categories,'parent_id');
    let groups = this.helpers.groupBy(categories,'parent_id');
    groups = groups.filter(item => {
      return item.key !== null;
    });
    this.children_groups = groups;
    if(this.content && categories){
      this.content.categories.forEach((item,index,arr) => {
        for(let cat of categories){
          if(item.id === cat.id){
            arr[index] = cat;
          }
        }
      })
    }
    console.log('content:', this.content);
    console.log('categories:', categories);
  }
  getMainCatChildren(){
    let group = this.getGroup(this.selected_category);
    this.available_groups.push(group);
  }
  selectCat(cat: ICategory){
    //todo: include the group only when it's parent is selected:
    let group = this.getGroup(cat);
    if(group){
      this.available_groups = this.available_groups.filter(item => {
        return item.key !== group.key;
      });
      this.available_groups.push(group);
    }else{
      this.selected_sub_cat = cat;
      setTimeout(() => {
        this.matched_type = this.prepareType();
      },500);
    }
  }
  prepareType():IContentType{
    const matched = this.findTargetType();
    const defFields = this.getDefaultFields();
    if(this.content){
      const defValues = this.getDefaultValues(defFields);
      this.content.values = this.content.values.concat(defValues);
    }
    if(matched && matched.sections.length){
      this.default_fields = defFields;
      for(let field of defFields){
        matched.sections[0].fields.unshift(field);
      }
    }
    //parent type:
    let parent = <IContentType>{};
    for(let type of this.types){
      if(type.id === matched.parent_id){
        parent = type;
      }
    }
    if(parent && parent.sections && parent.sections.length){
      for(let section of parent.sections){
        for(let field of section.fields){
          matched.sections[0].fields.push(field);
        }
      }
    }
    return matched;
  }
  findTargetType():IContentType{
    let target_types:IContentType[] = [];
    let matched_type = null;
    if(this.content){
      return this.main_type;
    }
    if(this.main_type.children && this.main_type.children.length){
      target_types = this.main_type.children;
    }else{
      target_types.push(this.main_type);
    }
    let cats_to_match = [];
    for(let group of this.available_groups){
      for(let cat_id of group.key){
        cats_to_match.push(cat_id);
      }
    }
    for(let type of target_types){
      if(type.categories){
        for(let cat of type.categories){
          if(type.strict_mode){
            if(this.selected_sub_cat.id === cat.id){
              matched_type =  type;
            }
          }else{
            for(let id of cats_to_match){
              if(id === cat.id.toString()){
                matched_type = type;
              }
            }
            if(!matched_type && this.selected_sub_cat.id === cat.id){
              matched_type =  type;
            }
          }
        }
      }else if(target_types.length < 2){
        matched_type = type;
      }
    }
    if(matched_type){
      this.type_title = this.getTypeTitle(matched_type);
    }
    return matched_type;
  }
  collectFinished(content:IContent){
    // since we don't know the fields names from 'content'
    // search for them and assign them to 'content' properly
    for(let val of content.values){
      for(let field of this.default_fields){
        if(field.id === val.field_id){
          content[field.name] = val.value;
        }
      }
    }
    // remove the default fields from 'values':
    content.values = content.values.filter(item => {
      let include = true;
      for(let field of this.default_fields){
        if(field.id === item.field_id){
          include = false;
        }
      }
      return include;
    });
    //gather categories:
    let cats:ICategory[] = [];
    if(this.isContent() && !this.content){
      cats.push({id: this.selected_sub_cat.id});
      if(!this.matched_type.strict_mode){
        for(let group of this.available_groups){
          cats.push({id: parseInt(group.key)});
        }
      }
    }
    if(cats.length){
      content.categories = cats;
    }
    console.log('to save:', content);
    this.saveContent(content).catch(console.log);
  }
  getGroup(cat: ICategory){
    let group = this.children_groups.filter((item) => {
      return item.key === cat.id.toString();
    });
    if(group.length){
      return group[0];
    }
    return null;
  }
  isGroupSelected(group: IGroup):boolean{
    if(group.key === this.selected_category.id.toString()){
      return true;
    }
    let selected = false;
    for(let _group of this.available_groups){
      selected = _group.key === group.key;
    }
    return selected;
  }
  getTypeTitle(type?:IContentType): string{
    let title = "content";
    let findParent = (id:number)=>{
      for(let parent of this.types){
        if(parent.id === id){
          return parent;
        }
      }
      return;
    };
    if(type){
      if(type.parent_id){
        title = this.getTypeTitle(findParent(type.parent_id));
      }else{
        title = type.type;
      }
    }else if(this.matched_type){
      if(this.matched_type.parent_id) {
        //todo: fix getting title from parent:
        title = this.getTypeTitle(findParent(this.matched_type.parent_id));
      }else if(this.matched_type.type){
        title = this.matched_type.type;
      }
    }
    return title;
  }
  isProfile():boolean{
    const current_id = this.matched_type.id;
    const org_id = this.settings.organization_type_id;
    const prof_id = this.settings.profile_type_id;
    if(current_id === org_id || current_id === prof_id){
      return true;
    }
    return false;
  }
  isContent():boolean{
    return !this.isProfile();
  }
  getDefaultFields():IField[]{
    return <IField[]>[
      {
        id: new Date().getTime(),
        label: "Quantity",
        name: "quantity",
        type: "number",
        value: "1",
        required: true,
        isDefault: true,
        add: true,
      },
      {
        id: new Date().getTime() + 10,
        label: "Details",
        name: "body",
        type: "text",
        value: null,
        required: true,
        isDefault: true,
        add: true,
      },
      {
        id: new Date().getTime() + 11,
        label: "Title",
        name: "title",
        type: "text",
        value: "",
        required: true,
        isDefault: true,
        add: true,
      }
    ]
  }
  getDefaultValues(defFields:IField[]):IFieldVal[] {
    let values = <IFieldVal[]> [];
    for(let field of defFields){
      let val = <IFieldVal>{
        field_id: field.id,
        id: field.id,
        value: this.content[field.name],
      };
      values.push(val);
    }
    return values;
  }
}
