import {NgModule} from "@angular/core";
import {IonicModule, IonicPageModule} from "ionic-angular";
import {PermissionsPage} from "./permissions";

@NgModule({
  declarations:[PermissionsPage],
  imports:[
    IonicPageModule.forChild(PermissionsPage),
    IonicModule
  ]
})
export class PermissionsPageModule{}