import {Component, OnInit, ViewChild} from '@angular/core';
import {NavController, NavParams, App, Events, IonicPage} from 'ionic-angular';
import { Slides } from 'ionic-angular';
import {Diagnostic} from "@ionic-native/diagnostic";
import {File} from "@ionic-native/file";
import {Storage} from "@ionic/storage";
import {AuthService} from "../../providers/auth.service";
import {LoginPage} from "../login/login";

@IonicPage()
@Component({
  selector: 'page-permissions',
  templateUrl: 'permissions.html',
  providers: [Diagnostic]
})
export class PermissionsPage implements OnInit{
  @ViewChild(Slides) slides: Slides;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private app:App,
    private diagnostic: Diagnostic,
    public storage:Storage,
    private auth: AuthService,
    private events:Events,
  ) {}

  ngOnInit(){
  }
  ionViewDidEnter(){
    //todo: add arabic support for slides
    //this.slides._rtl = true;
  }
  slideChanged(){
    // Mic:     2
    // Camera:  4
    // Write:   6
    let currentIndex = this.slides.getActiveIndex();
    console.log('Current index is', currentIndex);
    switch (currentIndex){
      case 2:
        this.checkMicPermission();
        break;
      case 4:
        this.checkCameraPermission();
        break;
      case 6:
        this.checkWritePermission();
        break;
      case 8:
        this.checkLocationPermission();
        break;
      case 9:
        //
        this.requestsDone();
        break;
      default:
        //do nothing;
        break;
    }
  }
  slideNext(){
    this.slides.slideNext();
  }
  requestsDone(){
    setTimeout(() => {
      this.storage.set('hasInitPermissions',true)
        .then(() => {
          this.exitPage();
        })
        .catch(() => {
          console.log('failed to save permissions check result');
          this.exitPage().catch(console.log);
        });

    },500)
  }
  async exitPage(){
    const logged = await this.auth.hasLoggedIn().catch(console.log);
    if(logged){
      this.navCtrl.setRoot('TabsPage').catch(console.log);
      this.events.publish("app:load");
    }else{
      this.navCtrl.setRoot("LoginPage").catch(console.log)
    }
    this.events.publish('permissions:done');
  }
  checkWritePermission(){
    //write
    this.diagnostic.isExternalStorageAuthorized().then(res => {
      if(res){
        //permission granted
        console.log('write permission granted');
        this.slideNext();
      }else{
        this.diagnostic.requestExternalStorageAuthorization().then(granted => {
          if(granted && granted !== 'DENIED_ALWAYS'){
            //permission granted
            console.log('write permission granted');
            this.slideNext();
          }else{
            console.log(granted);
            console.log('The app needs write permission in order to store files!');
            this.slideNext();
          }
        }).catch(console.warn);
      }
    }).catch(console.warn);
  }
  checkCameraPermission(){
    //camera
    this.diagnostic.isCameraAuthorized().then(res => {
      if(res){
        //permission granted
        console.log('camera permission granted');
        this.slideNext();
      }else{
        this.diagnostic.requestCameraAuthorization().then(granted => {
          if(granted && granted !== 'DENIED_ALWAYS'){
            //permission granted
            console.log('camera permission granted');
            this.slideNext();
          }else{
            console.log(granted);
            console.log('The app needs permission to use the camera in order to send photos!');
            this.slideNext();
          }
        }).catch(console.warn);
      }
    }).catch(console.warn);
  }
  checkMicPermission(){
    //microphone
    this.diagnostic.isMicrophoneAuthorized().then(res => {
      if(res){
        //permission granted
        console.log('mic permission granted');
        this.slideNext();
      }else{
        this.diagnostic.requestMicrophoneAuthorization().then(granted => {
          if(granted && granted != 'DENIED_ALWAYS'){
            //permission granted
            console.log('mic permission granted');
            this.slideNext();
          }else{
            console.log('The app needs permission to use the microphone in order to send audio!');
          }
        }).catch(console.warn);
      }
    }).catch(console.warn);
  }
  checkLocationPermission(){
    //location
    this.diagnostic.isLocationAuthorized().then(res => {
      if(res){
        //location granted
        console.log('location permission granted');
        this.slideNext();
      }else{
        this.diagnostic.requestLocationAuthorization().then(granted => {
          if(granted && granted != 'DENIED_ALWAYS'){
            //permission granted
            console.log('location permission granted');
            this.slideNext();
          }else{
            console.log('The app needs permission to know your location in order to serve you better');
          }
        }).catch(console.warn);
      }
    }).catch(console.warn);
  }
}
