import {NgModule} from "@angular/core";
import {IonicModule, IonicPageModule} from "ionic-angular";
import {RemindersPage} from "./reminders";
import {MomentModule} from "angular2-moment";

@NgModule({
  declarations:[RemindersPage],
  imports:[
    IonicPageModule.forChild(RemindersPage),
    IonicModule,
    MomentModule,
  ]
})
export class RemindersPageModule{}