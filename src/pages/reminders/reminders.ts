import {Component, OnInit} from '@angular/core';
import {AlertController, Events, ModalController, NavController, NavParams} from 'ionic-angular';
import {ReminderService} from "../../providers/reminder-service";
import {IPagination} from "../../interfaces/IPagination";
import {PaginationService} from "../../providers/pagination.service";
import {IReminder} from "../../interfaces/IReminder";
import {IContentType} from "../../interfaces/IContentType";
import {IProfile} from "../../interfaces/IProfile";
import {Storage} from "@ionic/storage";
import {HelpersService} from "../../providers/helpers.service";
import {AuthService} from "../../providers/auth.service";
import {IContentUpdate} from "../../interfaces/IContentUpdate";
import {LoadingService} from "../../providers/loading.service";
import {ContentUpdateService} from "../../providers/content-update.service";
import {IonicPage} from 'ionic-angular';
import {IAuthor} from "../../interfaces/IAuthor";
import {ConfirmationService} from "../../providers/confirmation.service";
import {IQRCode} from "../../interfaces/IQRCode";
import {IConfirmation} from "../../interfaces/IConfirmation";

@IonicPage()
@Component({
  selector: 'page-reminders',
  templateUrl: 'reminders.html',
})
export class RemindersPage implements OnInit{
  items: IReminder[];
  pagination:IPagination;
  loading: boolean;
  types:IContentType[];
  profile:IProfile;
  confirmations: IConfirmation[];
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private rem_ser:ReminderService,
              private pag_ser:PaginationService,
              private storage:Storage,
              private helpers:HelpersService,
              private auth_ser:AuthService,
              private modalCtrl:ModalController,
              private alertCtrl:AlertController,
              private loading_ser:LoadingService,
              private con_up_ser:ContentUpdateService,
              private conf_ser:ConfirmationService,
              private events:Events) {
    this.items = [];
    this.confirmations = [];
    this.loading = true;
    this.events.unsubscribe('reminders_scan_result');
  }
  ngOnInit(){
    this.initPage();
    this.events.subscribe('reminders_scan_result',(code:IQRCode) => {
      this.resolve(code).catch(console.log);
    });
  }
  initPage(){
    this.getItems(null,async () => {
      await this.loadContentTypes();
      await this.getProfile();
      this.loading = false;
      console.log(this.items);
    })
  }
  async loadContentTypes(){
    const types = <IContentType[]>await this.storage.get('content_types').catch(console.log);
    if(types){
      this.types = <IContentType[]>this.helpers.flattenObjects(types,'children');
    }
  }
  async getProfile(){
    const profile = <IProfile> await this.auth_ser.getCurrentProfile().catch(console.log);
    if(profile){
      this.profile = profile;
    }
  };
  openItem(item: IReminder){
    let update = item.content_update;
    let params = {
      update: update,
      types:this.types,
      profile:this.profile,
    };
    if(this.helpers.platform === 'browser'){
      let modal = this.modalCtrl.create("UpdateViewPage", params);
      modal.present().catch(console.log);
    }else{
      this.navCtrl.push("UpdateViewPage",params).catch(console.log);
    }
  }
  isExpired(item:IReminder):boolean{
    let schedule = this.helpers.toIso(item.content_update.schedule.toString());
    return this.helpers.isBefore(schedule);
  }
  showAlert(item:IReminder) {
    const prompt = this.alertCtrl.create({
      title: 'Are you sure?',
      message: "Are you sure you want to cancel this meeting?",
      buttons: [
        {
          text: 'No',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.cancel(item).catch(console.log);
          }
        }
      ]
    });
    prompt.present().catch(console.log);
  }
  async cancel(item:IReminder){
    this.loading_ser.showLoading(true,'Please wait..').catch(console.log);
    let _update = <IContentUpdate>item.content_update;
    let content_update: IContentUpdate = {
      previous_id: _update.id,
      parent_id: _update.id,
      office_id: _update.office_id,
      direct: _update.direct,
      status: 3,
      actions_allowed: false,
      details: _update.details,
      scheduled: _update.scheduled,
      schedule: _update.schedule,
      modified: false,
      lat: _update.lat,
      lng:_update.lng,
    };
    if(_update.parent_id){
      content_update.parent_id = _update.parent_id;
    }
    let reminder = <IReminder>{
      id: item.id,
      status: 2,
    };
    await this.rem_ser.update(reminder).toPromise().catch(console.log);
    let content_id = _update.content.id;
    const save_res = await this.con_up_ser.save(content_id,content_update).toPromise().catch(console.log);
    if(save_res){
      this.items.forEach(rem => {
        if(rem.id === item.id){
          rem.content_update.actions_allowed = false;
          rem.status = 2;
        }
      })
    }
    this.loading_ser.showLoading(false).catch(console.log);
  }
  showScanPage(reminder:IReminder){
    (async () => {
      const profiles = [];
      for(let profile of reminder.profiles){
        profiles.push(profile.id);
      }
      const update = reminder.content_update;
      const confs = await this.conf_ser.filter(profiles,update.content_id,update.id)
        .toPromise()
        .catch(console.log);
      if(confs && confs.length){
        this.confirmations = confs;
        const qr_codes: IQRCode[] = [];
        for(let conf of confs){
          const qr_code = <IQRCode>{
            id: conf.id,
            code: conf.code,
          };
          qr_codes.push(qr_code);
        }
        const params = {
          codes: qr_codes,
          display_code: true,
          caller: "reminders",
        };
        this.navCtrl.push("ScanPage",params).catch(console.log);
      }
    })().catch(console.log);
  }
  async resolve(qr_code: IQRCode){
    this.loading_ser.showLoading(true,'Please wait..').catch(console.log);
    const conf = this.confirmations.filter(item => {
      return item.id === qr_code.id;
    })[0];
    if(!conf){
      return false;
    }
    const reminder = this.items.filter(item => {
      return item.content_update.id === conf.content_update_id;
    })[0];
    if(!reminder){
      return false;
    }
    let _update = reminder.content_update;
    let content_update: IContentUpdate = {
      previous_id: _update.id,
      parent_id: _update.id,
      office_id: _update.office_id,
      direct: _update.direct,
      status: _update.office_id? 6: 10,
      actions_allowed: false,
      details: _update.details,
      scheduled: _update.scheduled,
      schedule: _update.schedule,
      modified: false,
      lat: _update.lat,
      lng:_update.lng,
      primary: true,
    };
    if(_update.parent_id){
      content_update.parent_id = _update.parent_id;
    }
    if(this.profile.isOffice){
      // set as received by office:
      content_update.status = 7;
    }
    // if needy accepted the appointment or office accepted to receive the package:
    if(_update.status === 16 || _update.status === 13){
      const hasOffice = _update.office && _update.office.author;
      if(hasOffice && this.profile.id === _update.office.author.id){
        // set as delivered:
        content_update.status = 8;
      }else{
        // set as received:
        content_update.status = 9;
      }
    }
    let content_id = _update.content.id;
    const save_res = await this.con_up_ser.save(content_id,content_update).toPromise().catch(console.log);
    this.loading_ser.showLoading(false).catch(console.log);
    console.log('not loading, finished');
    if(save_res){
      this.disableActions(reminder.id);
    }else{
      console.log(save_res);
    }
  }
  getItems(refresher?,callback?){
    let finish  = ()=> {
      if(refresher){
        refresher.complete();
      }
      if(callback){
        callback();
      }
    };
    this.rem_ser.getReminders().subscribe(pagination => {
      this.items = pagination.data;
      this.pagination = pagination;
      this.pagination.data = null;
      console.log('items:', this.items);
      finish();
    },err => {
      finish();
      console.log('Error while fetching reminders:', err);
    });
  }
  loadNext(infiniteScroll):Promise<any>{
    return new Promise((resolve,reject) => {
      if(this.pagination.meta.last_page != this.pagination.meta.current_page){
        this.pag_ser.next(this.pagination.links.next).subscribe(res => {
          this.items = this.items.concat(res.data);
          this.pagination = res;
          this.pagination.data = null;
          resolve();
          infiniteScroll.complete();
        },err => {
          infiniteScroll.complete();
          console.log('Error while fetching reminders:', err);
        });
      }else{
        resolve();
        infiniteScroll.complete();
      }
    });
  }
  getOtherParty(reminder:IReminder):IAuthor{
    const update = reminder.content_update;
    if(this.profile){
      const other_party_id = reminder.profiles.filter(id => {
        return id.id !== this.profile.id;
      })[0];
      if(update.author.id === other_party_id.id){
        return update.author;
      }else if(update.office.author.id === other_party_id.id){
        return update.office.author;
      }
      return update.content.author;
    }
    return update.content.author;
  }
  disableActions(id: number){
    for(let item of this.items){
      if(item.id === id){
        item.status = 1;
      }
    }
  }
  isResolved(item: IReminder):boolean{
    return item.status === 1;
  }
  isCanceled(item: IReminder):boolean{
    return item.status === 2;
  }
}
