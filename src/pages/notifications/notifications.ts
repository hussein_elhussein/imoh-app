import {Component, OnInit} from '@angular/core';
import {NotificationService} from "../../providers/notification.service";
import {IPagination} from "../../interfaces/IPagination";
import {INotification} from "../../interfaces/INotification";
import {PaginationService} from "../../providers/pagination.service";
import {HelpersService} from "../../providers/helpers.service";
import {Events, IonicPage, ModalController, NavController} from "ionic-angular";
import {ContentUpdateService} from "../../providers/content-update.service";
import {IContentUpdate} from "../../interfaces/IContentUpdate";
import {IContentType} from "../../interfaces/IContentType";
import {Storage} from "@ionic/storage";
import {IProfile} from "../../interfaces/IProfile";
import {AuthService} from "../../providers/auth.service";
import {UpdateViewPage} from "../content/update-view/update-view";

@IonicPage()
@Component({
  selector: 'page-notifications',
  templateUrl: 'notifications.html'
})
export class NotificationsPage implements OnInit{
  items: INotification[];
  pagination:IPagination;
  loading: boolean;
  types:IContentType[];
  profile: IProfile;
  constructor(
    private not_ser:NotificationService,
    private pag_ser: PaginationService,
    private helpers:HelpersService,
    private con_up_ser:ContentUpdateService,
    private storage:Storage,
    private events:Events,
    private auth_ser:AuthService,
    private modalCtrl:ModalController,
    private nav:NavController) {
    this.loading = true;
    this.items = [];
  }
  ngOnInit(){
    this.initPage();
    this.events.publish('notifications:reset',null);
  }
  initPage(){
    this.events.subscribe('notifications:new',(item) => {
      this.items.unshift(item);
      this.setRead([item]);
      this.loadContentUpdates().catch(console.log);
    });
    this.getItems(null,() => {
      this.loadContentTypes().catch(console.log);
      this.getProfile().catch(console.log);
      this.loading = false;
      console.log(this.items);
    })
  }
  async loadContentTypes(){
    const types = <IContentType[]>await this.storage.get('content_types').catch(console.log);
    if(types){
      this.types = <IContentType[]>this.helpers.flattenObjects(types,'children');
    }
  }
  async getProfile(){
    const profile = <IProfile> await this.auth_ser.getCurrentProfile().catch(console.log);
    if(profile){
      this.profile = profile;
    }
  };
  getItems(refresher?,callback?){
    let finish  = ()=> {
      if(refresher){
        refresher.complete();
      }
      if(callback){
        callback();
      }
    };
    this.not_ser.getNotifications().subscribe(pagination => {
      this.items = <INotification[]> pagination.data.filter((item:INotification) => {
        return item.data.visible;
      });
      this.pagination = pagination;
      this.pagination.data = null;
      this.setRead();
      this.loadContentUpdates().catch(console.log);
      finish();
    },err => {
      //this.loading = false;
      finish();
      console.log('Error while fetching notifications:', err);
    });
  }
  openItem(item: INotification){
    if(this.isContentUpdate(item)){
      let update = item.data.original;
      let params = {
        update: update,
        types:this.types,
        profile:this.profile,
      };
      if(this.helpers.platform === 'browser'){
        let modal = this.modalCtrl.create("UpdateViewPage",params);
        modal.present().catch(console.log);
      }else{
        this.nav.push("UpdateViewPage", params).catch(console.log);
      }
    }
  }
  setRead(items?: INotification[]){
    let ids = [];
    if(items){
      for(let item of items){
        if(!item.read_at){
          ids.push(item.id);
        }
      }
    }else{
      for(let item of this.items){
        if(!item.read_at){
          ids.push(item.id);
        }
      }
    }
    if(ids.length){
      console.log('to set as read:', ids);
      this.not_ser.setRead(ids).subscribe(res => {

      },err => {
        console.log('Failed to set notifications as read:', err);
      });
    }else{
      console.log('no new notifications');
    }
  }
  loadNext(infiniteScroll):Promise<any>{
    return new Promise((resolve,reject) => {
      if(this.pagination.meta.last_page != this.pagination.meta.current_page){
        this.pag_ser.next(this.pagination.links.next).subscribe(res => {
          this.items = this.items.concat(res.data);
          this.pagination = res;
          this.pagination.data = null;
          this.loadContentUpdates().catch(console.log);
          this.setRead(res.data);
          resolve();
          infiniteScroll.complete();
        },err => {
          infiniteScroll.complete();
          console.log('Error while fetching notifications:', err);
        });
      }else{
        resolve();
        infiniteScroll.complete();
      }
    });
  }
  async loadContentUpdates(){
    let ids = [];
    for(let item of this.items){
      if((item.type === 'update_created' || item.type === 'update_updated') && !item.data.original){
        ids.push(item.data.id);
      }
    }
    let updateItem = (content_update: IContentUpdate) => {
      this.items.forEach((item,index,arr) => {
        if(item.data.id === content_update.id){
          arr[index].data.original = content_update;
        }
      })
    };
    if(ids.length){
      const updates = <IContentUpdate[]> await this.con_up_ser.getMany(ids).toPromise().catch(console.log);
      if(updates){
        for(let content_update of updates){
          updateItem(content_update);
        }
      }
    }
  }
  isContentUpdate(item:INotification):boolean{
    return (item.type === 'update_created' || item.type === 'update_updated');
  }
}
