import {NgModule} from "@angular/core";
import {IonicModule, IonicPageModule} from "ionic-angular";
import {NotificationsPage} from "./notifications";
import {PipesModule} from "../../pipes/pipes.module";
import {ComponentsModule} from "../../components/components.module";
import {MomentModule} from "angular2-moment";

@NgModule({
  declarations:[NotificationsPage],
  imports:[
    IonicPageModule.forChild(NotificationsPage),
    MomentModule,
    PipesModule,
    ComponentsModule,
  ]
})
export class NotificationsPageModule{}