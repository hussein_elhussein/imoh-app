import {NgModule} from "@angular/core";
import {IonicModule, IonicPageModule} from "ionic-angular";
import {PipesModule} from "../../../pipes/pipes.module";
import {OrganizationIndexPage} from "./organization-index";

@NgModule({
  declarations:[OrganizationIndexPage],
  imports:[
    IonicPageModule.forChild(OrganizationIndexPage),
    IonicModule,
    PipesModule
  ]
})
export class OrganizationIndexPageModule{}