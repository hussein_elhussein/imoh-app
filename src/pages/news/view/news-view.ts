import {Component} from '@angular/core';
import {ActionSheetController,IonicPage, ModalController, NavController, NavParams} from "ionic-angular";
import {SettingsService} from "../../../providers/settings.service";
import {ContentService} from "../../../providers/content.service";
import {ProfileService} from "../../../providers/profile.service";
import {HelpersService} from "../../../providers/helpers.service";
import {LocationHelperService} from "../../../providers/location-helper.service";
import {IContent} from "../../../interfaces/IContent";
import {Storage} from "@ionic/storage";
import {IContentType} from "../../../interfaces/IContentType";
import {ICategory} from "../../../interfaces/ICategory";
import {LoadingService} from "../../../providers/loading.service";

@IonicPage()
@Component({
  selector: 'page-news-view',
  templateUrl: 'news-view.html'
})
export class NewsViewPage{
  content:IContent;
  content_type:IContentType;
  types: IContentType[];
  categories: ICategory[];
  loading: boolean;
  id: number;
  constructor(
    private navCtrl:NavController,
    private navParams:NavParams,
    private settings:SettingsService,
    private ser:ContentService,
    private profile_ser:ProfileService,
    private actionSheetCtrl:ActionSheetController,
    private helpers:HelpersService,
    private location_ser:LocationHelperService,
    private loading_ser:LoadingService,
    private modalCtrl: ModalController,
    private storage:Storage,
  ) {
    this.categories = [];
    this.types = [];
    this.loading = true;
  }
  ngOnInit(){
    this.initPage().catch(console.log);
  }
  async initPage(){
    const content = await this.ser.getContent(this.id).toPromise().catch(console.log);
    if(!content){return;}
    this.content = content;
    console.log('content:', this.content);
    const types = await this.storage.get('content_types').catch(console.log);
    if(!types){
      console.log('Failed to load content types');
      return;
    }
    this.types = this.helpers.flattenObjects(types,'children');
    this.content_type = this.types.filter(item => {
      return item.id === this.content.content_type_id;
    })[0];
    const categories = await this.storage.get('categories').catch(console.log);
    if(!categories){
      console.log('failed to load categories');
      return;
    }
    this.categories = categories;
    this.loading = false;
  }
  hasCategories(): boolean{
    const doHave = this.content.categories && this.content.categories.length;
    return !!doHave;
  }
  getCategory(id: number):ICategory{
    let found = this.categories.filter(item => {
      return item.id === id;
    });
    return found[0];
  }
  getName():string{
    if(this.content.author){
      return this.profile_ser.getName(this.content.author);
    }else{
      return;
    }
  }
  presentActions():void{
    let sheetContent = {
      title: 'Actions',
      buttons: [
        {
          text: 'Edit Service',
          icon: 'create',
          role: 'destructive',
          handler: () => {
            this.navCtrl.push("ContentEditAddPage", {content: this.content});
          }
        },
        {
          text: 'Cancel',
          icon: 'close-circle',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    };
    let actionSheet = this.actionSheetCtrl.create(sheetContent);
    actionSheet.present().catch();
  }

}
