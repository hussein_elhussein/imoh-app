import {NgModule} from "@angular/core";
import {IonicModule, IonicPageModule} from "ionic-angular";
import {PipesModule} from "../../../pipes/pipes.module";
import {NewsViewPage} from "./news-view";
@NgModule({
  declarations: [NewsViewPage],
  imports:[
    IonicPageModule.forChild(NewsViewPage),
    PipesModule,
    IonicModule,
  ]
})
export class NewsViewModule {}