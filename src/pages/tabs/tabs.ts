import {Component, OnInit} from '@angular/core';
import {Events, IonicPage, NavController, NavParams} from 'ionic-angular';
import {Storage} from "@ionic/storage";
import {ITab} from "../../interfaces/ITab";

@IonicPage()
@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage{
  tabs: ITab[] = [];
  mySelectedIndex: number;

  constructor(
    private navParams: NavParams,
    private events:Events,
    private storage:Storage,
    private nav:NavController) {
    this.tabs = navParams.get('tabs');
    this.mySelectedIndex = navParams.data.tabIndex || 0;
  }
  ionViewWillEnter(){
    this.events.subscribe('notifications:count',(count: number) => {
    this.updateNotifications(count);
    });
    this.events.subscribe('notifications:reset',(count: number) => {
      this.updateNotifications(count);
    });
    this.events.publish('tabs-view:loaded');
    this.storage.get('hasInitPermissions').then((hasInitPermissions:boolean) => {
      if(!hasInitPermissions){
        //this.nav.setRoot(PermissionsPage);
      }
    });
  }
  updateNotifications(count:number){
    if(this.tabs && this.tabs.length){
      this.tabs.forEach((tab,index,arr) => {
        if(tab.page === 'notifications'){
          if(count){
            tab.tabBadge = count;
          }else{
            tab.tabBadge = null;
            tab.tabBadgeStyle = null;
          }
        }
      });
    }
  }
}
