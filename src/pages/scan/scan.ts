import {Component, OnInit, ViewChild} from '@angular/core';
import {Events, IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';
import {ZXingScannerComponent} from "@zxing/ngx-scanner";
import {BarcodeScanner} from "@ionic-native/barcode-scanner";
import {NotificationService} from "../../providers/notification.service";
import {HelpersService} from "../../providers/helpers.service";
import {Vibration} from "@ionic-native/vibration";
import {IQRCode} from "../../interfaces/IQRCode";

@IonicPage()
@Component({
  selector: 'page-scan',
  templateUrl: 'scan.html',
})
export class ScanPage implements OnInit{
  toggle_br_scan: boolean;
  browser: boolean;
  @ViewChild('scanner')
  scanner: ZXingScannerComponent;
  availableCameras: Array<MediaDeviceInfo>;
  availableCodes: IQRCode[];
  display_code: boolean;
  caller: string;
  scan_sub: any;
  scanning: boolean;
  showCode: boolean;
  constructor(public navCtrl: NavController,
              private viewCtrl:ViewController,
              public navParams: NavParams,
              private barcodeScanner: BarcodeScanner,
              private vibration: Vibration,
              private note_ser:NotificationService,
              private helpers:HelpersService,
              private events:Events) {
    this.toggle_br_scan = false;
    this.browser = false;
    this.availableCodes = this.navParams.get('codes');
    this.display_code = this.navParams.get('display_code');
    this.caller = this.navParams.get('caller');
    this.showCode = this.display_code && this.availableCodes.length > 0;
    if(!this.availableCodes){
      console.log('warning: no available codes to check against');
    }
  }
  ionViewDidEnter(){
    if(!this.showCode && this.browser){
      this.loadScanning();
    }
  }
  loadScanning(){
    let timeout = 0;
    const interval = setInterval(() => {
      timeout += 100;
      if(this.availableCameras){
        this.scan().catch(console.log);
        clearInterval(interval);
      }else if(timeout > 5000){
        console.log('timeout waiting for devices:', this.availableCameras);
        clearInterval(interval);
      }
    },100);
  }
  ngOnInit(){
    this.browser = this.helpers.platform === 'browser';
  }
  toggleScan(){
    if(this.browser){
      if(this.toggle_br_scan){
        this.scanner.resetScan();
        this.toggle_br_scan = false;
      }else{
        this.toggle_br_scan = true;
        setTimeout(() => {
          this.scanner.resetScan();
          this.scanner.startScan(this.availableCameras[0]);
        },1000);
      }
    }else{
      this.scan().catch(console.log);
    }
  }
  async scan(){
    if(this.browser){
     this.toggleScan();
    }else{
      const res = await this.barcodeScanner.scan({disableSuccessBeep: true}).catch(console.log);
      if(res && res.text){
        this.vibrate();
        this.matchCode(res.text);
      }else{
        console.log('qr code not found');
      }
    }
  }
  scanSuccess(text_code: string){
    if(text_code){
      this.matchCode(text_code);
    }
  }
  matchCode(text_code: string){
    const found_code = this.availableCodes.filter(code => {
      return code.code === text_code;
    });
    if(found_code.length){
      this.finish(found_code[0]);
    }else{
      console.log('no matching code found');
    }
  }
  finish(code: IQRCode){
    console.log('finish called');
    let topic = "scan_result";
    if(this.caller){
      topic = this.caller + "_scan_result";
    }
    this.events.publish(topic, code);
    this.cancelScan();
  }
  cancelScan(){
    if(this.browser){
      this.viewCtrl.dismiss().catch(console.log);
    }else{
      this.navCtrl.pop().catch(console.log)
    }
  }
  addCameras(cameras: Array<any>){
    this.availableCameras = cameras;
  }
  scanError(event){
    console.log('scan error:', event);
  }
  scanFailure(event){
    console.log('scan failure:', event);
  }
  vibrate(){
    setTimeout(() => {
      this.vibration.vibrate(100);
    },10)
  }

}
