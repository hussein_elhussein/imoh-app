import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ScanPage } from './scan';
import {ZXingScannerModule} from "@zxing/ngx-scanner";
import {BarcodeScanner} from "@ionic-native/barcode-scanner";
import {Vibration} from "@ionic-native/vibration";
import {QRCodeModule} from "angularx-qrcode";

@NgModule({
  declarations: [
    ScanPage,
  ],
  imports: [
    IonicPageModule.forChild(ScanPage),
    QRCodeModule,
    ZXingScannerModule,
  ],
  providers:[BarcodeScanner,Vibration]
})
export class ScanPageModule {}
