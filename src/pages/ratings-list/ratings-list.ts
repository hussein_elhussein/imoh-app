import {Component, OnInit} from '@angular/core';
import {Events, IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';
import {IRating} from "../../interfaces/IRating";
import {RatingService} from "../../providers/rating.service";
import {IPagination} from "../../interfaces/IPagination";
import {SettingsService} from "../../providers/settings.service";
import {ProfileService} from "../../providers/profile.service";
import {PaginationService} from "../../providers/pagination.service";

@IonicPage()
@Component({
  selector: 'ratings-list',
  templateUrl: 'ratings-list.html'
})
export class RatingsListPage implements OnInit{
  ratings: IRating[];
  id: number;
  rateable: string;
  pagination:IPagination;
  constructor(public navParams: NavParams,
              private rating_ser:RatingService,
              private settings:SettingsService,
              private profile_ser:ProfileService,
              private navCtrl:NavController,
              private pag_ser:PaginationService,
              public events:Events){
    this.ratings = [];
    this.id = this.navParams.get('id');
    this.rateable = this.navParams.get('rateable');
  }
  ngOnInit(){
    this.getRatings();
  }

  getRatings(refresher = null){
    this.rating_ser.getRatings(this.rateable,this.id).subscribe(res => {
      this.ratings = res.data;
      res.data = null;
      this.pagination = res;
      console.log(this.ratings[0]);
      if(refresher){
        refresher.complete();
      }
    }, err => {
      refresher.complete();
      console.log(err);
    });
  }
  loadNext(infiniteScroll):Promise<any>{
    return new Promise((resolve,reject) => {
      if(this.pagination.meta.last_page != this.pagination.meta.current_page){
        this.pag_ser.next(this.pagination.links.next).subscribe(res => {
          for(let item of res.data){
            this.ratings.push(item);
          }
          this.pagination = res;
          this.pagination.data = null;
          resolve();
          infiniteScroll.complete();
        },err => {
          infiniteScroll.complete();
          console.log(err);
        });
      }else{
        resolve();
        infiniteScroll.enable(false);
      }
    });
  }
  getFullURL(url:string):string{
    return this.settings.storage_url + url;
  }
  presentFilter() {
    console.log('filter');
  }
  gotoUserProfile(id:number){
    this.events.publish('navigate:profile',id);
  }
}
