import {NgModule} from "@angular/core";
import {IonicModule, IonicPageModule} from "ionic-angular";
import {RatingsListPage} from "./ratings-list";
import {PipesModule} from "../../pipes/pipes.module";
import {VRatingModule} from "../../v-rating/v-rating.module";

@NgModule({
  declarations:[RatingsListPage],
  imports:[
    IonicPageModule.forChild(RatingsListPage),
    IonicModule,
    VRatingModule,
    PipesModule,
  ]
})
export class RatingsLisPageModule{}