import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {Events, IonicPage, ModalController, NavController, NavParams} from 'ionic-angular';
import {OfficeService} from "../../providers/office.service";
import {LoadingService} from "../../providers/loading.service";
import {AuthService} from "../../providers/auth.service";
import {IProfile} from "../../interfaces/IProfile";
import {ProfileService} from "../../providers/profile.service";
import {ContentService} from "../../providers/content.service";
import {IContent} from "../../interfaces/IContent";
import {IContentType} from "../../interfaces/IContentType";
import {Storage} from "@ionic/storage";
import {HelpersService} from "../../providers/helpers.service";
import {IContentUpdate} from "../../interfaces/IContentUpdate";
import {LocationHelperService} from "../../providers/location-helper.service";
import {ContentUpdateService} from "../../providers/content-update.service";
import Moment from 'moment';
import { extendMoment } from 'moment-range';
import {SettingsService} from "../../providers/settings.service";
import {NotesService} from "../../providers/notes.service";
const moment = extendMoment(Moment);

@IonicPage()
@Component({
  selector: 'page-take-action',
  templateUrl: 'take-action.html',
})
export class TakeActionPage implements OnInit,AfterViewInit{
  content:IContent;
  is_direct: boolean;
  offered: boolean;
  id: number;
  offices: IContent[];
  profile: IProfile;
  update: IContentUpdate;
  date: string;
  place: string;
  @ViewChild('search',{ read: ElementRef })
  searchElement: ElementRef;
  search_ready: boolean;
  old_update: IContentUpdate;
  loading: boolean;
  min_date: string;
  max_date: string;
  location_err: boolean;
  map_height: number;
  constructor(
    private navParams: NavParams,
    private con_ser:ContentService,
    private con_up_ser:ContentUpdateService,
    private office_ser:OfficeService,
    private loading_ser: LoadingService,
    private auth_ser: AuthService,
    private profile_ser:ProfileService,
    private storage:Storage,
    private helpers:HelpersService,
    private location_ser:LocationHelperService,
    public modalCtrl: ModalController,
    public navCtrl:NavController,
    private events:Events,
    private settings:SettingsService,
    private note_ser:NotesService,
    private elementRef:ElementRef)
  {
    this.content = navParams.get('content');
    this.id = navParams.get('id');
    this.old_update = navParams.get('update');
    let format = "YYYY-MM-DD";
    this.min_date = moment().format(format);
    this.max_date = moment().add(1, 'years').format(format);
    this.offices = [];
    this.update = {
      office_id: 0,
      status: 1,
      schedule: null,
      details: '',
      lat: null,
      lng: null,
    };
    this.search_ready = false;
    this.place = "";
    this.location_err = false;
  }
  ngOnInit(){
    this.initData().catch(console.log);
  }
  ngAfterViewInit(){
    this.getMapHeight();
  }
  async initData(){
    this.loading = true;
    const profile = await this.auth_ser.getCurrentProfile().catch(console.log);
    if(profile){
      this.profile = profile;
    }
    const types = <IContentType[]> await this.storage.get('content_types').catch(console.log);
    if(types){
      if(this.id){
        const content = <IContent> await this.con_ser.getContent(this.id).toPromise().catch(console.log);
        if(content){
          this.content = content;
        }
      }else if(this.old_update){
        this.content = this.old_update.content;
        console.log('content:', this.content);
      }
      //check if the content is offered, direct
      this.offered = this.isOffered(types);
      if(this.offered && !this.is_direct && !this.isOfficeUpdate()){
        await this.getNearbyOffices();
      }
      if(this.is_direct){
        this.loadSearch();
      }
      if(this.old_update){
        this.populateFields();
      }
      this.loading = false;
    }
  }
  isOfficeOwner():boolean{
    if(this.profile && this.isOfficeUpdate()){
      return this.profile.id === this.old_update.office.profile_id;
    }
    return false;
  }
  isOfficeUpdate():boolean{
    if(this.old_update && this.old_update.office){
      const status = this.old_update.status;
      return status === 12 ||
        status === 13 ||
        status === 14 ||
        status === 15 ||
        status === 6;
    }
    return false;
  }
  populateFields(){
    if(this.old_update.office){
      this.update.office_id = this.old_update.office.id;
    }
    this.update.status = this.old_update.status;
    this.date = this.helpers.toIso(this.old_update.schedule.toString());
    this.update.lat = this.old_update.lat;
    this.update.lng = this.old_update.lng;
  }
  isOffered(types:IContentType[]):boolean{
    let content_type = <IContentType>{};
    let parent_type = <IContentType>{};
    const offered_type_id = this.settings.offered_service_type_id;
    for(let type of types){
      if(type.id === this.content.content_type_id){
        content_type = type;
      }
    }
    for(let type of types){
      if(content_type.parent_id === type.id){
        parent_type = type;
      }
    }
    this.is_direct = this.isDirectOffer(parent_type);
    return parent_type.id === offered_type_id;
  }
  isDirectOffer(type: IContentType){
    for(let section of type.sections){
      for(let val of this.content.values){
        for(let field of section.fields){
          if(val.field_id === field.id){
            let name = field.name.replace(' ','_').toLowerCase();
            if(name.match('direct')){
              return val.value === '1'
            }
          }
        }
      }
    }
    return null;
  }
  isLocationValid():boolean{
    if(this.location_err){
      return false;
    }
    if(this.is_direct){
      let lat_valid = this.update.lat !== null && this.update.lat !== undefined;
      let lng_valid = this.update.lng !== null && this.update.lng !== undefined;
      return lat_valid && lng_valid;
    }else{
      return true;
    }

  }
  loadSearch(){
    let interval = setInterval(() => {
      if(this.searchElement !== undefined){
        clearInterval(interval);
        let el = this.searchElement.nativeElement.getElementsByTagName('input')[0];
        this.location_ser.onPlaceSearch.subscribe(res => {
          if(res){
            this.update.lat = res.lat;
            this.update.lng = res.lng;
          }
        });
        this.location_ser.placeSearch(el).subscribe(res => {
          this.search_ready = true;
        },err => {
          console.log(err);
        });
      }
    },100);
  }
  async getNearbyOffices(){
    const offices = await this.office_ser.officesForService(this.content).toPromise().catch(console.log);
    if(!offices){
      console.log('err:', offices);
      console.log('could not load offices');
      this.location_err = true;
      this.note_ser.showNote("Couldn't load offices, if your location is disabled please enable it",10000);
      return false;
    }
    if(this.old_update && this.old_update.office_id){
      const found = offices.filter(item => {
        return item.id === this.old_update.office_id;
      });
      if(!found.length){
        const office = await this.con_ser.getContent(this.old_update.office_id).toPromise().catch(console.log);
        if(office){
          offices.unshift(office);
        }else{
          console.log('Cannot find old update office:', office);
        }
      }
    }
    this.offices = offices;
    this.loading = false;
  }
  openMap(){
    this.events.subscribe('map:result',(coords) => {
      this.update.lat = coords.lat;
      this.update.lng = coords.lng;
      this.events.unsubscribe('map:result');
    });
    if(this.update.lat && this.update.lng){
      this.navCtrl.push("MapPage",{coords: {lat: this.update.lat,lng:this.update.lng}});
    }else{
      this.navCtrl.push("MapPage");
    }
  }
  save(){
    (async ()=>{
      const update = this.initUpdateObj();
      console.log('update',update);
      const res = await this.con_up_ser.save(this.content.id,update).toPromise().catch(console.log);
      if(res && res.success){
        console.log('update saved:', res);
        if(this.old_update){
          this.events.publish('notifications:update',{
            id: this.old_update.id,
            actions_allowed: false,
          });
        }
        this.navCtrl.pop().catch(console.log);
      }else{
        console.log("error while saving update:", res);
      }
    })().catch(console.log);
  }
  initUpdateObj():IContentUpdate{
    let date = this.helpers.normalizeIso(this.date);
    let unix = this.helpers.toUnix(date);
    let update = <IContentUpdate>{
      status: 1,
      schedule: unix,
      actions_allowed: true,
    };
    if(this.update.office_id){
      update.office_id = this.update.office_id
    }
    if(this.update.details && this.update.details.length){
      update.details = this.update.details;
    }
    if(this.update.lat && this.update.lng){
      update.lat = this.update.lat;
      update.lng = this.update.lng;
    }

    if(this.old_update){
      update.status = 4;
      update.previous_id = this.old_update.id;
      update.parent_id = this.old_update.id;
      if(this.old_update.office_id){
        update.office_id = this.old_update.office_id;
      }
      if(this.old_update.parent_id){
        update.parent_id = this.old_update.parent_id;
      }
    }else{
      update.primary = true;
    }
    if(this.isOfficeUpdate() && this.old_update){
      update.status = this.old_update.status;
      if(this.old_update.status === 12){
        update.primary = false;
      }
    }
    return update;
  }
  getTitle():string{
    if(this.isOfficeUpdate() && this.old_update.status !== 15){
      return "Schedule meeting with the office";
    }else if(this.isOfficeUpdate() && this.old_update.status === 15){
      return "Schedule meeting";
    }else if(this.offered){
      return "Claim"
    }else{
      return "Respond"
    }
  }
  getButtonText():string{
    if(this.isOfficeUpdate()){
      return "Save";
    }else{
      return this.old_update? "Edit":"Send";
    }
  }
  getMapHeight(){
    let el_height = this.elementRef.nativeElement.offsetHeight;
    this.map_height = this.helpers.percent(el_height,40);
  }

}
