import {NgModule} from "@angular/core";
import {IonicModule, IonicPageModule} from "ionic-angular";
import {TakeActionPage} from "./take-action";
import {MapPageModule} from "../map/map.module";

@NgModule({
  declarations:[TakeActionPage],
  imports:[
    IonicPageModule.forChild(TakeActionPage),
    IonicModule,
    MapPageModule,
  ]
})
export class TakeActionPageModule{}