import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ConfirmActionPage } from './confirm-action';
import {BarcodeScanner} from "@ionic-native/barcode-scanner";
import { Vibration } from '@ionic-native/vibration';
import {ZXingScannerModule} from "@zxing/ngx-scanner";
import {QRCodeModule} from "angularx-qrcode";
@NgModule({
  declarations: [
    ConfirmActionPage,
  ],
  imports: [
    IonicPageModule.forChild(ConfirmActionPage),
    QRCodeModule,
    ZXingScannerModule,
  ],
  providers:[BarcodeScanner,Vibration]
})
export class ConfirmActionPageModule {}
