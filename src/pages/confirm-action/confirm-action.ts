import {Component, HostListener, OnInit, ViewChild} from '@angular/core';
import {Events, IonicPage, NavController, NavParams} from 'ionic-angular';
import {BarcodeScanner} from "@ionic-native/barcode-scanner";
import {NotificationService} from "../../providers/notification.service";
import {INotification} from "../../interfaces/INotification";
import { Vibration } from '@ionic-native/vibration';
import {HelpersService} from "../../providers/helpers.service";
import {IContentUpdate} from "../../interfaces/IContentUpdate";
import {LoadingService} from "../../providers/loading.service";
import {ContentUpdateService} from "../../providers/content-update.service";
import {IProfile} from "../../interfaces/IProfile";
import {AuthService} from "../../providers/auth.service";
import {ConfirmationService} from "../../providers/confirmation.service";
import {IReminder} from "../../interfaces/IReminder";
import {IConfirmation} from "../../interfaces/IConfirmation";
import {ZXingScannerComponent} from "@zxing/ngx-scanner";

@IonicPage()
@Component({
  selector: 'page-confirm-action',
  templateUrl: 'confirm-action.html',
})
export class ConfirmActionPage implements OnInit{
  reminder: IReminder;
  confirmation:IConfirmation;
  error: boolean;
  loading: boolean;
  profile: IProfile;
  toggle_br_scan: boolean;
  browser: boolean;
  @ViewChild('scanner')
  scanner: ZXingScannerComponent;
  availableCameras: Array<MediaDeviceInfo>;
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private barcodeScanner: BarcodeScanner,
              private vibration: Vibration,
              private note_ser:NotificationService,
              private helpers:HelpersService,
              private loading_ser:LoadingService,
              private con_up_ser:ContentUpdateService,
              private events:Events,
              private auth_ser:AuthService,
              private conf_ser:ConfirmationService,
              ) {
    this.reminder = this.navParams.get('reminder');
    this.confirmation = null;
    this.error = false;
    this.loading = true;
    this.toggle_br_scan = false;
    this.browser = false;
  }
  ngOnInit(){
    console.log('reminder:', this.reminder);
    this.initPage().catch(console.log);
    this.browser = this.helpers.platform === 'browser';

  }
  async initPage(){
    const profile = await this.auth_ser.getCurrentProfile().catch(console.log);
    if(!profile){
      console.log('could not load current logged in user profile');
      return;
    }
    this.profile = profile;
    const profiles = [];
    for(let profile of this.reminder.profiles){
      profiles.push(profile.id);
    }
    const update = this.reminder.content_update;
    const confs = await this.conf_ser.filter(profiles,update.content_id,update.id)
      .toPromise()
      .catch(console.log);
    if(confs && confs.length){
      this.confirmation = confs[0];
    }
    this.loading = false;
  }
  scan(){
    if(this.helpers.platform === 'browser'){
      if(this.toggle_br_scan){
        this.scanner.resetScan();
        this.toggle_br_scan = false;
      }else{
        this.toggle_br_scan = true;
        setTimeout(() => {
          this.scanner.resetScan();
          this.scanner.startScan(this.availableCameras[0]);
        },1000);
      }
    }else{
      this.barcodeScanner.scan({disableSuccessBeep: true})
        .then(res => {
          this.vibrate();
          if(res && res.text){
            if(this.confirmation.code === res.text){
              console.log('qr code matched');
              this.resolve().catch(console.log);
            }else{
              console.log('qr code does NOT match');
            }
          }else{
            console.log('qr code not found');
          }
        })
        .catch(console.log)
    }
  }
  selectCamera(cameras: Array<any>){
    this.availableCameras = cameras;
  }
  scanComplete(event){
   this.toggle_br_scan = false;
   console.log('scan complete:', event);
  }
  scanError(event){
    console.log('scan error:', event);
  }
  scanFailure(event){
    console.log('scan failure:', event);
  }
  scanSuccess(event){
    console.log('scan success:', event);
  }
  retry(){
    this.resolve().catch(console.log);
  }
  async resolve(){
    this.loading_ser.showLoading(true,'Please wait..').catch(console.log);
    let _update = this.reminder.content_update;
    let content_update: IContentUpdate = {
      previous_id: _update.id,
      parent_id: _update.id,
      office_id: _update.office_id,
      direct: _update.direct,
      status: _update.office_id? 6: 10,
      actions_allowed: false,
      details: _update.details,
      scheduled: _update.scheduled,
      schedule: _update.schedule,
      modified: false,
      lat: _update.lat,
      lng:_update.lng,
      primary: true,
    };
    if(_update.parent_id){
      content_update.parent_id = _update.parent_id;
    }
    if(this.profile.isOffice){
      // set as received by office:
      content_update.status = 7;
    }
    // if needy accepted the appointment:
    if(_update.status === 16){
      const hasOffice = _update.office && _update.office.author;
      if(hasOffice && this.profile.id === _update.office.author.id){
        // set as delivered:
        content_update.status = 8;
      }else{
        // set as received:
        content_update.status = 9;
      }
    }
    let content_id = _update.content.id;
    const save_res = await this.con_up_ser.save(content_id,content_update).toPromise().catch(console.log);
    this.loading_ser.showLoading(false).catch(console.log);
    if(save_res){
      this.events.publish('content_update:resolved', content_update.id);
      this.navCtrl.pop().catch();
    }else{
      this.error = true;
    }
  }
  vibrate(){
   setTimeout(() => {
     this.vibration.vibrate(100);
   },10)
  }
}
