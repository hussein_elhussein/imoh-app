import { Component, OnInit} from '@angular/core';
import {NavController, NavParams, LoadingController, Loading, AlertController, App, IonicPage} from 'ionic-angular';
import {FormBuilder, Validators, FormGroup} from "@angular/forms";
import {AuthService} from "../../providers/auth.service";
import {IRegister} from "../../interfaces/IRegister";
import {LoginPage} from "../login/login";
import {CountryService} from "../../providers/country.service";
import {IResponse} from "../../interfaces/IResponse";
import {LoadingService} from "../../providers/loading.service";
import {ProfileService} from "../../providers/profile.service";
import {IRole} from "../../interfaces/IRole";

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html'
})
export class RegisterPage implements OnInit{
  register:IRegister;
  registerForm:FormGroup;
  loading:Loading;
  email:string;
  password: string;
  roles:IRole[];
  selected_role:number;
  constructor(
    public navCtrl: NavController,
    public fb:FormBuilder,
    public navParams: NavParams,
    private auth_ser:AuthService,
    private profile_ser:ProfileService,
    public loadingCtrl:LoadingController,
    private alertCtrl: AlertController,
    private country_ser:CountryService,
    private loading_ser:LoadingService,
    private app:App,

  ) {
    this.register = {
      name:'',
      email:'',
      password:'',
      role_id: null,
    };
    this.roles = [];

  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad RegisterPage');
  }
  ngOnInit(){
    this.profile_ser.getRoles().subscribe(res => {

    });
    this.buildForm();
    //console.log(this.country_ser.getCountries());
  }
  DoRegistration():void{
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loading.present().catch(console.log);
    this.auth_ser.register(this.registerForm.value).subscribe((res:IResponse) => {
      if(res.success){
        //after successful registration, login:
        this.email = this.registerForm.value.email;
        this.password = this.registerForm.value.password;
        this.auth_ser.login(this.email,this.password).subscribe(res => {
          if(res){
            console.log('user logged in');
            this.loading_ser.showLoading(false);
          }else{
            this.loading_ser.showLoading(false);
            this.showError();
          }
        },err => {
          this.loading_ser.showLoading(false);
          if(err === 401){
            this.showError();
          }
        });
      }else{
        this.showError();
      }
    });
  }
  showError() {
    setTimeout(() => {
      this.loading.dismiss().catch(console.log);
    });

    let alert = this.alertCtrl.create({
      title: 'Fail',
      subTitle: "E-Mail already exist, if it's yours , please login instead",
      buttons: [
        {
          text: 'OK',
          role: 'cancel',
        },
        {
          text: 'Login',
          handler: () => {
          this.navCtrl.push(LoginPage, {email:this.registerForm.value.email}).catch(console.log);
          }
        },

      ]
    });
    alert.present().catch(console.log);
  }

  buildForm():void{
    this.registerForm = this.fb.group({
      'name': [this.register.name, [Validators.required]],
      'email': [this.register.email, [Validators.required]],
      'password': [this.register.password, [Validators.required]],
      'role': [this.register.role_id, [Validators.required]],
    });
  }

}
