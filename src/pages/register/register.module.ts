import {NgModule} from "@angular/core";
import {IonicModule, IonicPageModule} from "ionic-angular";
import {RegisterPage} from "./register";

@NgModule({
  declarations:[RegisterPage],
  imports:[
    IonicPageModule.forChild(RegisterPage),
    IonicModule
  ]
})
export class RegisterPageModule{}