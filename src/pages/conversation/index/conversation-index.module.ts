import {NgModule} from "@angular/core";
import {IonicModule, IonicPageModule} from "ionic-angular";
import {ConversationIndexPage} from "./conversation-index";
import {PipesModule} from "../../../pipes/pipes.module";
import {MomentModule} from "angular2-moment";
import {AttachmentService} from "../../../providers/attachment.service";
import {File} from "@ionic-native/file";
import {FileTransfer} from "@ionic-native/file-transfer";

@NgModule({
  declarations:[ConversationIndexPage],
  imports:[
    IonicPageModule.forChild(ConversationIndexPage),
    IonicModule,
    MomentModule,
    PipesModule,
  ]
})
export class ConversationIndexPageModule{}