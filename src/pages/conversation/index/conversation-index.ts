import {Component, OnInit} from '@angular/core';
import {
  NavController,
  NavParams,
  Events,
  ActionSheetController,
  IonicPage
} from 'ionic-angular';
import {AuthService} from "../../../providers/auth.service";
import {IProfile} from "../../../interfaces/IProfile";
import {LoadingService} from "../../../providers/loading.service";
import {ConversationService} from "../../../providers/conversation.service";
import {IConversation} from "../../../interfaces/IConversation";
import {IPagination} from "../../../interfaces/IPagination";
import {PaginationService} from "../../../providers/pagination.service";
import {IAuthor} from "../../../interfaces/IAuthor";
import {ConversationViewPage} from "../view/conversation-view";
import {IPrivateMessage} from "../../../interfaces/IPrivateMessage";
import {AttachmentService} from "../../../providers/attachment.service";
import {LocalConversationService} from "../../../providers/local-conversation.service";

@IonicPage()
@Component({
  selector: 'page-conversation-index',
  templateUrl: 'conversation-index.html',
  providers:[AttachmentService],
})
export class ConversationIndexPage implements OnInit{
  profile: IProfile;
  conversations:IConversation[];
  loading: boolean;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private auth_ser:AuthService,
    private events:Events,
    public actionSheetCtrl: ActionSheetController,
    private loading_ser:LoadingService,
    private local_conv:LocalConversationService,
    private conv_ser:ConversationService,
    public att_ser:AttachmentService,
  ) {
    this.loading = true;
    this.conversations = [];
  }
  ngOnInit(){
    this.initPage().catch(console.log);
  }
  async initPage(){
    const profile = await this.auth_ser.getCurrentProfile().catch(console.log);
    if(profile){
      this.profile = profile;
    }
    this.getItems(null,() => {
      this.loading = false;
    })
  }
  getItems(refresher?, callback?):void{
    let finish = () => {
      if(refresher){
        refresher.complete();
      }
      if(callback){
        callback();
      }
    };
    let initItems = async (conversations:IConversation[]) => {
      this.conversations = conversations;
      for(let item of this.conversations){
        if(item.last_message){
          const message = await this.getOriginalMessage(item.last_message.id,item.id).catch(console.log);
          if(message){
            item.last_message = message;
          }
        }
      }
      finish();
    };
    this.local_conv.getConvesations()
      .then((res) => {
        if(res){
            initItems(res).catch(console.log);
        }else{
          finish();
        }
      })
      .catch(err => {
        console.log(err);
        finish();
      })
  }
  presentOptions(item:IConversation):void {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Actions',
      buttons: [
        {
          text: 'Delete',
          icon:'trash',
          handler: () => {
            this.deleteConversation(item).catch(console.log)
          }
        },{
          text: 'Cancel',
          icon:'close-circle',
          role: 'cancel',
          handler: () => {
          }
        }
      ]
    });
    actionSheet.present().catch(console.log);

  }
  presentMultipleOptions(){
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Actions',
      buttons: [
        {
          text: 'Delete All',
          icon:'trash',
          handler: () => {
            this.deleteAll().catch(console.log);
          }
        },{
          text: 'Cancel',
          icon:'close-circle',
          role: 'cancel',
          handler: () => {
          }
        }
      ]
    });
    actionSheet.present().catch(console.log);
  }
  openItem(item:IConversation){
    console.log('to open:', item.id);
    this.navCtrl.push('ConversationViewPage',{conversation:item}).catch(console.log);
  }
  getName(item:IConversation){
    let name = "";
    if(item.opener.id === this.profile.id){
      if(item.receivers.length === 1){
        return item.receivers[0].name;
      }else{
        item.receivers.forEach((receiver,index,arr) => {
          if(index === arr.length){
            name += receiver.name;
          }else{
            name += receiver.name + ", ";
          }
        });
        return name;
      }
    }else{
      return item.opener.name;
    }
  }
  getReceiver(item: IConversation):IAuthor{
    if(item.opener.id === this.profile.id){
      return item.receivers[0];
    }else{
      return item.opener;
    }
  }
  async deleteConversation(conversation:IConversation){
    const deleted = await this.conv_ser.deleteConversation(conversation.id).toPromise().catch(console.log);
    if(!deleted){return;}
    const local_deleted = await this.local_conv.deleteConversation(conversation.id).catch(console.log);
    if(!local_deleted){return;}
    this.conversations = this.conversations.filter(item => {
      return item.id !== conversation.id;
    })
  }
  async deleteAll(){
    const ids = [];
    for(let conv of this.conversations){
      ids.push(conv.id);
    }
    if(!ids.length){return;}
    await this.local_conv.deleteAll().catch(console.log);
    await this.conv_ser.deleteConversations(ids).toPromise().catch(console.log);
  }
  async getOriginalMessage(message_id:number,conv_id:number):Promise<IPrivateMessage>{
    const conv = await this.local_conv.getConversation(conv_id).catch(console.log);
    if(!conv || !conv.messages.length){
      return;
    }
    return conv.messages.filter(item => {
      return item.id === message_id;
    })[0];
  }
  getLastMessageText(message:IPrivateMessage): string{
    if(message){
      if(message.text){
        return message.text;
      }else if(this.att_ser.isAudio(message)){
        return message.attachment.duration_humanized;
      }else if(this.att_ser.isImage(message)){
        return "Image";
      }else if(this.att_ser.isFile(message)){
        return "File";
      }
    }else {
      return "";
    }
  }

}
