import {NgModule} from "@angular/core";
import {ConversationViewPage} from "./conversation-view";
import {IonicModule, IonicPageModule} from "ionic-angular";
import {ComponentsModule} from "../../../components/components.module";
import {VUploaderModule} from "../../../v-uploader/v-uploader.module";
import {PipesModule} from "../../../pipes/pipes.module";
import {MomentModule} from "angular2-moment";
import {Diagnostic} from "@ionic-native/diagnostic";
import {FileChooser} from "@ionic-native/file-chooser";
import {PhotoViewer} from "@ionic-native/photo-viewer";
import {Camera} from "@ionic-native/camera";
import {MediaService} from "../../../providers/media.service";
import {RecorderService} from "../../../providers/recorder.service";
import {PlayerService} from "../../../providers/player.service";

@NgModule({
  declarations:[ConversationViewPage],
  imports:[
    IonicPageModule.forChild(ConversationViewPage),
    IonicModule,
    MomentModule,
    PipesModule,
    ComponentsModule,
    VUploaderModule,
  ],
  providers:[
    Diagnostic,
    MediaService,
    Camera,
    FileChooser,
    PhotoViewer,
    RecorderService,
    PlayerService
  ]
})
export class ConversationViewPageModule{}