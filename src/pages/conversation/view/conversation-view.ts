import {Component, ViewChild, NgZone} from '@angular/core';
import {
  Content, NavController, Events, NavParams, ActionSheetController,
  ToastController, ModalController, IonicPage
} from 'ionic-angular';

import {AuthService} from "../../../providers/auth.service";
import {SettingsService} from "../../../providers/settings.service";
import {IProfile} from "../../../interfaces/IProfile";
import {Camera} from "@ionic-native/camera";
import {BlobHelper} from "../../../v-uploader/services/blob-helper.service";
import {VUploaderService} from "../../../v-uploader/services/v-uploader.service";
import {IPrivateMessage} from "../../../interfaces/IPrivateMessage";
import {FileChooser} from "@ionic-native/file-chooser";
import { PhotoViewer } from '@ionic-native/photo-viewer';
import {File, FileEntry, IFile} from "@ionic-native/file";
import {IAttachment} from "../../../interfaces/IAttachment";
import {Storage} from "@ionic/storage";
import {Diagnostic} from "@ionic-native/diagnostic";
import {Network} from "@ionic-native/network";
import {Observable} from "rxjs";
import {SocketService} from "../../../providers/socket.service";
import {ConversationService} from "../../../providers/conversation.service";
import {IConversation} from "../../../interfaces/IConversation";
import {IMError} from "../../../interfaces/IMError";
import {IMStatus} from "../../../interfaces/IMStatus";
import {HelpersService} from "../../../providers/helpers.service";
import {AttachPreviewPage} from "../../attach-preview/attach-preview";
import {LoadingService} from "../../../providers/loading.service";
import {Subject} from "rxjs/Subject";
import {IAuthor} from "../../../interfaces/IAuthor";
import {IPagination} from "../../../interfaces/IPagination";
import {NotesService} from "../../../providers/notes.service";
import {AttachmentService} from "../../../providers/attachment.service";
import {MediaService} from "../../../providers/media.service";
import {RecorderService} from "../../../providers/recorder.service";
import {PlayerService} from "../../../providers/player.service";
import {LocalConversationService} from "../../../providers/local-conversation.service";
import {Subscription} from "rxjs/Subscription";

@IonicPage()
@Component({
  selector: 'page-conversation-view',
  templateUrl: 'conversation-view.html',
  providers: [],
})

export class ConversationViewPage {
  @ViewChild(Content) content: Content;
  chat:string;
  currentUser:IProfile;
  progress:Array<Subject<number>>;
  playngPercentage:number;
  autoDownload: boolean;
  recording:boolean;
  recordCanceled: boolean;
  recordingTime: string;
  record_path: any;
  messErrors: IMError[];
  conversation:IConversation;
  loading: boolean;
  viewers: IAuthor[];
  pagination: IPagination;
  date_sub: Subject<any>;
  subscriptions: Array<Subscription>;
  new_messages:IPrivateMessage[];
  x: number = 50;
  y: number = 50;
  constructor(
    public navCtrl: NavController,
    public navParams:NavParams,
    public auth_ser:AuthService,
    public settings:SettingsService,
    public events:Events,
    private helpers:HelpersService,
    public actionSheetCtrl: ActionSheetController,
    private blob_ser:BlobHelper,
    private camera:Camera,
    private photoViewer: PhotoViewer,
    private natFile:File,
    private fileChooser: FileChooser,
    private toastCtrl:ToastController,
    public modalCtrl: ModalController,
    private up_ser:VUploaderService,
    public storage: Storage,
    private diagnostic: Diagnostic,
    private network: Network,
    public socket_ser:SocketService,
    private conv_ser:ConversationService,
    private local_conv:LocalConversationService,
    private loading_ser:LoadingService,
    private note_ser:NotesService,
    private zone: NgZone,
    public att_ser:AttachmentService,
    private media:MediaService,
    private recorder: RecorderService,
    public player: PlayerService,
  ) {
    this.chat = "";
    this.recording = false;
    this.recordingTime = '';
    this.recordCanceled = false;
    this.record_path = [];
    this.messErrors = [];
    this.viewers = [];
    this.loading = true;
    this.date_sub = new Subject<any>();
    this.progress = [];
    this.subscriptions = [];
    this.new_messages = [];
    //todo: set new messages as unread in order for when fetching new messages, those wont be fetched
  }

  ngOnInit(){
    this.storage.get('auto_download')
      .then(auto => {
        this.autoDownload = auto;
      })
      .catch(err => {
        console.log('error while trying to check auto download:');
      });
    this.auth_ser.getCurrentProfile().then(res => {
      this.currentUser = res;
      if(this.navParams.get('conversation')){
        this.populateFields().catch(console.log);
      }else{
        this.initConversation().catch(console.log);
      }
      this.listenToEvents();
      this.listenToTransferEvents();
    });
  }
  ngOnDestroy(){
    for(let item of this.subscriptions){
      item.unsubscribe();
    }
  }
  ionViewWillLeave(){
    if(this.conversation){
      this.socket_ser.left(this.conversation,this.currentUser.id);
      this.events.publish('conversation:left', this.conversation.id)
    }
  }
  async initConversation(){
    let initPag = () => {
      this.pagination = {
        data: [],
        links: {
          first: null,
          last: null,
          next: null,
          prev: null,
        },
        meta:{
          current_page: null,
          from:0,
          last_page: null,
          path: null,
          per_page: null,
          to: null,
          total: null,
        }
      };
    };
    let receiver = this.navParams.get('receiver');
    let opener:IAuthor = {
      id: this.currentUser.id,
      name: this.currentUser.name,
      avatar: this.currentUser.avatar,
      location: null,
      content_type_id: this.currentUser.content_type_id,
      last_active: this.helpers.toTimeStamp(this.helpers.momentObj()),
    };
    const receivers = [receiver];
    const conversation = await this.local_conv.initConversation(receivers,opener).catch(console.log);
    if(conversation){
      this.conversation = conversation;
      if(conversation.messages){
        this.pagination = this.local_conv.getMessagesPage(1,5, conversation.messages);
        setTimeout(() => {
          this.scroll();
        },500)
      }else{
        initPag();
      }
      setTimeout(() => {
        this.loading = false;
      },2000);
    }else{
      initPag();
    }
    return true;
  }
  async populateFields(){
    const ser_copy = <IConversation> this.navParams.get('conversation');
    const saved = <IConversation> await this.local_conv.getConversation(ser_copy.id).catch(console.log);
    if(saved){
      this.conversation = saved;
      this.conversation.viewers = ser_copy.viewers;
      this.conversation.last_message = ser_copy.last_message;
      this.conversation.profiles = saved.profiles;
      this.conversation.receivers = saved.receivers;
      this.conversation.initialized = true;
      this.conversation.opened = true;
    }
    for(let viewer of ser_copy.viewers){
      if(viewer.id !== this.currentUser.id){
        this.viewers.push(viewer);
      }
    }
    this.pagination = this.local_conv.getMessagesPage(1,10,saved.messages);
    this.socket_ser.entered(ser_copy,this.currentUser.id);
    this.events.publish('conversation:entered', ser_copy.id);
    setTimeout(() => {
      this.scroll(1000);
    },300);
    setTimeout(() => {
      this.loading = false;
    },5000);
  }
  // async getNewMessages(){
  //   const unreads = await this.local_conv.getUnreadMessagges(this.conversation.id).catch(console.log);
  //   if(!unreads){return;}
  //   for(let unread of unreads){
  //     this.new_messages.push(unread);
  //   }
  // }
  // async setRead(){
  //   const unreads = await this.local_conv.getUnreadMessagges(this.conversation.id).catch(console.log);
  //   if(!unreads){return;}
  //   const ids = [];
  //   for(let unread of unreads){
  //     ids.push(unread.id);
  //   }
  //   if(ids.length){
  //     await this.conv_ser.setRead(ids).toPromise().catch(console.log);
  //   }
  // }
  canLoadNext():boolean{
    let can = false;
    if(!this.loading && this.pagination && this.pagination.data && this.pagination.data.length > 5){
      can = true;
    }
    return can;
  }
  loadNext(infiniteScroll):Promise<any>{
    return new Promise((resolve,reject) => {
      if(this.pagination && this.pagination.meta.last_page != this.pagination.meta.current_page){
        const old_data = this.pagination.data;
        const pag = this.local_conv.getMessagesNextPage(this.pagination, this.conversation.messages);
        this.pagination = pag;
        this.pagination.data = this.pagination.data.concat(old_data);
        setTimeout(() => {
          resolve();
          infiniteScroll.complete();
        },1000);
      }else{
        setTimeout(() => {
          resolve();
          infiniteScroll.complete();
          infiniteScroll.enable(false);
        },1000);
      }
    });
  }
  listenToEvents(){
    const conv_entered = this.socket_ser.onConversationEntered.subscribe(res => {
      if(res.id === this.conversation.id && res.profile_id !== this.currentUser.id){
        this.conversationEntered(res);
      }
    });
    const conv_left = this.socket_ser.onConversationLeft.subscribe(res => {
      if(res.id === this.conversation.id && res.profile_id !== this.currentUser.id){
        this.conversationLeft(res);
      }
    });
    const on_message = this.socket_ser.onMessage.subscribe(res => {
      if(res.conversation_id === this.conversation.id){
        this.receiveMessage(res);
      }
    });
    this.subscriptions = this.subscriptions.concat([conv_entered,conv_left,on_message]);
  }
  listenToTransferEvents(){
    const start_sub = this.att_ser.onStart.subscribe(updated => {
      console.log('start fired');
      this.progress[updated.inAppID] = new Subject<number>();
      this.updateMessage(updated).catch(console.log);
    });
    const progress_sub = this.att_ser.onProgress.subscribe(updated => {
      this.zone.run(() => {
        this.progress[updated.inAppID].next(updated.attachment.progress);
      });
    });
    const finish_sub = this.att_ser.onFinish.subscribe(updated => {
      this.updateMessage(updated).catch(console.log);
      if(updated.attachment.uploaded){
        this.send(null,updated);
      }
      if(updated.attachment.downloaded){
        if(this.att_ser.isAudio(updated)){
          this.player.getAudioDuration(updated.attachment.local_path).subscribe(res => {
            updated.attachment.duration = res;
            this.updateMessage(updated).catch(console.log);
          },console.log);
        }
      }
    });
    const error_sub = this.att_ser.onError.subscribe(updated => {
      console.log('error while transfering:', updated);
      this.updateMessage(updated).catch(console.log);
    });
    this.subscriptions = this.subscriptions.concat([start_sub,progress_sub,finish_sub,error_sub])
  }
  async receiveMessage(_message:IPrivateMessage){
    const message = await this.local_conv.addNewMessage(_message,this.conversation,false).catch(console.log);
    if(message){
      console.log('message:', message);
      this.updateMessage(message).catch(console.log);
    }
  }
  conversationEntered(res: any){
    if(this.conversation.receivers.length === 1){
      let viewer = this.conversation.receivers.filter(item => {
        return item === res.profile_id;
      });
      if(!viewer.length){
        viewer.push(this.conversation.opener);
      }
      this.viewers.push(viewer[0]);
      this.setAllRead();
    }
  }
  conversationLeft(res:any){
    if(this.conversation.receivers.length === 1){
      this.viewers = this.viewers.filter(item => {
        return item.id !== res.profile_id;
      });
    }
  }
  isReading():boolean{
    if(this.conversation){
      return this.viewers.length === this.conversation.receivers.length;
    }else{
      return false;
    }
  }
  isMultiReceivers():boolean{
    const receivers = this.conversation.receivers.filter(item => {
      return item.id !== this.currentUser.id;
    });
    return receivers.length > 1;
  }
  setAllRead(){
    for(let message of this.conversation.messages){
      if(message.status){
        message.status.seen = true;
        message.status.received = true;
      }else{
        message.status = <IMStatus> {
          message_id: message.id,
          received: true,
          seen: true,
          sent: true,
        };
      }
      this.updateMessage(message).catch(console.log);
    }
  }
  openConv():Observable<boolean>{
    let sub = new Observable<boolean>(observer => {
      let opened = false;
      let loaded = 0;
      let checkDone = ()=>{
        if(loaded === 2){
          this.socket_ser.entered(this.conversation,this.currentUser.id);
          this.events.publish('conversation:entered', this.conversation.id);
          observer.next(opened);
          observer.complete();
        }
      };
      if(this.conversation.initialized){
        observer.next(this.conversation.opened);
        observer.complete();
      }else{
        this.socket_ser.onConversationOpened.subscribe(res => {
          let interval = setInterval(() => {
            if(this.conversation.id){
              if(res.id === this.conversation.id){
                opened = true;
                this.conversation.opened = opened;
              }
              loaded +=1;
              checkDone();
              clearInterval(interval);
            }
          },200);
        });
        this.conv_ser.save(this.conversation).subscribe((saved:IConversation) => {
          this.conversation.id = saved.id;
          this.conversation.socket_id = saved.socket_id;
          this.conversation.initialized = true;
          this.local_conv.save(this.conversation).then(() => {
            loaded +=1;
            checkDone();
          }, console.log)
        });
      }
    });
    return sub;
  }
  send(v, messageWithAtt: IPrivateMessage = null){
    this.chatSend(v,messageWithAtt)
      .then((res) => {
        if(!res){
          console.log('could not send message');
        }
      })
      .catch(console.log);
  }
  async chatSend(v, messageWithAtt?: IPrivateMessage):Promise<any>{
    let message = <IPrivateMessage>{};
    if(v){
      message = this.local_conv.initMessage(this.currentUser,
        this.conversation,
        true,true,this.isReading());
      message.text = v.chatText;
    }else{
      message = messageWithAtt;
    }
    message.status.state.processing = true;
    message.status.state.sending = true;
    this.chat = '';
    await this.updateMessage(message).catch(console.log);
    this.scroll();
    const conv = await this.openConv().toPromise().catch(console.log);
    if(!conv){
      return false;
    }
    if(!message.conversation_id){
      message.conversation_id = this.conversation.id;
      message.conversation_socket_id = this.conversation.socket_id;
    }
    const clone = <IPrivateMessage>this.helpers.clone(message);
    if(clone.attachment){
      clone.attachment = <IAttachment>{
        blob_ext: message.attachment.blob_ext,
        path: message.attachment.path,
        blob_type: message.attachment.blob_type,
        duration_humanized: message.attachment.duration_humanized,
      };
    }
    clone.status = null;
    clone.author = null;
    clone.inAppID = null;
    //send
    const res = await this.conv_ser.sendMessage(clone).toPromise().catch(console.log);
    if(res){
      clone.id = res.id;
      this.socket_ser.sendMessage(clone);
      message.author = null;
      message.id = res.id;
      message.status.message_id = res.id;
      message.status.state.sending = false;
      message.status.state.processing = false;
      message.status.sent = true;
      if(this.isReading()){
        message.status.seen = true;
      }
      this.conversation.last_message = message;
      await this.updateMessage(message,true).catch(console.log);
    }else{
      message.error = {
        error_code: null,
        error_message: "connection lost",
        handled: true,
        retry: false
      };
      message.status.state.processing = false;
      await this.updateMessage(message, true).catch(console.log);
    }
    return true;
  }
  isErrored(message:IPrivateMessage):boolean{
    if(message.error && (message.error.error_code || message.error.error_message)){
      return true;
    }
    return false
  }
  gotoUserProfile(id:number){
    this.navCtrl.push("ProfileViewPage", {id:id});
  }
  presentAttOptions():void {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Attach',
      buttons: [
        {
          text: 'File',
          icon:'document',
          handler: () => {
            this.attachFile().catch(console.log);
          }
        },{
          text: 'Take photo',
          icon:'camera',
          handler: () => {
            this.attachImage(this.camera.PictureSourceType.CAMERA).catch(console.log);
          }
        },{
          text: 'Gallery',
          icon:'image',
          handler: () => {
            this.attachImage(this.camera.PictureSourceType.PHOTOLIBRARY).catch(console.log);
          }
        },{
          text: 'Cancel',
          icon:'close-circle',
          role: 'cancel',
          handler: () => {
          }
        }
      ]
    });
    actionSheet.present().catch(console.log);

  }
  async attachFile():Promise<boolean>{
    const uri = await this.fileChooser.open().catch(console.log);
    if(!uri){
      return false;
    }
    const fileEntry = <FileEntry>await this.natFile.resolveLocalFilesystemUrl(uri).catch(console.log);
    if(!fileEntry){
      return false;
    }
    const file = <IFile> await new Promise<IFile>((resolve, reject) => {
      fileEntry.file(resolve,reject)})
      .catch(console.log);
    if(!file){
      return false;
    }
    const ext = file.type.split('/')[1];
    const base64 = await this.blob_ser.getBase64(file).catch(console.log);
    const attachment = <IAttachment>{
      blob: fileEntry,
      blob_type: file.type == 'image/jpeg' || file.type == 'image/jpg' ? 'image': 'file',
      blob_ext: ext,
      preview:base64,
      source_type: 2,
      finished: false,
      progress: 0,
    };
    const message = this.local_conv.initMessage(this.currentUser,
      this.conversation,
      this.isReading(),
      true,
      false);
    message.attachment = attachment;
    this.presentAttachPreview(message);
  }
  async attachImage(sourceType){
    const options = {
      quality: 50,
      targetWidth: 800,
      targetHeight: 800,
      destinationType: this.camera.DestinationType.DATA_URL,
      mediaType: sourceType,
      sourceType:sourceType,
      encodingType: this.camera.EncodingType.JPEG,
      saveToPhotoAlbum: false,
      correctOrientation: true,
    };
    // Get the data of an image
    const imageData = await this.camera.getPicture(options).catch(console.log);
    if(!imageData){
      this.note_ser.showNote('Error while selecting image: ' + imageData);
    }
    const blob = this.blob_ser.convertToBlob(imageData,'image/jpeg');
    const image = "data:image/jpeg;base64," + imageData;
    const attachment = <IAttachment>{
      blob: blob,
      blob_type:'image',
      blob_ext:'jpeg',
      preview: image,
      finished: false,
      progress: 0,
    };
    console.log('blob', blob);
    const messageData = this.local_conv.initMessage(this.currentUser,
      this.conversation,
      this.isReading(),
      true,
      false);
    messageData.attachment = attachment;
    this.presentAttachPreview(messageData);
  }
  presentAttachPreview(message: IPrivateMessage){
    let showCopyErr = (err) => {
      this.note_ser.showNote("Couldn't copy the file!");
      console.log(err);
    };
    let finish = (im: IPrivateMessage)=>{
      if(im !== undefined){
        if(!im.text || !im.text.length){
          delete im.text;
        }
        this.updateMessage(im).then(() => {
          this.att_ser.upload(im);
        }).catch(console.log);
      }
    };
    if(this.helpers.platform === 'browser'){
      const modal = this.modalCtrl.create("AttachPreviewPage", message);
      modal.onDidDismiss((_message:IPrivateMessage) => {
        finish(_message);
      });
      modal.present().catch(console.log);
    }else{
      this.events.unsubscribe('preview:done');
      this.events.subscribe('preview:done',(_message:IPrivateMessage) => {
        this.att_ser.copyAttachment(_message)
          .then(path => {
            _message.attachment.generated_path = path;
            _message.attachment.local_path = path['full'];
            finish(_message);
          });
      });
      this.navCtrl.push("AttachPreviewPage",message).catch(console.log);
    }
  }
  startAudRec(ev):void{
    const message = this.local_conv.initMessage(this.currentUser,
      this.conversation,
      this.isReading(),
      false,
      false);
    const timeSub = this.recorder.onTimeUpdate.subscribe(time => {
      this.zone.run(() => {
        this.recordingTime = time;
      });
    });
    const sub = this.recorder.onStop.subscribe((im: IPrivateMessage) => {
      setTimeout(() => {
        this.zone.run(() => {
          this.recording = false;
          this.recordingTime = '00:00';
        });
      },100);
      sub.unsubscribe();
      timeSub.unsubscribe();
      if(im){
        this.updateMessage(im).catch(console.log);
        this.att_ser.upload(im);
      }
    });
    this.recorder.recordAudio(message);
    this.recordingTime = '00:00';
    this.recording = true;
  }
  stopAudRec(ev):void{
    this.recorder.stopAudRec();
  }
  cancelAudioRec(ev = null):void{
   this.recorder.cancelAudioRec(ev);
  }
  canPlay(message:IPrivateMessage):boolean{
    if(message.sender === this.currentUser.id){
      return true;
    }else{
      return message.attachment.downloaded;
    }
  }
  playMedia(message: IPrivateMessage){
    if(this.player.isPlaying(message)){
      console.log('paused');
      this.player.pause();
    }else if(this.player.isPaused(message)) {
      console.log('resumed');
      this.player.resume();
    }else if(this.player.isPlaying()){
      console.log('replaying');
      this.player.stop();
      this.playngPercentage = 0;
      setTimeout(()=>{
        this.player.play(message).subscribe(null,(err) => {
          this.note_ser.showNote(err);
        });
      },100);
    }else{
      this.player.play(message).subscribe(null,(err) => {
        this.note_ser.showNote(err);
      });
    }
    this.player.onPercentageUpdate.subscribe(res => {
      this.zone.run(()=>{
        this.playngPercentage = res;
      })
    });
    this.player.onFinish.subscribe(() => {
      this.zone.run(()=>{
        this.playngPercentage = 0;
      });
    });
  }
  getPreviewSource(message:IPrivateMessage):string{
    if(message.attachment.blob_type === 'image'){
      return message.attachment.local_path;
    }
    return;
  }
  attemptToSend(message:IPrivateMessage){
    message.status.state.processing = true;
    message.error.error_message = null;
    message.error.error_code = null;
    message.error.retry = true;
    if(message.status.state.sending){
      this.send(null, message);
    }else if(message.status.state.downloading){
      this.att_ser.download(message);
    }else if(message.status.state.uploading){
      this.att_ser.upload(message);
    }
  }
  getFullURL(url:string):string{
    return this.settings.storage_url + url;
  }
  scroll(duration: number = 300){
    if(this.content && this.content._scroll){
      this.content.scrollToBottom(duration).catch((err) => {
        console.log('scroll error:', err);
        console.log('retrying to scroll');
        setTimeout(() => {
          this.scroll(duration);
        },1000);
      });
    }
  }
  async updateMessage(message:IPrivateMessage, skipPag = false):Promise<boolean>{
    if(!skipPag){
      this.updatePagMessage(message);
    }
    let found = false;
    let messagesCount = this.conversation.messages.length;
    //look for the message and update it
    if(messagesCount > 0){
      this.conversation.messages.forEach((item,index,messages) => {
        if(messages[index].inAppID == message.inAppID){
          messages[index] = message;
          found = true;
        }
      });
    }
    if(!found){
      let cloned: IPrivateMessage = null;
      if(message.attachment){
        cloned = this.helpers.clone(message);
        cloned.attachment = <IAttachment>{
          blob_ext: message.attachment.blob_ext,
          blob_type: message.attachment.blob_type,
          path: message.attachment.path,
          local_path: message.attachment.local_path,
          duration: message.attachment.duration,
        }
      }else{
        cloned = message;
      }
      this.conversation.messages.push(cloned);
    }
    await this.local_conv.updateConversation(this.conversation).catch(console.log);
    return true;
  }
  updatePagMessage(message:IPrivateMessage){
    let found = false;
    let messagesCount = this.pagination.data.length;

    //look for the message and update it
    if(messagesCount > 0){
      this.pagination.data.forEach((item,index,messages) => {
        if(messages[index].inAppID == message.inAppID){
          this.zone.run(() => {
            messages[index] = message;
          });
          found = true;
        }
      });
    }
    //insert new message if not found:
    if(!found){
      this.pagination.data.push(message);
      this.loading = true;
      setTimeout(()=>{
        this.scroll();
      },300);
      setTimeout(() => {
        this.loading = false;
      },1000)
    }

    // if it has attachment, download download it:
    if(this.canDownload(message) && !this.isErrored(message)){
      this.att_ser.download(message);
    }
  }
  canDownload(message: IPrivateMessage):boolean{
    if(message.sender !== this.currentUser.id){
      return message.attachment &&
        !message.attachment.downloaded &&
        !message.status.state.downloading;
    }
    return false;
  }
  isDownloading(message:IPrivateMessage){
    return message.attachment &&
      !message.attachment.downloaded &&
      message.status &&
      message.status.state.downloading &&
      !this.isErrored(message);
  }
  isUploading(message:IPrivateMessage){
    return message.attachment &&
      !message.attachment.uploaded &&
      message.status &&
      message.status.state.uploading &&
      !this.isErrored(message);
  }
  isSeen(message:IPrivateMessage){
    return message.status && message.status.seen && message.sender === this.currentUser.id
  }
  isSent(message:IPrivateMessage){
    return message.status &&
      message.status.sent &&
      !this.isSeen(message) &&
      message.sender === this.currentUser.id;
  }
  openAttachment(message: IPrivateMessage):void{
    if(message.text){
      this.photoViewer.show(message.attachment.local_path, message.text, {share: false});
    }else{
      this.photoViewer.show(message.attachment.local_path,null, {share: false});
    }
  }
  idToAuthor(id:number):IAuthor{
    let receiver = null;
    for(let _receiver of this.conversation.receivers){
      if(_receiver.id === id){
        receiver = _receiver;
      }
    }
    if(receiver){
      return receiver
    }else{
      return this.conversation.opener;
    }
  }
  getReceivers():IAuthor[]{
    if(this.conversation){
      const receivers = this.conversation.receivers.filter(item => {
        return item.id !== this.currentUser.id;
      });
      if(!receivers.length){
        receivers.push(this.conversation.opener);
      }
      return receivers;
    }
    return [];
  }
  async getPermission(permission: string):Promise<boolean>{
    //camera
    if(permission === 'camera'){
      const auth = await this.diagnostic.isCameraAuthorized(true).catch(console.log);
      if(auth){
        return true;
      }else{
        const granted = await this.diagnostic.requestCameraAuthorization(true).catch(console.log);
        return granted && granted !== "DENIED_ALWAYS";
      }
    }
    //audio
    if(permission == 'audio'){
      const auth = await this.diagnostic.isMicrophoneAuthorized().catch(console.log);
      if(auth){
        return true;
      }else{
        const granted = await this.diagnostic.getMicrophoneAuthorizationStatus().catch(console.log);
        return granted && granted !== "DENIED_ALWAYS";
      }
    }
    return false;
  }
  onPan(event: any): void {
    this.x = event.x;
    this.y = event.y;
  }
  isNew(date: any):boolean{
    return this.helpers.isNew(date);
  }
  isYesterday(date: any):boolean{
    return this.helpers.diffinDays(date) === 1;
  }

}
