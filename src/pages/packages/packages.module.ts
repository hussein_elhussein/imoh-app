import {NgModule} from "@angular/core";
import {IonicModule, IonicPageModule} from "ionic-angular";
import {MomentModule} from "angular2-moment";
import {PackagesPage} from "./packages";
import {PipesModule} from "../../pipes/pipes.module";
import {ComponentsModule} from "../../components/components.module";
import {PackageService} from "../../providers/package.service";

@NgModule({
  declarations:[PackagesPage],
  imports:[
    IonicPageModule.forChild(PackagesPage),
    MomentModule,
    PipesModule,
    ComponentsModule,
  ],
  providers: [PackageService]
})
export class PackagesModule{}