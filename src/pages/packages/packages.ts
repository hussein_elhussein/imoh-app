import {Component, OnInit} from '@angular/core';
import {AlertController, Events, ModalController, NavController, NavParams} from 'ionic-angular';
import {ReminderService} from "../../providers/reminder-service";
import {IPagination} from "../../interfaces/IPagination";
import {PaginationService} from "../../providers/pagination.service";
import {IContentType} from "../../interfaces/IContentType";
import {IProfile} from "../../interfaces/IProfile";
import {Storage} from "@ionic/storage";
import {HelpersService} from "../../providers/helpers.service";
import {AuthService} from "../../providers/auth.service";
import {LoadingService} from "../../providers/loading.service";
import {IonicPage} from 'ionic-angular';
import {PackageService} from "../../providers/package.service";
import {IPackage} from "../../interfaces/IPackage";
import {IContentUpdate} from "../../interfaces/IContentUpdate";
import {IQRCode} from "../../interfaces/IQRCode";
import {NotesService} from "../../providers/notes.service";

@IonicPage()
@Component({
  selector: 'page-packages',
  templateUrl: 'packages.html',
})
export class PackagesPage implements OnInit{
  items: IPackage[];
  backUp_items: IPackage[];
  pagination:IPagination;
  loading: boolean;
  types:IContentType[];
  profile:IProfile;
  searchInput: string;
  shouldShowCancel: boolean;
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private rem_ser:ReminderService,
              private pag_ser:PaginationService,
              private storage:Storage,
              private helpers:HelpersService,
              private auth_ser:AuthService,
              private modalCtrl:ModalController,
              private alertCtrl:AlertController,
              private loading_ser:LoadingService,
              private pac_ser:PackageService,
              private events:Events,
              private note_ser:NotesService) {
    this.items = [];
    this.backUp_items = [];
    this.loading = true;
    this.searchInput = '';
    this.shouldShowCancel = false;
  }
  ngOnInit(){
    //todo: search for profiles by using code scanner for browser in ConfirmAction
    this.initPage().catch(console.log);
    this.events.subscribe('scan_result',(code:IQRCode) => {
      this.filterById(code.id);
    });
  }
  filterById(id: number){
    const pack = this.items.filter(item => {
      return item.id === id;
    });
    if(pack.length){
      this.items = pack;
      this.searchInput = pack[0].to.qr_code;
      this.shouldShowCancel = true;
    }else{
      this.note_ser.showNote("This profile doesn't have packages");
    }
  }
  async initPage(){
    await this.getProfile().catch(console.log);
    this.getItems(null,() => {
      this.loading = false;
    })
  }
  async getProfile(){
    const profile = <IProfile> await this.auth_ser.getCurrentProfile().catch(console.log);
    if(profile){
      this.profile = profile;
    }
  };
  displayAppointmentPanel(update:IContentUpdate){
    update.status = 15;
    if(this.helpers.platform === 'browser'){
      let modal = this.modalCtrl.create("TakeActionPage",{update: update});
      modal.present().catch(console.log);
    }else{
      this.navCtrl.push("TakeActionPage",{update: update}).catch(console.log);
    }
  }
  isDelivered(item:IPackage): boolean{
    return item.status === 2;
  }
  getItems(refresher?,callback?){
    let finish  = ()=> {
      if(refresher){
        refresher.complete();
      }
      if(callback){
        callback();
      }
    };
    this.pac_ser.index().subscribe(pagination => {
      this.items = pagination.data;
      this.backUp_items = pagination.data;
      this.pagination = pagination;
      this.pagination.data = null;
      finish();
    },err => {
      finish();
      console.log('Error while fetching reminders:', err);
    });
  }
  loadNext(infiniteScroll):Promise<any>{
    return new Promise((resolve,reject) => {
      if(this.pagination.meta.last_page != this.pagination.meta.current_page){
        this.pag_ser.next(this.pagination.links.next).subscribe(res => {
          this.items = this.items.concat(res.data);
          this.pagination = res;
          this.pagination.data = null;
          resolve();
          infiniteScroll.complete();
        },err => {
          infiniteScroll.complete();
          console.log('Error while fetching reminders:', err);
        });
      }else{
        resolve();
        infiniteScroll.complete();
      }
    });
  }
  onSearchInput(){
    const hasText = this.searchInput && this.searchInput.trim() != '';
    if(hasText){
      this.items = this.items.filter((item) => {
        let anInput = this.searchInput.toLowerCase();
        let to_qr_code = item.to.qr_code;
        let to_name = item.to.name.toLowerCase();
        let from_qr_code = item.to.qr_code;
        let from_name = item.to.name.toLowerCase();
        return (
          to_qr_code.indexOf(anInput) > -1
          ||
          to_name.indexOf(anInput) > -1
          ||
          from_qr_code.indexOf(anInput) > -1
          ||
          from_name.indexOf(anInput) > -1
        );
      });
      this.shouldShowCancel = true;
    }else{
      this.shouldShowCancel = false;
      this.items = this.backUp_items;
    }
  }
  onSearchCancel(){
    this.searchInput = '';
    this.shouldShowCancel = false;
    this.items = this.backUp_items;
  }
  toggleScan(){
    const codes: IQRCode[] = [];
    for(let item of this.items){
      const code = <IQRCode>{
        id: item.id,
        code: item.to.qr_code,
      };
      codes.push(code);
    }
    if(this.helpers.platform === 'browser'){
      let modal = this.modalCtrl.create("ScanPage",{codes: codes});
      modal.present().catch(console.log);
    }else{
      this.navCtrl.push("ScanPage",{codes: codes}).catch(console.log);
    }
  }
}
