import {Component, OnInit} from '@angular/core';
import {
  NavController,
  NavParams,
  AlertController,
  App,
  IonicPage
} from 'ionic-angular';
import {AuthService} from "../../providers/auth.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {LoadingService} from "../../providers/loading.service";
import {RegisterPage} from "../register/register";

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage implements OnInit{
  loginForm:FormGroup;
  email: string;
  password: string;
  constructor(
    public navCtrl: NavController,
    public fb:FormBuilder,
    public navParams: NavParams,
    private auth_ser:AuthService,
    private alertCtrl: AlertController,
    private app:App,
    private loading_ser:LoadingService
  ) {
    this.email = "";
    this.password = "";
  }
  ngOnInit(){
    this.buildForm();
    //if already logged in, logout
    if(this.auth_ser.isLogged()){
      this.auth_ser.logOut().subscribe(res => {
      },err => console.log);
    }else{
      //if navigated from registration page, get the email from it
      if(this.navParams.get('email')){
        this.loginForm.controls['email'].setValue(this.navParams.get('email'));
      }
    }
  }
  Login():void{
    this.loading_ser.showLoading();
    const email = this.loginForm.value.email;
    const password = this.loginForm.value.password;
    this.auth_ser.login(email,password).subscribe(res => {
      if(res){
        this.navCtrl.setRoot("TabsPage").catch(console.log);
      }else{
        this.loading_ser.showLoading(false);
        this.showError();
      }
    },err => {
      this.loading_ser.showLoading(false);
      if(err === 401){
        this.showError();
      }
    });
  }
  showError() {
    let alert = this.alertCtrl.create({
      title: 'Fail',
      subTitle: 'Incorrect E-mail or Password!',
      buttons: ['OK']
    });
    alert.present().catch(console.log);
  }
  showRegistration(){
    this.navCtrl.push(RegisterPage).catch(console.log);
  }
  buildForm():void{
    this.loginForm = this.fb.group({
      'email': [this.email, [Validators.required]],
      'password': [this.password, [Validators.required]],
    });
  }
}
