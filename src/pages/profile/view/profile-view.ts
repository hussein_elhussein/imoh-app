import {Component, OnInit, ViewChild} from '@angular/core';
import {
  NavController, NavParams, Events, ActionSheetController, App, Content, AlertController,
  ModalController, IonicPage
} from 'ionic-angular';
import {Clipboard} from "@ionic-native/clipboard";
import {AuthService} from "../../../providers/auth.service";
import {IProfile} from "../../../interfaces/IProfile";
import {SettingsService} from "../../../providers/settings.service";
import {ProfileService} from "../../../providers/profile.service";
import {ContentService} from "../../../providers/content.service";
import {IPost} from "../../../interfaces/IPost";
import {LoadingService} from "../../../providers/loading.service";
import {IComment} from "../../../interfaces/IComment";
import {IFutureComment} from "../../../interfaces/IFutureComment";
import {ILike} from "../../../interfaces/ILike";
import {IFutureRating} from "../../../interfaces/IFutureRating";
import {RatingService} from "../../../providers/rating.service";
import {HelpersService} from "../../../providers/helpers.service";
import {ShareObject} from "../../../components/share/share";
import {ICategory} from "../../../interfaces/ICategory";
import {Storage} from "@ionic/storage";
import {IContentType} from "../../../interfaces/IContentType";
import {OrganizationService} from "../../../providers/organization.service";
import {IContent} from "../../../interfaces/IContent";
import {IPagination} from "../../../interfaces/IPagination";
import {IRole} from "../../../interfaces/IRole";
import {IContentFilter} from "../../../interfaces/IContentFilter";
import {Observable} from "rxjs/Observable";
import {CommentService} from "../../../providers/comment.service";
import {IAuthor} from "../../../interfaces/IAuthor";
@IonicPage()
@Component({
  selector: 'page-profile-view',
  templateUrl: 'profile-view.html'
})
export class ProfileViewPage implements OnInit{
  profile:IProfile;
  @ViewChild(Content) content: Content;
  id: number;
  selfProfile:boolean;
  active_tab: IContentType;
  comment: IFutureComment;
  comments:IComment[];
  rating:IFutureRating;
  rate_text:string;
  auth:IProfile;
  shareable:ShareObject;
  profile_type: string;
  categories:ICategory[];
  types: IContentType[];
  role:IRole;
  browser: boolean;
  comment_input: boolean;
  commenting: boolean;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private clipboard:Clipboard,
    private auth_ser:AuthService,
    private profile_ser:ProfileService,
    private org_ser:OrganizationService,
    private comment_ser:CommentService,
    private settings:SettingsService,
    private events:Events,
    public actionSheetCtrl: ActionSheetController,
    private loading_ser:LoadingService,
    private ser:ContentService,
    private app:App,
    private helpers:HelpersService,
    private alertCtrl:AlertController,
    private rating_ser:RatingService,
    private modalCtrl: ModalController,
    private storage:Storage,

  ) {
    //if profile viewed from other than the profile tab:
    this.id = this.navParams.get('id');
    const pType = this.navParams.get('profile_type');
    this.profile_type = pType? pType: "profile";
    this.selfProfile = false;
    this.comment = {
      commentable_id: null,
      commentable_type: 'profile',
      comment: "",
    };
    this.comment_input = false;
    this.commenting = false;
    this.rating = {
      rateable_id: null,
      rating: 0,
      comment:'Hey'
    };
    this.rate_text = '';
    this.browser = this.helpers.platform === 'browser';
  }

  //todo: display the user posts
  ngOnInit(){
    this.loadCategories().catch(console.log);
    if(this.id){
      this.getProfile((received) => {
        if(received){
          this.comment.commentable_id = this.profile.id;
          this.rating.rateable_id = this.profile.id;
          this.auth_ser.getCurrentProfile().then(profile => {
            this.auth = profile;
            this.viewingSelfProfile();
            this.loading_ser.showLoading(false).catch(console.log);
          });
        }
      });
    }else{
      this.auth_ser.getCurrentProfile().then(profile => {
        this.profile = profile;
        this.auth = profile;
        this.loadRole()
          .then(()=>{
            this.loadTypes().catch(console.log);
          })
          .catch(console.log);
        this.getShareable();
        this.comment.commentable_id = this.profile.id;
        this.rating.rateable_id = this.profile.id;
        this.viewingSelfProfile();
      });
    }
  }
  getShareable(){
    this.shareable = {
      subject: this.profile.name,
      message: this.profile.name,
      file: null,
      url: this.settings.catch_external_url + 'profile/' + this.profile.id
    };
  }
  takeAction(id: number):void{
    //let modal = this.modalCtrl.create(TakeActionPage,{id: id});
    //modal.present();
  }
  viewingSelfProfile(){
    this.selfProfile = this.auth.id === this.profile.id;
  }
  presentActions():void{
    let sheetContent = {
      title: 'Actions',
      cssClass:'profile-action-sheet',
      buttons: [],
    };
    if(!this.selfProfile){
      sheetContent.buttons = [
        {
          text: 'Contact',
          icon: 'chatboxes',
          role: 'destructive',
          handler: () => {
            this.navCtrl.push("ConversationViewPage", {receiver:this.profile})
              .catch(console.log);
          }
        },
        {
          text: 'Report',
          icon: 'flag',
          handler: () => {
            console.log('Archive clicked');
          }
        },
      ]
    }else{
      sheetContent.buttons = [
        {
          text: 'Update profile',
          icon: 'create',
          role: 'destructive',
          handler: () => {
            this.edit().catch(console.log);
          }
        },
        {
          text: 'Change profile type',
          icon: 'create',
          role: 'destructive',
          handler: () => {
            this.changeProfileType();
          }
        },
      ]
    }
    const addButtons = [
      {
        text: 'Scan',
        icon: 'barcode',
        role: 'destructive',
        handler: () => {
          this.navCtrl.push("CodeScanPage",{qr_code: this.profile.qr_code})
            .catch(console.log);
        }
      },
      {
        text: 'Cancel',
        icon: 'close-circle',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }
    ];
    sheetContent.buttons = sheetContent.buttons.concat(addButtons);
    let actionSheet = this.actionSheetCtrl.create(sheetContent);
    actionSheet.present().catch(console.log);
  }
  displayShare
  getFullURL(url:string):string{
    return this.settings.storage_url + url;
  }
  async edit(){
    this.navCtrl.push("ContentEditAddPage",{content: this.profile})
      .catch(console.log);
  }
  copyProfileID():void{
    this.helpers.copyToClipboard(this.clipboard,this.profile.profile_id);
  }
  changeProfileType(){
    // if this profile of type "user", then allow to change, otherwise just go to edit form:
    if(this.settings.profile_type_id === this.profile.content_type_id){
      this.navCtrl.push('ProfileEditAddPage',{type_id: this.profile.content_type_id})
        .catch(console.log);
    }else{
      console.log('content edit add page');
      this.navCtrl.push('ContentEditAddPage',{content: this.profile})
        .catch(console.log);
    }
  }
  gotoService(id:number):void{
    //go to the services tabs
    this.app.getRootNav().getActiveChildNav().select(1);
    setTimeout(() => {
      this.events.publish('navigate:service',id);
    },1000);
  }
  openItem(content:IContent):void{
   this.navCtrl.push("ContentViewPage",{content: content}).catch(console.log);
  }
  getName(profile:IProfile | IAuthor = null):string{
    if(profile){
      return this.profile_ser.getName(profile);
    }else{
      return this.profile_ser.getName(this.profile);
    }
  }
  getProfile(callback:any = null):void{
    const finish = () => {
      this.getShareable();
      this.loadRole()
        .then(()=>{
          this.loadTypes().catch(console.log);
        })
        .catch(console.log);
      if(callback){
        callback(true);
      }
    };
    if(this.profile_type === 'profile'){
      this.profile_ser.getProfile(this.id).subscribe(profile => {
        this.profile = profile;
        finish();
      },err => {
        console.log(err);
        if(callback){
          callback(false);
        }
      });
    }else{
      console.log('getting organization');
      this.org_ser.getOrganization(this.id).subscribe(profile => {
        this.profile = profile;
        finish();
      },err => {
        if(callback){
          callback(false);
        }
      })
    }

  }
  postComment():void{
    this.loading_ser.showLoading();
  }
  commentIsValid():boolean{
    let comment = this.comment.comment;
    comment.trim();
    return comment.length > 0;
  }
  rate(data):void{
    this.loading_ser.showLoading();
    this.rating.rating = data.rating;
    this.rating.comment = data.comment;
    console.log(this.rating);
    this.rating_ser.rate('profile',this.rating).subscribe(res => {
      this.getProfile(() => {
        this.scroll();
        this.loading_ser.showLoading(false);
      })
    },err => {
      console.log(err);
      this.loading_ser.showLoading(false);
    })
  }
  like(is_like:boolean = true, post:IPost = null,comment:IComment = null):void{
    this.loading_ser.showLoading();
    let like = <ILike>{
      likeable_id:post?post.id: comment.id,
      likeable_type: post?'post': 'comment',
      like:is_like
    };
  }
  scroll(){
    setTimeout(() => {
      if(this.content && this.content._scroll){
        this.content.scrollToBottom(1000)
      }else{
        console.log('cant scroll');
      }
    },500)
  }
  openRatingsList(){
    if(this.profile.rating_count){
      let params = {rateable:'profile',id:this.profile.id};
      this.navCtrl.push("RatingsListPage", params).catch(console.log);
    }
  }
  async loadRole(){
    const role = await this.profile_ser.getDefaultRole(this.profile).catch(console.log);
    if(role){
      this.profile.roles = [];
      this.profile.roles.push(role);
    }
  }
  async loadCategories(){
    const cats = await this.storage.get('categories').catch(console.log);
    if(cats){
      this.categories = cats;
    }
  }
  async loadTypes(){
    //gets the content types the user/organization can have
    const all_types = <IContentType[]> await this.storage.get('content_types').catch(console.log);
    const types = [];
    //load services types:
    const service = all_types.filter(item => {
      return item.id === this.settings.service_type_id;
    })[0];

    types.push(service);
    const services_types = [];
    for(let type of all_types){
      if(type.parent_id && type.parent_id === service.id){
        const hasPermission = this.can('add',type) && this.can('browse',type);
        if(hasPermission){
          services_types.push(type);
        }
      }
    }
    if(services_types.length){
      const contents = await this.loadContents(services_types).catch(console.log);
      if(contents && contents.data){
        service.contents = contents;
      }
    }
    for(const type of all_types){
      const isValid = type.profile_tab && type.id !== service.id && type.parent_id !== service.id;
      const canAdd = this.can('add',type);
      if(isValid && canAdd){
        const _types = [type];
        const contents = <IPagination>await this.loadContents(_types).catch(console.log);
        if(contents && contents.data){
          type.contents = contents;
          types.push(type);
        }
      }
    }
    // load comments
    const comments = await this.loadComments().toPromise().catch(console.log);

    //create a comment content type:
    const comment_type = <IContentType>{
      type: "comment",
      id: 0,
      contents: comments,
    };
    types.push(comment_type);
    this.types = types;
    this.active_tab = this.types[0];
  }
  loadComments():Observable<IPagination>{
    return this.comment_ser.index(this.profile.id);
  }
  loadContents(types:IContentType[]):Promise<IPagination>{
    const ids = [];
    for(let type of types){
      ids.push(type.id);
    }
    // Check if profile has other profiles & include them to load offices:
    const prof_ids = [];
    if(types.length === 1 && types[0].id === this.settings.office_type_id){
      if(this.profile.profiles && this.profile.profiles.length){
        for(let profile of this.profile.profiles){
          prof_ids.push(profile.id);
        }
      }
    }
    prof_ids.push(this.profile.id);
    const filter = <IContentFilter>{};
    filter.contentTypes = ids;
    filter.profiles = prof_ids;
    filter.pagination = true;
    filter.minimal = false;
    return <Promise<IPagination>>this.ser.getFilteredContent(filter).toPromise();
  }
  can(ability:string,type:IContentType){
    const type_name = type.type.toLowerCase().replace(' ','_');
    const key = ability + "_" + type_name;
    if(this.profile.roles && this.profile.roles.length){
      for(let role of this.profile.roles){
        if(role.permissions && role.permissions.length){
          for(let permission of role.permissions){
            if(permission.key === key){
              return true;
            }
          }
        }
      }
    }
    return false;
  }
  getCategory(id:number):ICategory{
    if(!this.categories){return;}
    const found = this.categories.filter(item => {
      return item.id === id;
    });
    if(!found.length){return}
    return found[0]
  }
  getTabTitle(tab:IContentType){
    if(tab.type.endsWith('s')){
      return tab.type;
    }else{
      return tab.type + 's';
    }
  }
  getTabContent():any{
    if(this.active_tab.contents && this.active_tab.contents.data){
      return this.active_tab.contents.data;
    }else{
      return [];
    }
  }
  isService(){
    return this.active_tab.id === this.settings.service_type_id;
  }
  isComment():boolean{
    return this.active_tab.type === "comment";
  }
  canAddOffice(){
    return this.active_tab.id === this.settings.office_type_id && this.selfProfile;
  }
  canAddComment(){
    return this.active_tab.type === 'comment';
  }
  showCommentForm(){
    this.comment_input = true;
  }
  addComment(){
    this.doAddComment().catch(console.log);
  }
  async doAddComment(){
    //this.commenting = true;
    const res = await this.comment_ser.save(this.comment).toPromise().catch(console.log);
    if(!res){
      return false;
    }
    const comment = <IComment>{
      id: res.id,
      author:{
        id: this.auth.id,
        name: this.auth.name,
        avatar: this.auth.avatar,
      },
      content: this.comment.comment,
      created_at: new Date().getTime()
    };
    for(let type of this.types){
      if(type.type === 'comment'){
        type.contents.data.push(comment);
      }
    }
    this.commenting = false;
    this.comment.comment = "";
  }
  addOffice(){
    const type_id = this.profile.content_type_id;
    this.navCtrl.push("ProfileEditAddPage",{type_id:type_id, with_office: true})
      .catch(console.log)
  }
}
