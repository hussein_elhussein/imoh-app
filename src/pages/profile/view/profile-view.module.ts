import {NgModule} from "@angular/core";
import {IonicModule, IonicPageModule} from "ionic-angular";
import {ProfileViewPage} from "./profile-view";
import {VRatingModule} from "../../../v-rating/v-rating.module";
import {PipesModule} from "../../../pipes/pipes.module";
import {ComponentsModule} from "../../../components/components.module";

@NgModule({
  declarations:[ProfileViewPage],
  imports:[
    IonicPageModule.forChild(ProfileViewPage),
    IonicModule,
    ComponentsModule,
    VRatingModule,
    PipesModule,
  ],
})
export class ProfileViewPageModule{}