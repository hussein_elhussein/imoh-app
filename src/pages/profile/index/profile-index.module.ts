import {NgModule} from "@angular/core";
import {IonicModule, IonicPageModule} from "ionic-angular";
import {ProfileIndexPage} from "./profile-index";
import {PipesModule} from "../../../pipes/pipes.module";

@NgModule({
  declarations:[ProfileIndexPage],
  imports:[
    IonicPageModule.forChild(ProfileIndexPage),
    IonicModule,
    PipesModule
  ]
})
export class ProfileIndexPageModule{}