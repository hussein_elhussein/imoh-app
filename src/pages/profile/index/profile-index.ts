import {Component, OnInit} from '@angular/core';
import {NavController, NavParams, Events, IonicPage} from 'ionic-angular';
import {IProfile} from "../../../interfaces/IProfile";
import {SettingsService} from "../../../providers/settings.service";
import {AuthService} from "../../../providers/auth.service";
import {ProfileService} from "../../../providers/profile.service";
import {ProfileViewPage} from "../view/profile-view";
import {PaginationService} from "../../../providers/pagination.service";
import {IPagination} from "../../../interfaces/IPagination";
import {IAuthor} from "../../../interfaces/IAuthor";
import {IRole} from "../../../interfaces/IRole";

@IonicPage()
@Component({
  selector: 'page-profile-index',
  templateUrl: 'profile-index.html',
})
export class ProfileIndexPage implements OnInit{
  profiles: IProfile[];
  pagination: IPagination;
  backUpprofiles: IProfile[];
  searchInput:string = '';
  foundedprofiles:IProfile[];
  shouldShowCancel:boolean = false;
  founded:boolean  = false;
  loading: boolean;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private profile_ser:ProfileService,
    private auth_ser:AuthService,
    private settings:SettingsService,
    private events:Events,
    private pag_ser:PaginationService,
  ) {
    this.profiles = [];
    this.foundedprofiles = [];
    this.backUpprofiles = [];
    this.loading = true;
  }

  ngOnInit(){
    this.events.subscribe('navigate:profile',(id:number) => {
      this.navCtrl.popAll().then(() => {
        this.gotoProfile(id);
      },() => {
        this.gotoProfile(id);
      });
    });
    this.getItems(null,() => {
      this.loading = false;
    });
  }
  loadNext(infiniteScroll):Promise<any>{
    return new Promise((resolve,reject) => {
      let finish = ()=> {
        resolve();
        infiniteScroll.complete();
      };
      if(this.pagination.meta.last_page != this.pagination.meta.current_page){
        this.pag_ser.next(this.pagination.links.next).subscribe(res => {
          for(let item of res.data){
            this.profiles.push(item);
          }
          this.pagination = res;
          this.pagination.data = null;
          this.loadRoles()
            .then(() => {
              finish();
            })
            .catch(() => {
              finish();
            })
        },err => {
          finish();
          console.log(err);
        });
      }else{
        finish();
      }
    });
  }
  getItems(refresher?, callback?):void{
    console.log('getting items');
    let finish = () => {
      if(refresher){
        refresher.complete();
      }
      if(callback){
        callback();
      }
    };
    let initItems = (pagination:IPagination) => {
      this.profiles = pagination.data;
      this.pagination = pagination;
      this.pagination.data = null;
      this.loadRoles().then(() => {
        finish();

      }).catch((err) => {
        finish();
      });
    };
    this.profile_ser.all().subscribe(res => {
      initItems(res);
    },err => {
      console.log(err);
      finish();
    })
  }
  gotoProfile(id:number){
    this.navCtrl.push("ProfileViewPage", {id:id}).catch(console.log);
  }
  async loadRoles(){
    for(const profile of this.profiles){
      const role = await this.profile_ser.getDefaultRole(profile).catch(console.log);
      if(role){
        profile.roles = [];
        profile.roles.push(role);
      }
    }
  }
  findprofile():void{
    if (this.searchInput && this.searchInput.trim() != '') {
      this.profiles = this.profiles.filter((item) => {
        let anInput = this.searchInput.toLowerCase();
        let profileName = item.name.toLowerCase();
        console.log(item);
        let profileCountry = item.country.name.toLowerCase();
        let profileID = item.id.toString();
        return (
          profileName.indexOf(this.searchInput.toLowerCase()) > -1
          ||
          profileCountry.indexOf(this.searchInput.toLowerCase()) > -1
          ||
          profileID == this.searchInput
        );
      });
      this.founded = true;
      this.shouldShowCancel = true;
    }else{
      this.founded = false;
      this.shouldShowCancel = false;
      this.profiles = this.backUpprofiles;
    }
  }
  onCancel(){
    this.founded = false;
    this.profiles = this.backUpprofiles;
  }
  getName(profile:IProfile):string{
    return this.profile_ser.getName(profile);
  }


}
