import {Component, OnInit} from '@angular/core';
import {Events, IonicPage, NavController, NavParams} from 'ionic-angular';
import {BarcodeScanner} from "@ionic-native/barcode-scanner";
import { Vibration } from '@ionic-native/vibration';
import {HelpersService} from "../../../providers/helpers.service";
import {LoadingService} from "../../../providers/loading.service";

@IonicPage()
@Component({
  selector: 'page-code-scan',
  templateUrl: 'code-scan.html',
})
export class CodeScanPage implements OnInit{
  qr_code: string;
  error: boolean;
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private barcodeScanner: BarcodeScanner,
              private vibration: Vibration,
              private helpers:HelpersService,
              private loading_ser:LoadingService,
              private events:Events
              ) {
    this.qr_code = this.navParams.get('qr_code');
    this.error = false;
  }
  ngOnInit(){
    this.initPage().catch(console.log);
  }
  async initPage(){

  }
  scan(){
    if(this.helpers.platform !== 'browser'){
      this.barcodeScanner.scan({disableSuccessBeep: true})
        .then(res => {
          this.vibrate();
          if(res && res.text){
            console.log('scan result:', res.text);
            //find all updates that it's author qr_code matches res.text:

          }
        })
        .catch(console.log)
    }
  }
  retry(){

  }
  vibrate(){
   setTimeout(() => {
     this.vibration.vibrate(100);
   },10)
  }
}
