import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import {BarcodeScanner} from "@ionic-native/barcode-scanner";
import { Vibration } from '@ionic-native/vibration';
import {CodeScanPage} from "./code-scan";
import {QRCodeModule} from "angularx-qrcode";
@NgModule({
  declarations: [
    CodeScanPage,
  ],
  imports: [
    IonicPageModule.forChild(CodeScanPage),
    QRCodeModule
  ],
  providers:[BarcodeScanner,Vibration]
})
export class ConfirmActionPageModule {}
