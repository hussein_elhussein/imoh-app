import {Component, EventEmitter,OnInit, Output} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {Storage} from "@ionic/storage";
import {IContentType} from "../../../interfaces/IContentType";
import {IContent} from "../../../interfaces/IContent";
import {IField} from "../../../interfaces/IField";
import {StatusBar} from "@ionic-native/status-bar";
import {AuthService} from "../../../providers/auth.service";
import {LoadingService} from "../../../providers/loading.service";
import {SettingsService} from "../../../providers/settings.service";
import {ContentService} from "../../../providers/content.service";
import {NotesService} from "../../../providers/notes.service";
import {HelpersService} from "../../../providers/helpers.service";
import {LocationHelperService} from "../../../providers/location-helper.service";
import {ProfileService} from "../../../providers/profile.service";
import {type} from "os";
import {IProfile} from "../../../interfaces/IProfile";
import {ISection} from "../../../interfaces/ISection";
import {ILocation} from "../../../interfaces/ILocation";
import {Observable} from "rxjs/Observable";
import {ICategory} from "../../../interfaces/ICategory";
@IonicPage()
@Component({
  selector: 'page-profile-edit-add',
  templateUrl: 'profile-edit-add.html',
  providers: [StatusBar]
})
export class ProfileEditAddPage implements OnInit{
  currentType:IContentType;
  content: IContent;
  selected_type: IContentType;
  with_office: boolean;
  types: IContentType[];
  @Output()
  item = EventEmitter;
  type_title: string;
  loading: boolean;
  defaultFields: IField[];
  office_def_sections:ISection[];
  profile_def_sections:ISection[];
  constructor(
    public navCtrl: NavController,
    public storage: Storage,
    private ser:ContentService,
    private loading_ser:LoadingService,
    private navParams: NavParams,
    private settings:SettingsService,
    private auth_ser: AuthService,
    public notes_ser:NotesService,
    public helpers:HelpersService,
    private location_ser:LocationHelperService,
    private profile_ser:ProfileService) {
    this.content = navParams.get('content');
    this.type_title = "";
    this.with_office = this.navParams.get('with_office');
    this.loading = true;
    this.office_def_sections = [];
    this.profile_def_sections = [];
  }
  // todo: implement office add functionality
  ionViewDidLoad() {}
  ngOnInit(){
    this.initPage().catch(console.log);
  }

  ionViewWillEnter(){
    //this.status_bar.hide();
    //this.status_bar.overlaysWebView(false);
  }
  ionViewWillLeave(){
    //this.status_bar.show();
    //this.status_bar.overlaysWebView(false);
  }
  async initPage(){
    const types = await this.loadTypes().catch(console.log);
    if(!types){
      return;
    }
    //find the profile types, eg: organization, profile:
    this.types = types.filter(item => {
      return item.parent_id && item.parent_id === this.settings.profile_type_id;
    });
    if(!this.types.length){
      this.showErr();
      return
    }

    //find the type that is received as param
    const currentType = types.filter(item => {
      return item.id === this.navParams.get('type_id');
    });
    if(!currentType.length){
      console.log('main type does not exist');
      this.showErr();
      return;
    }
    this.currentType = currentType[0];


    // if add profile with office then skip selecting a type and find the office type:
    if(this.with_office){
      const categories = <ICategory[]>await this.storage.get('categories').catch(console.log);
      if(!categories || !categories.length){
        return;
      }
      this.selected_type = this.prepareOfficeType(types,categories);
      if(!this.selected_type){
        console.log('selected type might not have sections not found');
      }
    }
    this.loading = false;
  }
  prepareOfficeType(types:IContentType[],categories:ICategory[]):IContentType{
    let office_type = <IContentType> null;
    let profile_type = <IContentType> null;
    // look for 'office' and 'profile' types:
    for(let type of types){
      if(type.id === this.settings.office_type_id){
        office_type = type;
      }else if(type.id === this.settings.profile_type_id){
        profile_type = type;
      }
    }
    if(!office_type || !profile_type){
      return;
    }

    let cats_options = {
      default: categories[0].id,
      multiple: true,
      options:{}
    };
    for(let cat of categories){
      cats_options.options[cat.id] = cat.name;
    }
    const options = JSON.stringify(cats_options);
    //office fields:
    const office_def_fields = <IField[]>[
      {
        id: this.helpers.randomInt(),
        label: "Title",
        name: "title",
        type: "text",
        value: "",
        required: true,
        isDefault: true,
        order: 1,
        add: true,
      },
      {
        id: this.helpers.randomInt(),
        label: "Location",
        name: "location",
        type: "location",
        value: null,
        hidden_val: null,
        required: true,
        isDefault: true,
        order: 1,
        add: true,
      },
      {
        id: this.helpers.randomInt(),
        label: "Type of offered services",
        name: "categories",
        type: "select_dropdown",
        value: null,
        options: options,
        hidden_val: null,
        required: true,
        isDefault: true,
        order: 1,
        add: true,
      },
    ];

    //Office Section:
    const office_section = <ISection>{
      id: this.helpers.randomInt(),
      title: "Office Details",
      order: 1,
      fields: office_def_fields,
      default: true,
    };
    let last_off_order;
    for(let sec of office_type.sections){
      sec.order += 1;
      last_off_order = sec.order;
    }
    office_type.sections.push(office_section);
    const profile_section = <ISection>{
      id: this.helpers.randomInt(),
      title: "Office Manager",
      order: last_off_order +1,
      fields: this.getDefaultFields(true),
      default: true,
    };
    for(let sec of profile_type.sections){
      sec.order = profile_section.order +1;
    }
    profile_type.sections.push(profile_section);
    this.office_def_sections.push(office_section);
    this.profile_def_sections.push(profile_section);
    office_type.sections = office_type.sections.concat(profile_type.sections);
    office_type.sections = this.helpers.sortBy(office_type.sections,'order','asc');
    return office_type;
  }
  async loadTypes():Promise<IContentType[]>{
    const types = <IContentType[]> await this.storage.get('content_types').catch(console.log);
    if(!types){
      this.notes_ser.showNote('Failed to load data');
      await this.loading_ser.showLoading(false).catch(console.log);
      return;
    }
    return types;
  }
  showErr(){
    this.notes_ser.showNote("Currently you can't change your profile type");
    this.loading_ser.showLoading(false).catch(console.log);
  }
  typeChanged(type:IContentType){
    setTimeout(() => {
      if(type.sections && type.sections.length){
        let fields = this.getDefaultFields();
        const loc = <IField>{
          id: this.helpers.randomInt(),
          label: "Location",
          name: "location",
          type: "location",
          value: null,
          hidden_val: null,
          required: true,
          isDefault: true,
          order: 4,
          add: true,
        };
        fields.push(loc);
        fields = <IField[]>this.helpers.sortBy(fields,'order');
        fields.forEach(item => {
          type.sections[0].fields.unshift(item);
        });
        this.defaultFields = fields;
        this.selected_type = type;
      }
    },500)
  }
  getTypeTitle(type?:IContentType): string{
    if(type && type.type){
      return type.type.toLowerCase();
    }else if(this.selected_type && this.selected_type.type){
      return this.selected_type.type.toLowerCase()
    }else{
      return "Profile"
    }
  }
  collectFinished(content:IContent){
    (async()=>{
      let showErr = (office?: boolean,profile?:boolean) =>{
        let str = "Unable to save";
        if(office){
          this.notes_ser.showNote(str + ' office');
        }else if(profile){
          this.notes_ser.showNote(str + ' profile');
        }else{
          this.notes_ser.showNote(str);
        }
      };
      if(this.with_office){
        const offContent = await this.prepareOfficeContent(content).catch(console.log);
        if(!offContent){
          showErr(true,false);
          return false;
        }
        const res = await this.ser.saveMany(offContent).toPromise().catch(console.log);
        console.log('save result:', res);
        if(!res || res.success){
          return;
        }
        this.navCtrl.pop().catch(console.log);
      }else{
        const res = await this.prepareProfileContent(content).catch(console.log);
        if(!res){
          this.notes_ser.showNote('Unable to save');
          return false;
        }
        const save_res = await this.profile_ser.changeType(res).toPromise().catch(console.log);
        if(!save_res || !save_res.success){
          return;
        }
        this.notes_ser.showNote('Profile type changed, please wait for the approval',5000);
        console.log('profile type changed:', save_res);
      }
    })().catch(console.log);
  }
  async prepareOfficeContent(content:IContent){
    const types = await this.loadTypes().catch(console.log);
    if(!types){
      console.log('types not loaded');
      return false;
    }
    const profile_type_id = this.settings.profile_type_id;
    const office_type_id = this.settings.office_type_id;
    let profile_type = <IContentType>{};
    let office_type = <IContentType>{};
    for(let type of types){
      if(type.id === profile_type_id){
        profile_type = type;
      }else if(type.id === office_type_id){
        office_type = type;
      }
    }
    if(!office_type || !profile_type){
      return false;
    }
    //gather profile fields values
    const profile = <IProfile>{
      values: []
    };
    for(let section of profile_type.sections){
      for(let field of section.fields){
        for(let value of content.values){
          if(value.field_id === field.id){
            profile.values.push(value);
          }
        }
      }
    }
    //gather office fields values:
    const office = <IContent>{
      values: []
    };
    for(let section of office_type.sections){
      for(let field of section.fields){
        for(let value of content.values){
          if(value.field_id === field.id){
            office.values.push(value);
          }
        }
      }
    }

    //gather profile default sections fields:
    for(let def_sec of this.profile_def_sections){
     for(let field of def_sec.fields){
       for(let val of content.values){
         if(val.field_id === field.id){
           profile[field.name] = val.value;
         }
       }
     }
    }
    //gather office default sections fields:
    for(let def_sec of this.office_def_sections){
      for(let field of def_sec.fields){
        for(let val of content.values){
          if(val.field_id === field.id){
            if(field.type === 'location'){
              office.location = <ILocation>{
                coords: {
                  latitude: val.value.lat,
                  longitude: val.value.lng,
                }
              };
              console.log('value coords:', val.value);
              console.log('assigned coords:', office.location.coords);
            }else if(field.type === 'select_dropdown' && field.name === 'categories'){
              const ids = [];
              for(let _val of val.value){
                const id = {
                  id: parseInt(_val),
                };
                ids.push(id);
              }
              office[field.name] = ids;
            }else{
              office[field.name] = val.value;
            }
          }
        }
      }
    }
    let showErr = () =>{
      this.notes_ser.showNote("Unable to get location, location is required");
    };
    if(office.location){
      const lat = office.location.coords.latitude;
      const lng = office.location.coords.longitude;
      const loc = await this.getLocation(lat,lng).toPromise().catch(console.log);
      if(loc){
        office.lat = lat;
        office.lng = lng;
        office.location = loc;
        profile.location = loc;
      }else{
        showErr();
        return;
      }
    }else{
      showErr();
      return;
    }
    office.content_type_id = office_type.id;
    profile.content_type_id = profile_type.id;
    const data = [];
    data[profile_type.type] = profile;
    data[office_type.type] = office;
    data['ids'] = [profile_type.id,office_type.id];
    const final = {};
    const keys = Object.keys(data);
    for(let key of keys){
      final[key] = data[key];
    }
    return final;
  }
  async prepareProfileContent(content:IContent){
    for(let val of content.values){
      for(let field of this.defaultFields){
        if(field.id === val.field_id){
          if(field.type === 'location'){
            content['location'] = <ILocation>{
              coords: {
                latitude: val.value.lat,
                longitude: val.value.lng,
              }
            }
          }else{
            content[field.name] = val.value;
          }
        }
      }
    }
    // remove the default fields from 'values':
    content.values = content.values.filter(item => {
      let include = true;
      for(let field of this.defaultFields){
        if(field.id === item.field_id){
          include = false;
        }
      }
      return include;
    });
    if(content.location){
      const coords = content.location.coords;
      const loc = await this.getLocation(coords.latitude,coords.longitude).toPromise().catch(console.log);
      if(loc){
        content.location = loc;
      }
    }
    return content;
  }
  getLocation(lat:number,lng: number):Observable<any>{
    return new Observable<any>(observer => {
      this.location_ser.reverse(lat,lng).retry(10).subscribe((res) => {
        observer.next(res);
        observer.complete();
      },err => {
        observer.error(err);
      })
    });
  }

  getDefaultFields(credentials?: boolean):IField[]{
    let fields = <IField[]>[
      {
        label: "Name",
        name: "name",
        type: "text",
        value: "",
        required: true,
        isDefault: true,
        order: 1,
        add: true,
      },
      {
        label: "Avatar",
        name: "avatar",
        type: "image",
        value: null,
        required: true,
        isDefault: true,
        isPublic: true,
        order: 2,
        add: true,

      },
    ];
    if(credentials){
      const cred_fields = <IField[]>[
        {
          label: "E-mail",
          name: "email",
          type: "text",
          value: null,
          required: true,
          isDefault: true,
          order: 3,
          add: true,
        },
        {
          label: "Password",
          name: "password",
          type: "password",
          value: "",
          required: true,
          isDefault: true,
          order: 4,
          add: true,

        },
      ];
      fields = fields.concat(cred_fields);
    }
    //generate unique for each field:
    for(let field of fields){
      field.id = this.helpers.randomInt();
    }
    return fields
  }

}
