import {NgModule} from "@angular/core";
import {IonicModule, IonicPageModule} from "ionic-angular";
import {PipesModule} from "../../../pipes/pipes.module";
import {ProfileEditAddPage} from "./profile-edit-add";
import {ComponentsModule} from "../../../components/components.module";
@NgModule({
  declarations: [
    ProfileEditAddPage
  ],
  imports:[
    PipesModule,
    IonicPageModule.forChild(ProfileEditAddPage),
    IonicModule,
    ComponentsModule,
  ]
})
export class ChangeProfileTypeModule {}