import { Component } from '@angular/core';
import {Events, IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';
import {IPrivateMessage} from "../../interfaces/IPrivateMessage";

@IonicPage()
@Component({
  selector: 'page-attach-preview',
  templateUrl: 'attach-preview.html',
})
export class AttachPreviewPage {
  message: IPrivateMessage;
  constructor(
      public navCtrl: NavController,
      public navParams: NavParams,
      public viewCtrl: ViewController,
      private events:Events,
  )
  {
    this.message = navParams.data;
  }
  sendData():void{
    this.events.publish('preview:done',this.message);
    this.viewCtrl.dismiss(this.message).catch(console.log);
  }

}
