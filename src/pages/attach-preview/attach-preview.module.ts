import {NgModule} from "@angular/core";
import {IonicModule, IonicPageModule} from "ionic-angular";
import {AttachPreviewPage} from "./attach-preview";

@NgModule({
  declarations:[AttachPreviewPage],
  imports:[
    IonicPageModule.forChild(AttachPreviewPage),
    IonicModule
  ],
  exports:[AttachPreviewPage]
})
export class MapPageModule{}