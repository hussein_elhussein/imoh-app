import {IMState} from "./IMState";

export interface IMStatus{
  message_id?:number;
  sent?:boolean;
  received?:boolean;
  seen?:boolean;
  state?:IMState;
}
