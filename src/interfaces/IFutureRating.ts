export interface IFutureRating{
  rating:number;
  comment:string;
  rateable_id?:number;
  created_at?:string;
}