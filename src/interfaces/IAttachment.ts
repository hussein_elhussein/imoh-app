export interface IAttachment{
    blob: any;
    blob_type: string;
    blob_ext:string;
    preview?: string;
    source_data?:string;
    source_type?:number;
    downloaded?: boolean;
    path?: string;
    local_path?:string;
    generated_path?:Array<string>;
    uploaded?: boolean;
    finished?:boolean;
    progress?:number;
    duration?:number;
    duration_humanized?:string;
}