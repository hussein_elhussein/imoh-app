export interface IFieldRead{
  id:number;
  type:string;
  label:string;
  default_title?:boolean;
  default_body?:boolean;
  order:number;
  browse:boolean;
  read:boolean;
}