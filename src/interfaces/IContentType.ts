import {ICategory} from "./ICategory";
import {ISection} from "./ISection";
import {ICategoryMin} from "./ICategoryMin";
import {IPagination} from "./IPagination";

export interface IContentType{
  id:number;
  title?:string;
  type?:string;
  parent_id?: number;
  sections: ISection[];
  children:IContentType[];
  categories_min?:ICategoryMin[];
  categories?:ICategory[];
  // generated inside the app:
  contents?:IPagination
  tab?:boolean;
  menu?:boolean;
  icon?:string;
  strict_mode?: boolean;
  moderated?: boolean;
  profile_tab?: boolean;
  show_map?:boolean;
  has_updates?:boolean;
  profile_limit?:number;
  role_id?:number;
  validity_duration?: number,
  validity_unit?: string,
  membership_type_id?:number;
}