import {IAuthor} from "./IAuthor";

export interface IEvent{
  author_id:number;
  author?:IAuthor;
  title:string;
  date:string;
  body:string;
  image?:string;
  loc_lat?:string;
  loc_lng?:string;
  created_at?:string
}