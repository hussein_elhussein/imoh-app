import {IAuthor} from "./IAuthor";
import {IContent} from "./IContent";

export interface IContentUpdate{
  id?:number;
  author?:IAuthor;
  office_id?:number;
  content_id?:number;
  parent_id?:number;
  previous_id?:number;
  primary?:boolean;
  office? :IContent;
  content?:IContent;
  parent?:IContentUpdate;
  children?:IContentUpdate[];
  children_count?:number;
  actions_allowed?: boolean;
  direct?: boolean,
  status: number;
  details?: string;
  scheduled?: boolean;
  modified?: boolean;
  schedule?:number;
  lat?:number;
  lng?:number;
  created_at?:string;
  updated_at?:string;
}