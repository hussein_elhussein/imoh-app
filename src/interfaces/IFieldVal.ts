import {IFieldRead} from "./IFieldRead";

export interface IFieldVal{
  id?:number;
  field_id:number;
  value:any;
  field?:IFieldRead;
  field_min?:IFieldRead
}