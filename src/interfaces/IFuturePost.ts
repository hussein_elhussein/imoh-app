export interface IFuturePost{
  author_id:number;
  staff_post?:boolean;
  title:string;
  body:string;
  image?:string;
  category_id:number;
  is_private?:boolean

}
