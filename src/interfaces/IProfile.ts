import {IRole} from "./IRole";
import {IFieldVal} from "./IFieldVal";
import {IContent} from "./IContent";
import {ILocation} from "./ILocation";

export interface IProfile{
  id:number;
  profile_id?:string;
  qr_code?:string;
  primary:boolean;
  content_type_id?:number;
  membership_id?:number;
  country_id?:number;
  socket_id?:string;
  roles?: IRole[];
  name?:string;
  avatar?:string;
  country?:{id:number,name:string};
  values?:IFieldVal[];
  values_min?:IFieldVal[];
  services?:IContent[]
  contents?:IContent[],
  lat?: number;
  lng?: number;
  rating_count?:number;
  rating_average?:number;
  online:boolean;
  status: number;
  last_active: number;
  access_token?: string;
  location?:ILocation;
  profiles?:IProfile[];
  isOffice?:boolean;
}
