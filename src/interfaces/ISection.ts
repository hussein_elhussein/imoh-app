import {IField} from "./IField";

export interface ISection{
  id:number;
  title:string;
  order:number;
  default:boolean;
  fields:IField[];
}