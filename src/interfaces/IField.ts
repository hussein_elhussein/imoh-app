export interface IField{
  id:number;
  name:string;
  type:string;
  label:string;
  value?:any;
  hidden_val?:any;
  isDefault?: boolean;
  isPublic?: boolean;
  order:number;
  details:any;
  options?:any;
  notes:string;
  disabled?:boolean;
  required:boolean;
  browse:boolean;
  read:boolean;
  edit:boolean;
  add:boolean;
  delete:boolean;
}