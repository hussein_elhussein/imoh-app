import {IAuthor} from "./IAuthor";
import {IOrganization} from "./IOrganization";
import {ILocation} from "./ILocation";

export interface IOffice{
  id?:number;
  author_id?:number;
  author?:IAuthor;
  organization_id?:number;
  organization?:IOrganization;
  location?:ILocation;
  is_head_office?:boolean;
  phone:string;
  second_phone?:string;
  email:string;
  second_email?:string;
  loc_lat:number;
  loc_lng:number;
  location_details?:string;
  created_at?:string;
  updated_at?:string;
}