import {ILocation} from "./ILocation";

export interface ILocationAuthor{
  id:number;
  name:string;
  visible_name:string;
  avatar:string;
  loc_lat:number;
  loc_lng:number;
  location_id:number;
  location:ILocation;
}