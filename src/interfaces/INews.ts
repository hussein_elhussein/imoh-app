import {IAuthor} from "./IAuthor";

export interface INews{
  author_id:number;
  author?:IAuthor;
  image?:string;
  title:string;
  body:string;
  created_at?:string
}