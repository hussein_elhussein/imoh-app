export interface IFutureComment{
  commentable_id:number;
  commentable_type:string;
  comment:string;
}
