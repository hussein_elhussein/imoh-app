import {IAuthor} from "./IAuthor";
export interface IRating{
  author:IAuthor;
  rating:number;
  comment:string;
}