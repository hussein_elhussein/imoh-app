import {IOffice} from "./IOffice";
import {IProfile} from "./IProfile";
import {IAuthor} from "./IAuthor";
import {ICategory} from "./ICategory";
export interface IOrganization{
  id?:number;
  author_id?:number;
  author?:IAuthor;
  name:string;
  logo:string;
  about?:string;
  status?:string;
  offices?: IOffice[];
  head_office:IOffice;
  categories?:ICategory[];
  categories_ids?:Array<number>;
  members?:IAuthor[];
  created_at?:string;
  updated_at?:string;
}