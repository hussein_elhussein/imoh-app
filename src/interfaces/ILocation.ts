import {ICoords} from "./ICoords";
import {ICountry} from "./ICountry";

export interface ILocation{
  id?:number;
  country_id?:number;
  administrative_area?:string;
  country_code?:string;
  country_name?:string;
  sub_administrative_area?:string;
  thoroughfare?:string;
  postal_code?: string;
  coords?:ICoords;
  country?: ICountry;
}