import {ICategory} from "./ICategory";
import {IFieldVal} from "./IFieldVal";
import {IAuthor} from "./IAuthor";
import {IContentUpdate} from "./IContentUpdate";
import {ILocation} from "./ILocation";
import {IProfile} from "./IProfile";

export interface IContent {
  id?:number;
  profile_id?: number,
  author?:IAuthor,
  content_type_id: number,
  type?:string;
  location_id?: number;
  parent_id?: number;
  title?: string;
  body?:string,
  name?:string;
  avatar?:string;
  avatar_file?:any;
  avatar_path?:string;
  excerpt?: string;
  quantity: number;
  status?:number;
  featured?: boolean;
  categories?:ICategory[];
  values?:IFieldVal[];
  values_min?:IFieldVal[];
  location?: ILocation;
  lat: number;
  lng: number;
  created_at?:string;
  updated_at?:string;
  updates?:IContentUpdate[];
}