export interface IMembershipType{
  id:number;
  title:string;
  price: number;
  is_default: boolean;
  is_free: boolean;
  validity_duration: number;
}