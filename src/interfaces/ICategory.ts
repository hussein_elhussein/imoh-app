export interface ICategory{
  id:number;
  name?:string;
  image?:string;
  parent_id?:number;
  children?:ICategory[];
  show?:boolean;
  parent?:ICategory;
}
