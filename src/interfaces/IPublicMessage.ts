import {ICreatedAt} from "./ICreatedAt";

export interface IPublicMessage{
  sender:number;
  content:string;
  date:ICreatedAt;

}
