import {IRole} from "./IRole";

export interface IRegister{
  name:string;
  email:string;
  password:string;
  role_id:number
}
