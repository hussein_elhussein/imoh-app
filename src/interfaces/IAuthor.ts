
import {IRole} from "./IRole";
import {ICountry} from "./ICountry";
import {ILocation} from "./ILocation";

export interface IAuthor{
  id:number;
  name:string;
  avatar:string;
  content_type_id: number,
  roles?: IRole[];
  role?:IRole;
  country?:ICountry;
  loc_lat?:number;
  loc_lng?:number;
  location: ILocation
  socket_id?:string;
  last_active?:number;
  isOffice?:boolean;
  qr_code?:string;
}