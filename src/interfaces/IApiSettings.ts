export interface IApiSettings {
  title: string;
  description: string;
  logo: string;
  google_analytics_tracking_id: string;
  service_type_id: number;
  offered_service_type_id: number;
  required_service_type_id: number;
  profile_type_id: number;
  organization_type_id: number;
  office_type_id: number;
}