import {IAuthor} from "./IAuthor";
import {ILike} from "./ILike";
export interface IComment{
    id?:number;
    author?:IAuthor;
    content?:string;
    likes?:ILike[]
    dislikes?:ILike[]
    created_at?:number;
}
