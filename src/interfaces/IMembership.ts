import {IMembershipType} from "./IMembershipType";

export interface IMembership{
  membership_type_id: number;
  active: boolean;
  start_date: string;
  end_date: string;
  membership_type: IMembershipType;
}