import {ILocation} from "./ILocation";

export interface IContentFilter {
  contentTypes?:Array<number>;
  categories?:Array<number>
  profiles?:Array<number>;
  location?:ILocation;
  excludeLoggedIn?: boolean;
  minimal?:boolean;
  pagination?:boolean;
  relations?:Array<string>;

}