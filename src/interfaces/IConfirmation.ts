import {IContentUpdate} from "./IContentUpdate";
import {IAuthor} from "./IAuthor";
import {IContent} from "./IContent";

export interface IConfirmation {
  id: number;
  content_id: number;
  content_update_id: number;
  code: string;
  profiles: IAuthor[];
  content?:IContent;
  content_update:IContentUpdate;
  created_at: string;

  // this is set internally not from server:
  needy?: IAuthor;
}