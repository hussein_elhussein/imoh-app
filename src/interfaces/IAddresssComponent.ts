export interface IAddresssComponent{
  address_components: [{
    long_name: string;
    short_name: string;
    types: Array<string>;
  }];
}