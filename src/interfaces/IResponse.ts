export interface IResponse{
    success: boolean;
    error: boolean;
    errors: Array<any>;
    details?:string;
    id?:number;
    message?:string;
}
