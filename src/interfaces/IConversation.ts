import {IPrivateMessage} from "./IPrivateMessage";
import {IAuthor} from "./IAuthor";
export interface IConversation{
  id?:number;
  socket_id?:string;
  opener:IAuthor;
  receivers:IAuthor[];
  viewers:IAuthor[];
  messages?:IPrivateMessage[];
  last_message?:IPrivateMessage;
  opened?:boolean;
  initialized?:boolean;
  profiles: Array<number>;
}
