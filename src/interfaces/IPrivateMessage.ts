import {IAttachment} from "./IAttachment";
import {IMStatus} from "./IMStatus";
import {IMError} from "./IMError";
import {IAuthor} from "./IAuthor";
export interface IPrivateMessage{
  id?:number;
  inAppID?: number;
  sender:number;
  author?:IAuthor;
  text?:string;
  attachment?:IAttachment;
  error?:IMError;
  status?:IMStatus;
  conversation_id?:number;
  conversation_socket_id?:string;
  date?:any;
  created_at?:string;
}
