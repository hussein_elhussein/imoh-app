import {ILocation} from "./ILocation";

export interface ISavedRev{
  id: string;
  reverse: ILocation;
  created_at: number;
}