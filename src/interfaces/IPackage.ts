import {IAuthor} from "./IAuthor";
import {IConfirmation} from "./IConfirmation";

export interface IPackage {
  id: number;
  from: IAuthor;
  to:IAuthor;
  receiver:IAuthor;
  confirmation: IConfirmation;
  status: number;
  created_at: number;
}