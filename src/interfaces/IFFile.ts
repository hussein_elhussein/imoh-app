import {IField} from "./IField";

export interface ChangeHistory {
  blob_name:string;
  uploaded:boolean;
}
export interface IFFile{
  field:IField;
  blob:any;
  change_history: Array<ChangeHistory>;
  ext?: string;
}