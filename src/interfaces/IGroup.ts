export interface IGroup {
  key: string;
  value: Array<any>;
}