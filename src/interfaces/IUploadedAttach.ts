export interface IUploadedAttach{
    message_id: string;
    attachment_id?: string;
    progress: number;
    finished?: boolean;
}