export interface IMState{
  processing?:boolean;
  downloading?:boolean;
  uploading?:boolean;
  sending?:boolean;
}