import {IContentType} from "./IContentType";
import {IFieldVal} from "./IFieldVal";
import {IAuthor} from "./IAuthor";
import {ILocationAuthor} from "./ILocationAuthor";
import {IServiceUpdate} from "./IServiceUpdate";

export interface IService{
  id?:number;
  author_id?:number;
  service_type_id?:number;
  author?:ILocationAuthor;
  responder?:IAuthor;
  offered:boolean;
  direct_offer?:number;
  featured?:boolean;
  status?:string;
  fields_values?:IFieldVal[];
  fields_values_min?:IFieldVal[];
  updates?:IServiceUpdate[];
  service_type?:IContentType;
  created_at?:string;
  updated_at?:string;
}