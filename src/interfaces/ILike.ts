import {IAuthor} from "./IAuthor";
export interface ILike{
    author?:IAuthor;
    author_id:number;
    like?:boolean;
    likeable_id?:number;
    likeable_type?:string;
    created_at?:string;
}
