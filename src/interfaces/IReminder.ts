import {IContentUpdate} from "./IContentUpdate";

export interface IReminder {
  id: number;
  content_update:IContentUpdate
  profiles: Array<{
    id: number;
  }>;
  status: number;

}