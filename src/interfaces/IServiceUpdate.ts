import {IAuthor} from "./IAuthor";

export interface IServiceUpdate{
  id:number;
  author_id:number
  service_id:number;
  author?:IAuthor;
  update: string;
  created_at?:string;
  updated_at?:string;
}