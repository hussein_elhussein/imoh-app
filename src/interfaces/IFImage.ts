import {IField} from "./IField";
import {IFFile} from "./IFFile";

export interface IFImage extends IFFile{
  image:string;
}