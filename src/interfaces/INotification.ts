import {IProfile} from "./IProfile";

export interface INotification {
  id: string;
  type: string;
  read_at: string;
  created_at: string;
  author: IProfile;
  data: {
    id: number;
    text: string;
    original?:any;
    visible?: boolean,
  }
}