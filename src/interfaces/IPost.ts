import {IComment} from "./IComment";
import {ILike} from "./ILike";
import {IProfile} from "./IProfile";
import {ICreatedAt} from "./ICreatedAt";
import {ICategory} from "./ICategory";
import {IAuthor} from "./IAuthor";
export interface IPost{
    id?:number;
    author_id?:number;
    staff_post:any;
    title:string;
    seo_title:string;
    excerpt?:string;
    body?:string;
    image?:string;
    slug?:string;
    status?:string;
    featured?:string;
    is_private?:boolean
    created_at:string;
    updated_at:string;
    likes_count:number;
    dislikes_count:number;
    comments_count:number;
    author?:IAuthor
    top_comment?:IComment;
    comments?:IComment[];
    likes?:ILike[];
    dislikes?:ILike[];
    categories?:ICategory[];
    category_id:number;



}
