import {IPrivateMessage} from "./IPrivateMessage";

export interface IMessagePag {
  from: number;
  to: number;
  per_page: number;
  total: number;
  next: number;
  data: IPrivateMessage[]
}