import {IPermission} from "./IPermission";

export interface IRole{
  id:number;
  name:string;
  display_name?:string;
  order?:number;
  public?: boolean;
  default?: boolean;
  registered_only?: boolean;
  created_at:string;
  updated_at:string;
  permissions?:IPermission[];
}
