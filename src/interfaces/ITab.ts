import {IContentType} from "./IContentType";

export interface ITab {
  title: string;
  icon: string;
  page: string;
  name: string;
  type: IContentType;
  tabBadge: number;
  tabBadgeStyle: string;
}