import {Injectable, EventEmitter} from '@angular/core';
import {Response} from "@angular/http";
import 'rxjs/Rx';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';
import {Observable, BehaviorSubject} from "rxjs/Rx";
import {IRegister} from "../interfaces/IRegister";
import {SettingsService} from "./settings.service";
import {IResponse} from "../interfaces/IResponse";
import {IProfile} from "../interfaces/IProfile";
import { Events } from 'ionic-angular';
import {IToken} from "../interfaces/IToken";
import {HttpErrorHandlerService} from "./http-error-handler.service";
import {HttpClient} from "@angular/common/http";
@Injectable()
export class AuthService {
  public token:string;
  onLoginError:EventEmitter<number>;
  HAS_LOGGED_IN = 'hasLoggedIn';
  constructor(
    private http: HttpClient,
    public events: Events,
    private settings:SettingsService,
    public storage:Storage,
    private error_handler:HttpErrorHandlerService,
  ) {
    this.onLoginError = new EventEmitter<number>();
  }

  login(email: string, password: string):Observable<IProfile>{
    //clear previous session:
    let postData = {
      grant_type: "password",
      client_id: this.settings.client_id,
      client_secret: this.settings.client_secret,
      username: email,
      password: password,
    };
    return this.http.post<IProfile>(this.settings.guestUrl + 'login', postData)
      .map((res: IProfile) => {
        if(res && res.access_token){
          this.token = res.access_token;
          // store the profile & token
          localStorage.setItem('token', this.token);
          this.storage.set(this.HAS_LOGGED_IN, true).catch(console.log);
          this.storage.set('currentProfile', res).catch(console.log);
          this.events.publish('user:login');
          return true;
        }else{
          return false;
        }
      })
      .catch(err => {
        return this.error_handler.handleError(err,false);
      });
  }
  logOut():Observable<IResponse>{
    let obs = new Observable<IResponse>(observer => {
      let sub = this.http.post<IResponse>(this.settings.baseUrl + 'auth/logout',{})
        .catch(err => {
          return this.error_handler.handleError(err);
        });
      sub.subscribe(res => {
        this.token = '';
        this.storage.set('token',null);
        localStorage.setItem('token',null);
        this.storage.set('currentProfile',null);
        this.storage.set(this.HAS_LOGGED_IN,false);
        this.events.publish('user:logout');
        observer.next(res);
      },err => {});
    });
    return obs;
  }
  getSavedToken():Promise<any>{
    return this.storage.get('token');
  }
  getCurrentProfile():Promise<IProfile>{
   return this.storage.get('currentProfile');
  }
  isLogged():boolean{
    return !!this.getToken();
  }

  hasLoggedIn(): Promise<boolean> {
    return this.storage.get(this.HAS_LOGGED_IN).then((value) => {
      return value === true;
    });
  };
  getToken():string{
    return localStorage.getItem('token');
  }
  register(body: IRegister):Observable<IResponse>{
    return this.http.post<IResponse>(this.settings.guestUrl + 'register', body)
      .catch(err => {
        return this.error_handler.handleError(err);
      })
  }
}
