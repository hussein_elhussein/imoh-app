import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {IPagination} from "../interfaces/IPagination";
import {Observable} from "../../node_modules/rxjs";
import {SettingsService} from "./settings.service";
import {HttpErrorHandlerService} from "./http-error-handler.service";
import {IReminder} from "../interfaces/IReminder";
import {IResponse} from "../interfaces/IResponse";
@Injectable()
export class ReminderService{

  constructor(public http: HttpClient, private settings: SettingsService, private err_handler:HttpErrorHandlerService) {

  }
  getReminders():Observable<IPagination>{
    return this.http.get<IPagination>(this.settings.baseUrl + 'reminder')
      .catch(err => {
        return this.err_handler.handleError(err);
      });
  }
  update(remider:IReminder):Observable<IResponse>{
    return this.http.put<IPagination>(this.settings.baseUrl + 'reminder/' + remider.id, remider)
      .catch(err => {
        return this.err_handler.handleError(err);
      });
  }

}
