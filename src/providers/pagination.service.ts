import {Injectable} from '@angular/core';
import 'rxjs/Rx';
import 'rxjs/add/operator/map';
import {Observable} from "rxjs/Rx";
import {SettingsService} from "./settings.service";
import {HttpErrorHandlerService} from "./http-error-handler.service";
import {IPagination} from "../interfaces/IPagination";
import {HttpClient} from "@angular/common/http";
@Injectable()
export class PaginationService {
  constructor(public http: HttpClient,private err_handler:HttpErrorHandlerService) {}
  next(url:string,body?:any):Observable<IPagination>{
    if(body){
      return this.http.post<IPagination>(url,body)
        .catch(err => {
          return this.err_handler.handleError(err);
        })
    }
    return this.http.get<IPagination>(url)
      .catch(err => {
        return this.err_handler.handleError(err);
      })
  }
}
