import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import {IUnDate} from "../interfaces/IUnDate";
import {Observable} from "rxjs";

@Injectable()
export class CompareDateService {

  constructor() {}
  resolveDate(dateSent:IUnDate):any{
    let newDate = '';
    let nowObj = new Date();
    let currentDate = <IUnDate>{
      year: nowObj.getFullYear(),
      day: nowObj.getDay(),
      hour: nowObj.getHours(),
      minute:nowObj.getMinutes(),
      second:nowObj.getSeconds(),
    };
    let ignoreMonth = true;
    let ignoreHour = dateSent.hour === currentDate.hour;
    let ignoreMin = dateSent.minute === currentDate.minute;
    let ignoreSec = dateSent.second === currentDate.second;


    //compare the current year with the sent year
    if(dateSent.year){
      if (dateSent.year < currentDate.year ){
        //just use the same date
        return false;
      }

    }

    //compare month
    if(dateSent.month)
      ignoreMonth = dateSent.month < currentDate.month;

    if(!ignoreHour){
      let newHour = currentDate.hour - dateSent.hour;
      newDate = "sent " + newHour.toString();
    }
    if(ignoreHour && !ignoreMin){
      let newMin = currentDate.minute - dateSent.minute;
      if(newDate == ''){
        newDate = newMin.toString() + " minute ago";
        if(newMin > 1){
          newDate = newMin.toString() + " minutes ago";
        }
      }else{
        newDate = newDate + "and " + newMin.toString() + " ago";
      }
    }
    if(ignoreMin && ignoreHour){
      let newSec = currentDate.second - dateSent.second;
      newDate = newSec + " second ago";
      if(newSec > 1)
        newDate = newSec + " seconds ago";
    }
    if(ignoreSec && ignoreMin && ignoreHour){
      newDate = "Just now";
    }

    return newDate;
  }

  formatLocalDate():any{
  var now = new Date(),
    tzo = -now.getTimezoneOffset(),
    dif = tzo >= 0 ? '+' : '-',
    pad = function(num) {
      var norm = Math.abs(Math.floor(num));
      return (norm < 10 ? '0' : '') + norm;
    };
  return now.getFullYear()
    + '-' + pad(now.getMonth()+1)
    + '-' + pad(now.getDate())
    + 'T' + pad(now.getHours())
    + ':' + pad(now.getMinutes())
    + ':' + pad(now.getSeconds())
    + dif + pad(tzo / 60)
    + ':' + pad(tzo % 60);
}
  refresh(seconds:number,howMany:number):Observable<any>{

    var source = Observable
      .interval(seconds * 1000 /* ms */)
      .take(howMany);

    return source;
  }
}
