import { Injectable } from '@angular/core';
import {ILanguage} from "../interfaces/ILanguage";
import { Storage } from '@ionic/storage';
import {Globalization} from "@ionic-native/globalization";
@Injectable()
export class SettingsService {
   url: string = 'http://localhost';
   //url: string = 'http://192.168.1.65';
   baseUrl: string = this.url + ':80/api/';
   storage_url: string = this.url + ':80/storage/';
   authUrl:string = this.url + ":80/auth/";
   guestUrl: string = this.url + ':80/api/guest/';
   socketHost: string = this.url + ':3000';
   client_id:number = 2;
   client_secret: string = "zZY2ezKwPJLTCp0HVmj0vQ6KQmQoYLrVqEt2gDQr";
   toast_duration: number = 3000;
   onesignal_app_id: string = "b8a185c5-3b49-41d2-a805-e5bde51f038b";
   fb_project_id: string = "imoh-app-1495308259814";
   catch_external_url: string = "http://imoh.org/";
   localStorageFolder = "IMOH";
   service_type_id = null;
   profile_type_id = null;
   organization_type_id = null;
   office_type_id = null;
   required_service_type_id = null;
   offered_service_type_id = null;
   constructor(public storage:Storage, private globalization: Globalization,) {}
   setLanguage(language:ILanguage):void{
    this.storage.set('language',language);
   }
   getLanguage():Promise<{value:string}>{
      return this.globalization.getPreferredLanguage();
   }
   removeLanguage():void{
     this.storage.remove('language').catch(console.log);
   }

}
