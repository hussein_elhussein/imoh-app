import {Injectable, NgZone} from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation';
import {Observable} from "rxjs/Observable";
import {ICoords} from "../interfaces/ICoords";
import {NativeGeocoder} from "@ionic-native/native-geocoder";
import {ILocation} from "../interfaces/ILocation";
import {HttpErrorHandlerService} from "./http-error-handler.service";
import {AlertController, Events, Platform} from "ionic-angular";
import {
  CameraPosition, GoogleMap, GoogleMaps, GoogleMapsEvent, ILatLng, LatLng, LocationService, Marker,
  MarkerOptions
} from "@ionic-native/google-maps";
import {MapsAPILoader} from "@agm/core";
import { Storage } from '@ionic/storage';
import {IAddresssComponent} from "../interfaces/IAddresssComponent";
import {ISavedRev} from "../interfaces/ISavedRev";
import {observable} from "rxjs/symbol/observable";
import {HelpersService} from "./helpers.service";
import {Subject} from "rxjs/Subject";
import {IMapRes} from "../interfaces/IMapRes";
import {Diagnostic} from "@ionic-native/diagnostic";
import {LocationAccuracy} from "@ionic-native/location-accuracy";
declare var google;
@Injectable()
export class LocationHelperService{
  private last_map: IMapRes;
  public onMapClick:Subject<ILatLng>;
  public onMapDragStart:Subject<any>;
  public onMapDrag:Subject<any>;
  public onMapDragEnd:Subject<any>;
  public onMarkerDragStart:Subject<any>;
  public onMarkerDrag:Subject<any>;
  public onMarkerDragEnd:Subject<any>;
  public onPlaceSearch:Subject<any>;
  readonly platform: string;
  constructor(
    private geolocation: Geolocation,
    private nativeGeocoder: NativeGeocoder,
    private err_handler:HttpErrorHandlerService,
    private platform_ser:Platform,
    private gmap_loader: MapsAPILoader,
    private storage: Storage,
    private helpers: HelpersService,
    private diagnostic:Diagnostic,
    private location_acc:LocationAccuracy,
    private events:Events,
    public zone: NgZone) {
    this.onMapClick = new Subject<ILatLng>();
    this.onMapDragStart = new Subject<any>();
    this.onMapDrag = new Subject<any>();
    this.onMapDragEnd = new Subject<any>();
    this.onMarkerDragStart = new Subject<any>();
    this.onMarkerDrag = new Subject<any>();
    this.onMarkerDragEnd = new Subject<any>();
    this.onPlaceSearch = new Subject<any>();
    this.platform = this.helpers.platform;
  }
  private async enableLocation(){
    const gps_enabled = await this.diagnostic.isGpsLocationEnabled().catch(console.log);
    const auth = await this.diagnostic.isLocationAuthorized();
    if(!gps_enabled){
      const can_request = this.location_acc.canRequest().catch(console.log);
      if(can_request) {
        // the accuracy option will be ignored by iOS
        await this.location_acc.request(this.location_acc.REQUEST_PRIORITY_HIGH_ACCURACY).catch(console.log);
        this.events.publish('app:reload');
      }
    }
    if(!auth){
      await this.diagnostic.requestLocationAuthorization().catch(console.log);
    }
  }
  getLocation():Observable<ILocation>{
    let getPos = async() => {
      let pos_obj = null;
      await this.enableLocation().catch(console.log);
      if(this.helpers.platform === 'browser'){
        const pos = await this.geolocation.getCurrentPosition({timeout:20000,enableHighAccuracy:true}).catch(console.log);
        if(pos){
          pos_obj = {
            lat: pos.coords.latitude,
            lng: pos.coords.longitude
          }
        }
      }else{
        const pos = await LocationService.getMyLocation({enableHighAccuracy:true}).catch(console.log);
        if(pos){
          pos_obj = {
            lat: pos.latLng.lat,
            lng: pos.latLng.lng
          }
        }
      }
      return pos_obj;
    };
    return new Observable<ILocation>((observer,) => {
      getPos().then((pos) => {
        if(pos){
          this.reverse(pos.lat,pos.lng).subscribe(loc => {
            observer.next(loc);
            observer.complete();
          },(err) => {
            observer.error(err);
          });
        }else{
          observer.error("Couldn't get device position");
        }
      },err => {
        observer.error(err);
      })
    }).timeout(6000)
  }
  watch():Observable<ICoords>{
    return new Observable<ICoords>((observer,) => {
      if(this.platform === 'android'|| this.platform === 'ios'){
        let watch_obs = this.geolocation.watchPosition();
        watch_obs.subscribe((data) => {
          let coords = <ICoords>{};
          coords.longitude = data.coords.longitude;
          coords.latitude = data.coords.latitude;
          observer.next(coords);
        });
      }else{
        observer.error('platform not supported for watching position');
      }
    });
  }
  reverse(lat:number,long: number):Observable<ILocation>{
    let save_id = this.generateLocationId(lat,long);
    return new Observable<ILocation>(observer => {
      // use nativeGeocoder only on mobile, otherwise use google geocoder
      if(this.platform === "android" || this.platform === 'ios'){
        this.nativeGeocoder.reverseGeocode(lat,long).then(res => {
          let loc = this.initLocationObj(lat,long,res);
          console.log('reverse res:', res);
          observer.next(loc);
        },err => {
          observer.error(err);
        });
      }else{
        let reverseBrowser = () => {
          this.gmap_loader.load().then(() => {
            let geocoder = new google.maps.Geocoder();
            geocoder.geocode({location: {lat: lat,lng:long}},(res,status) => {
              if(status === 'OK'){
                if(res[0]){
                  let loc = this.initLocationObj(lat,long,res,true);
                  //save the reverse result to avoid quota limit:
                  this.saveReverse(loc);
                  observer.next(loc);
                }else{
                  observer.error('no results found');
                }
              }else{
                observer.error(status);
              }
            })
          });
        };
        this.getReverse(save_id).subscribe(res => {
          if(res){
            console.log('loaded from storage');
            observer.next(res.reverse);
          }else{
            reverseBrowser();
          }
        });
      }
    })
  }
  private initLocationObj(lat:number,long:number, res: any, browser: boolean = false){
    let location = <ILocation>{};
    if(browser){
      let address = <IAddresssComponent[]>res;
      for(let comp of address[0].address_components){
        switch (comp.types[0]){
          case "country":
            location.country = {
              id: null,
              name: comp.long_name,
            };
            break;
          case "administrative_area_level_1":
            location.administrative_area = comp.long_name;
            break;
          case "administrative_area_level_2":
            location.sub_administrative_area = comp.long_name;
        }
      }
      location.coords = {latitude: lat,longitude:long};
    }else{
      location = {
        country_name: res[0].countryName,
        country_code: res[0].countryCode,
        postal_code: res[0].postalCode,
        administrative_area: res[0].administrativeArea,
        sub_administrative_area: res[0].subAdministrativeArea,
        thoroughfare: res[0].thoroughfare,
        coords:{latitude: lat, longitude: long},
        country: {id: null, name: res[0].countryName}
      }
    }
    return location;
  }
  private saveReverse(location: ILocation):void{
    let lat = location.coords.latitude;
    let lng = location.coords.longitude;
    let rev_id = "reversed_" + this.generateLocationId(lat,lng);
    let reverses: ISavedRev[] = [];
    this.storage.get('reverses').then((saved_rev:ISavedRev[]) => {
      let saveRev = () => {
        reverses = saved_rev ? saved_rev : [];
        let new_rev = <ISavedRev>{
          id: rev_id,
          reverse: location,
          created_at: Date.now(),
        };
        reverses.push(new_rev);
        this.storage.set('reverses',reverses);
      };
      if(saved_rev && saved_rev.length){
        let founded = false;
        for(let rev of saved_rev){
          if(rev.id === rev_id){
            founded = true;
          }
        }
        if(!founded){
          saveRev();
        }
      }else{
        saveRev();
      }
    });
  }
  private getReverse(id: string = null):Observable<ISavedRev>{
    let validateByTime = (saved_rev:ISavedRev) => {
      let diff = this.helpers.difference(Date.now(),saved_rev.created_at);
      let years = diff._data.years;
      let days = diff._data.days;
      let hours = diff._data.hours;
      let minutes = diff._data.minutes;
      if(years < 1 && days < 1){
        return (hours < 1 && minutes === 0) || minutes < 59;
      }else{
        return false;
      }
    };
    return new Observable<ISavedRev>(observer => {
      this.storage.get('reverses').then((saved_revs: ISavedRev[]) => {
        if(saved_revs && saved_revs.length){
          let founded = saved_revs.filter(item => {
            return item.id === id;
          });
          if(founded && founded.length){
            observer.next(founded[0])
          }else{
            //get location that saved since less than hour:
            let founded = null;
            for(let saved_rev of saved_revs){
              if(validateByTime(saved_rev)){
                founded = saved_rev;
              }
            }
            observer.next(founded);
          }
        }else{
          observer.next(null);
        }
      });
    })
  }
  private toInit(num: number){
    let fracs = num.toString().split('.');
    let num_str = "";
    for(let frac of fracs){
      num_str += frac;
    }
    return parseInt(num_str);
  }
  private generateLocationId(lat:number, long:number):string{
    let _lat = this.toInit(lat).toString();
    let _lng = this.toInit(long).toString();
    return _lat + "_" + _lng;
  }
  createMap(
    element:HTMLElement,
    lat: number,
    lng: number,
    circle: boolean = true,
    marker:boolean = false,
    marker_title?: string,
    marker_draggable: boolean = true,
    zoom: number = 12):Observable<IMapRes>{
    return new Observable<IMapRes>(observer => {
      this.zone.run(() => {
        let center = {
          lat: parseFloat(lat.toString()),
          lng: parseFloat(lng.toString())
        };
        let result = {
          marker: null,
          circle: null,
          map: null,
        };
        let circle_options = {
          center: center,
          strokeColor: '#f53d3d',
          fillColor: 'rgba(245, 61, 61, 0.2)',
          radius: 1500,
        };
        let marker_options = {
          position: center,
          draggable: marker_draggable,
        };
        if(marker){
          marker_options['animation'] = "DROP";
          if(marker_title){
            marker_options['title'] = marker_title;
          }
        }
        if(this.platform === 'browser'){
          let mapOptions = {
            center: center,
            zoom: zoom,
            mapTypeId:'roadmap',
          };

          // we don't use agm api wrapper because it has issue reloading the map
          this.gmap_loader.load().then(() => {
            const map = new google.maps.Map(element ,mapOptions);
            if(circle){
              circle_options['map'] = map;
              const circleInstance = new google.maps.Circle(circle_options);
              result.circle = circleInstance;
            }
            if(marker){
              marker_options['map'] = map;
              const marker_instance = new google.maps.Marker(marker_options);
              result.marker = marker_instance;
            }
            google.maps.event.addListenerOnce(map, 'idle',() => {
              result.map = map;
              this.last_map = result;
              this.listenToMapEvents(result);
              observer.next(result);
              observer.complete();
            });
          },err => {
            console.log(err);
            observer.error('error while loading google map sdk');
          })
        }else{
          let onLoad = new Subject();
          let loaded = 0;
          onLoad.subscribe(() => {
            loaded +=1;
            if(loaded === 4){
              this.last_map = result;
              this.listenToMapEvents(result);
              observer.next(result);
              observer.complete();
            }
          });
          let mapOptions: CameraPosition<any> = {
            target: center,
            zoom: zoom,
            tilt: 30
          };
          const map = GoogleMaps.create(element);
          map.one(GoogleMapsEvent.MAP_READY).then(() => {
            result.map = map;
            onLoad.next();
            map.moveCamera(mapOptions).then(() => {
              onLoad.next();
            });
            if(circle){
              map.addCircle(circle_options).then((circleInstance) => {
                result.circle = circleInstance;
                onLoad.next();
              });
            }else{
              onLoad.next();
            }
            if(marker){
              map.addMarker(marker_options).then(markerInstance => {
                result.marker = markerInstance;
                onLoad.next();
              });
            }else{
              onLoad.next();
            }
          }).catch((err) => {
            observer.error(err);
          });
        }
      })
    })
  }
  listenToMapEvents(map_inst: IMapRes){
    if(this.platform === 'browser'){
      // click
      map_inst.map.addListener("click",  (event) =>{
        let coords = <ILatLng>{
          lat: event.latLng.lat(),
          lng: event.latLng.lng(),
        };
        this.onMapClick.next(coords);
      });
      // drag start
      map_inst.map.addListener('dragstart',(event) => {
        this.onMapDragStart.next();

      });
      // drag
      map_inst.map.addListener('drag',(event) => {
        this.onMapDrag.next();

      });
      // drag end
      map_inst.map.addListener('dragend',(event) => {
        this.onMapDragEnd.next();
      });

      // marker events:
      if(map_inst.marker){
        // drag start
        map_inst.marker.addListener('dragstart',(event) => {
          this.onMarkerDragStart.next();
        });
        // drag
        map_inst.marker.addListener('drag',(event) => {
          this.onMarkerDrag.next();
        });
        // drag end
        map_inst.marker.addListener('dragend',(event) => {
          let coords = <ILatLng>{
            lat: event.latLng.lat(),
            lng: event.latLng.lng(),
          };
          this.onMarkerDragEnd.next(coords);
        });
      }
    }else{
      // click:
      map_inst.map.on(GoogleMapsEvent.MAP_CLICK).subscribe((res:LatLng[]) => {
        let coords = <ILatLng>{
          lat: res[0].lat,
          lng: res[0].lng,
        };
        this.onMapClick.next(coords);
      });
      //drag start:
      map_inst.map.on(GoogleMapsEvent.MAP_DRAG_START).subscribe((res:LatLng) => {
        this.onMapDragStart.next(res);
      });
      // drag
      map_inst.map.on(GoogleMapsEvent.MAP_DRAG).subscribe(() => {
        this.onMapDrag.next();
      });
      // drag end
      map_inst.map.on(GoogleMapsEvent.MAP_DRAG_END).subscribe((res:LatLng) => {
        this.onMapDragEnd.next(res);
      });
      if(map_inst.marker){

        let marker = <Marker>map_inst.marker;
        // drag start
        marker.addEventListener(GoogleMapsEvent.MARKER_DRAG_START).subscribe(res => {
          this.onMarkerDragStart.next();
        });

        // drag
        marker.addEventListener(GoogleMapsEvent.MARKER_DRAG).subscribe(res => {
          this.onMarkerDrag.next();
        });
        // drag end
        marker.addEventListener(GoogleMapsEvent.MARKER_DRAG_END).subscribe(res => {
          this.onMarkerDragEnd.next();
        });
      }
    }
  }
  updateMarker(lat:number, lng: number){
    let pos = {
      lat: parseFloat(lat.toString()),
      lng: parseFloat(lng.toString()),
    };
    if(this.platform === 'browser'){
      this.last_map.marker.setPosition(pos);
      this.last_map.map.panTo(pos);
    }else{
      let map = <GoogleMap>this.last_map.map;
      let marker = <Marker> this.last_map.marker;
      let zoom = map.getCameraZoom();
      let camOptions: CameraPosition<any> = {
        target: pos,
        zoom: zoom,
        tilt: 30,
        duration: 500,
      };
      marker.setPosition(pos);
      map.animateCamera(camOptions).then(() => {
      },err => {
        console.log('error while trying to move camera:', err);
      });
    }
  }
  getMarkerPosition():ILatLng{
    if(this.platform === 'browser'){
      if(this.last_map.marker){
        let lat = this.last_map.marker.getPosition().lat();
        let lng = this.last_map.marker.getPosition().lng();
        return {
          lat: lat,
          lng: lng,
        }
      }else{
        return null;
      }
    }else{
      if(this.last_map.marker){
        let marker = <Marker>this.last_map.marker;
        return marker.getPosition();
      }
    }
  }
  removeMap(){
    if(this.last_map && this.platform !== 'browser'){
      this.last_map.map.remove()
        .catch(err => {
          console.log('error while trying to remove a map:', err);
        });
    }
  }
  placeSearch(el:HTMLInputElement,id?:number):Observable<any>{
    return new Observable<any>((observer) => {
      this.gmap_loader.load().then(() => {
        let autocomplete = new google.maps.places.Autocomplete(el, {
          types: ["address"]
        });
        autocomplete.addListener("place_changed", () => {
          //get the place result
          let place = google.maps.places.PlaceResult = autocomplete.getPlace();

          //verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }
          //set latitude, longitude and zoom
          let lat = place.geometry.location.lat();
          let lng = place.geometry.location.lng();
          let coords = <ILatLng>{
            lat: lat,
            lng: lng,
          };
          if(id){
            this.onPlaceSearch.next({id: id,coords:coords});
          }else{
            this.onPlaceSearch.next(coords);
          }
        });
        observer.next();
      },err => {
        observer.error(err);
      });
    })
  }
}