import { Injectable } from '@angular/core';
import {Observable} from "rxjs/Observable";
import {IApiSettings} from "../interfaces/IApiSettings";
import {HttpErrorHandlerService} from "./http-error-handler.service";
import {HttpClient} from "@angular/common/http";
import {SettingsService} from "./settings.service";
@Injectable()
export class ApiSettingsService {
  constructor(private http:HttpClient,
              private err_handler:HttpErrorHandlerService,
              private settings:SettingsService) {}

  getSettings():Observable<IApiSettings>{
    return this.http.get<IApiSettings>(this.settings.baseUrl + 'settings')
      .catch(err => {
        return this.err_handler.handleError(err,true,false);
      })
  }

}
