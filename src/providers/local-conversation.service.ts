import {Injectable} from "@angular/core";
import {IConversation} from "../interfaces/IConversation";
import {IPagination} from "../interfaces/IPagination";
import {IPrivateMessage} from "../interfaces/IPrivateMessage";
import {IAuthor} from "../interfaces/IAuthor";
import {HelpersService} from "./helpers.service";
import {Storage} from "@ionic/storage";
import {ConversationService} from "./conversation.service";
import {AttachmentService} from "./attachment.service";
import {SocketService} from "./socket.service";
import {Events} from "ionic-angular";
import {IProfile} from "../interfaces/IProfile";
import {updateTemplate} from "@ionic/app-scripts/dist/template";
import {IAttachment} from "../interfaces/IAttachment";

@Injectable()
export class LocalConversationService {
  constructor(private helpers:HelpersService,
              private storage:Storage,
              private conv_ser:ConversationService,
              private att_ser:AttachmentService){
  }

  getConvesations():Promise<IConversation[]>{
    return this.storage.get('conversations');
  }
  getMessagesNextPage(pagination: IPagination,messages:IPrivateMessage[]):IPagination{
    const page = Number(pagination.links.next);
    const messages_pag = this.getMessagesPage(page,pagination.meta.per_page, messages);
    pagination.links.next = (page +1).toString();
    pagination.links.prev = (page -1).toString();
    pagination.meta.current_page = page;
    pagination.data = messages_pag.data;
    return pagination;

  }
  getMessagesPage(page: number, per_page: number, messages:IPrivateMessage[]):IPagination{
    const data = this.initPagination(per_page,messages);
    let pag = <IPagination> data.pagination;
    if(data.slices.length){
      let page_content:Array<any> = data.slices[page - 1];
      if(page_content.length && page_content.length > 1){
        pag.data = this.helpers.sortBy(page_content,'id','asc');
      }else{
        pag.data = page_content;
      }
    }else{
      pag.data = [];
    }
    pag.meta.current_page = page;
    return pag;
  }
  private initPagination(per_page: number = 10, messages: IPrivateMessage[]): any{
    let sorted = messages;
    if(messages.length && messages.length > 1){
      sorted = this.helpers.sortBy(messages,"id");
    }
    const slices = this.helpers.chunk(per_page,sorted);
    let next = slices.length > 1? 2: 1;
    let pag: IPagination = {
      data: null,
      links: {
        first: '1',
        last: slices.length.toString(),
        next: next.toString(),
        prev: null,
      },
      meta:{
        current_page: 1,
        from:0,
        last_page: slices.length,
        path: "",
        per_page: per_page,
        to: 0,
        total: 0,
      }
    };
    return {slices: slices,pagination: pag};
  }
  async getConversation(id: number):Promise<IConversation>{
    let conversations = <IConversation[]>await this.storage.get('conversations').catch(console.log);
    if(conversations && conversations.length){
      for(let conv of conversations){
        if(conv.id === id){
          return conv;
        }
      }
      return;
    }
    return;
  }
  async initConversation(receivers: IAuthor[], opener: IAuthor, skip_find?: boolean):Promise<IConversation>{
    if(!skip_find){
      const _receivers = this.helpers.clone(receivers);
      _receivers.push(opener);
      const found = <IConversation> await this.findConversation(_receivers).catch(console.log);
      if(found){
        return found;
      }
    }
    let conversation = {
      opener: opener,
      receivers:[],
      viewers:[],
      messages: [],
      opened: null,
      initialized: false,
      profiles: []
    };
    for(let receiver of receivers){
      let _receiver = <IAuthor>{
        id: receiver.id,
        avatar: receiver.avatar,
        name: receiver.name,
        location: null,
        content_type_id: receiver.content_type_id,
        last_active: receiver.last_active
      };
      conversation.receivers.push(_receiver);
      conversation.profiles.push(_receiver.id)
    }
    conversation.profiles = conversation.profiles.filter(item => {
      return item !== conversation.opener.id;
    });
    conversation.profiles.push(opener.id);
    return conversation;
  }
  async findConversation(profiles:IAuthor[]):Promise<IConversation>{
    const conversations = <IConversation[]> await this.storage.get('conversations').catch(console.log);
    if(!conversations){
      return;
    }
    const inConversation = (conv:IConversation,target_profile:number) =>{
      for(let profile_id of conv.profiles){
        if(profile_id === target_profile){
          return true;
        }
      }
      return false;
    };
    if(conversations.length){
      for(let con of conversations){
        console.log(con);
        let profilesMatch = [];
        for(let profile of profiles){
          if(inConversation(con, profile.id)){
            profilesMatch.push(profile);
          }
        }
        if(profilesMatch.length === profiles.length){
          return con;
        }
      }
      return;
    }
    return;
  }
  deleteAll(){
    return this.storage.set('conversations',[]).catch(console.log);
  }
  async deleteConversation(id: number){
    const convs = await this.getConvesations().catch(console.log);
    if(!convs){
      return;
    }
    const updatedConvs = convs.filter(item => {
      return item.id !== id;
    });
    await this.storage.set('conversations',updatedConvs).catch(console.log);
    return true;
  }
  async save(conversation:IConversation):Promise<IConversation>{
    const saved_conv = <IConversation[]> await this.storage.get('conversations').catch(console.log);
    let conversations = [];
    if(saved_conv){
      conversations = saved_conv;
    }
    conversations.push(conversation);
    await this.storage.set('conversations',conversations).catch(console.log);
    return conversation;
  }
  async updateConversation(conversation: IConversation):Promise<boolean>{
    const saved_convs = <IConversation[]>await this.storage.get('conversations').catch(console.log);
    if(saved_convs && saved_convs.length){
      let conversations = saved_convs.filter(item => {
        return item.id !== conversation.id;
      });
      conversations.push(conversation);
      await this.storage.set('conversations',conversations).catch(console.log);
      return true;
    }
    return false;
  }
  async checkForNewMessages(){
    const convs = <IConversation[]> await this.conv_ser.getConversations(false).toPromise().catch(console.log);
    if(!convs){
      return;
    }
    for(const conv of convs){
      const shouldLoad = await this.shouldUpdate(conv).catch(console.log);
      if(shouldLoad){
        const messages = <IPrivateMessage[]> await this.conv_ser.getMessages(conv.id).toPromise().catch(console.log);
        if(messages){
          conv.messages = messages;
          await this.addNewMessages(conv).catch(console.log);
        }
      }
    }
  }
  private async shouldUpdate(conversation:IConversation):Promise<boolean>{
    const conv = await this.getConversation(conversation.id).catch(console.log);
    if(!conv || !conv.messages.length){
      return true;
    }
    return conv.last_message.id !== conversation.last_message.id;
  }
  private async addNewMessages(conv:IConversation){
    let saved = await this.getConversation(conv.id).catch(console.log);
    if(!saved){
      saved = await this.addNewConversation(conv).catch();
    }
    if(!saved){return}
    for(const _message of conv.messages){
      if(!this.messageExist(_message.id,saved)){
        const message = await this.addNewMessage(_message,saved,false).catch(console.log);
        if(message){
          saved.messages.push(message);
        }
      }
    }
    const last_message = saved.messages.filter(item => {
      return item.id === conv.last_message.id;
    });
    saved.last_message = last_message[0];
    await this.updateConversation(saved).catch(console.log);
    await this.setUnread(saved).catch(console.log);
  }
  async setUnread(conversation:IConversation){
    let unread = <IConversation[]>[];
    const saved_unread = <IConversation[]> await this.getUnread().catch(console.log);
    if(saved_unread){
      unread = saved_unread;
    }
    const clone = <IConversation>this.helpers.clone(conversation);
    clone.messages = conversation.messages;
    unread.push(clone);
    await this.storage.set('unread',unread).catch(console.log);
  }
  async setRead(conversation_id){
    const unread = <IConversation[]> await this.storage.get('unread').catch(console.log);
    if(!unread || !unread.length){return;}
    const updated = unread.filter(item => {
      return item.id !== conversation_id;
    });
    await this.storage.set('unread', updated).catch(console.log);
  }
  getUnread():Promise<IConversation[]>{
    return this.storage.get('unread');
  }
  async getUnreadMessagges(conversation_id: number):Promise<IPrivateMessage[]>{
    const unreads = await this.getUnread().catch(console.log);
    if(!unreads || !unreads.length) {return;}
    const unread = unreads.filter(item => {
      return item.id === conversation_id;
    });
    if(!unread.length){return}
    return unread[0].messages;
  }
  private messageExist(id: number, conversation: IConversation):boolean{
    const exist = conversation.messages.filter(item => {
      return item.id === id;
    });
    return exist.length > 0;
  }
  async addNewMessage(_message:IPrivateMessage, conv:IConversation = null, update = true){
    if(!conv){
      conv = <IConversation> await this.getConversation(_message.conversation_id).catch(console.log);
      if(!conv){
        console.log('could not add new message, no conversation found');
        return false;
      }
    }
    const text = !!_message.text;
    const att = !!_message.attachment;
    let sender = conv.receivers.filter(item => {
      return item.id === _message.sender;
    })[0];
    let message = this.initMessage(sender,conv,true,text,false,att);
    message.status.state.processing = false;
    message.id = _message.id;
    message.text = _message.text;
    message.author = sender;
    message.sender = _message.sender;
    if(_message.created_at){
      message.date = _message.created_at
    }else{
      message.date = _message.date;
    }
    if(att){
      const name = _message.attachment.path.split('/').reverse()[0];
      const ext = name.split('.').reverse()[0];
      message.attachment.path = _message.attachment.path;
      message.attachment.blob_ext = ext;
      let blob_type = null;
      if(this.att_ser.isImage(_message)){
        blob_type = "image";
      }else if(this.att_ser.isAudio(_message)){
        blob_type = 'audio';
      }else{
        blob_type = 'file';
      }
      message.attachment.blob_type = blob_type;
      const props = Object.keys(_message.attachment);
      for(let prop of props){
        message.attachment[prop] = _message.attachment[prop];
      }
    }
    if(update){
      conv.messages.push(message);
      conv.last_message = message;
      await this.updateConversation(conv).catch(console.log);
    }
    return message;
  }
  async addNewConversation(conv:IConversation){
    const initConv = await this.initConversation(conv.receivers,conv.opener,true);
    initConv.id = conv.id;
    initConv.socket_id = conv.socket_id;
    initConv.messages = conv.messages;
    initConv.last_message = conv.last_message;
    initConv.viewers = conv.viewers;
    initConv.opened = true;
    initConv.initialized = true;
    await this.save(initConv).catch(console.log);
    return conv;
  }
  initMessage(sender:IProfile | IAuthor,
              conversation:IConversation,
              isReading: boolean,
              text: boolean = true,
              processing?: boolean,
              with_att?:boolean):IPrivateMessage{
    let message_date = this.helpers.toTimeStamp(this.helpers.momentObj());
    let message : IPrivateMessage = {
      inAppID: message_date,
      sender:sender.id,
      status: {
        message_id: null,
        sent: false,
        received: false,
        seen: false,
        state:{
          sending: false,
          processing: processing,
          downloading: false,
          uploading: false,
        }
      },
      conversation_id: conversation.id,
      conversation_socket_id: conversation.socket_id,
      date: message_date,
    };
    if(text){
      message.text = "";
    }
    if(!isReading){
      message.author = {
        id: sender.id,
        name: sender.name,
        content_type_id: sender.content_type_id,
        avatar: sender.avatar,
        location: null
      }
    }
    if(with_att){
      message.attachment = {
        uploaded: false,
        duration_humanized: null,
        duration: null,
        progress: null,
        blob_type: null,
        path: null,
        blob_ext: null,
        finished: false,
        downloaded: false,
        preview: null,
        local_path: null,
        source_type: null,
        generated_path: null,
        blob: null,
        source_data: null
      }
    }
    return message;
  }
}