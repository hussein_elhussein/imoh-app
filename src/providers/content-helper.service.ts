import {Injectable} from "@angular/core";
import {IContent} from "../interfaces/IContent";
import {Storage} from "@ionic/storage";
import {HelpersService} from "./helpers.service";
import {IContentType} from "../interfaces/IContentType";

@Injectable()
export class ContentHelperService {
  constructor(private storage:Storage, private helpers:HelpersService){}
  async loadFields(content:IContent){
    const types = await this.storage.get('content_types').catch(console.log);
    if(!types){
      console.log('Failed to load content types');
      return;
    }
    const flatten = <IContentType[]>this.helpers.flattenObjects(types,'children');
    for(let type of flatten){
     for(let section of type.sections){
       for(let field of section.fields){
       }
     }
    }
  }
}