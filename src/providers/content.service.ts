import {Injectable} from '@angular/core';
import 'rxjs/Rx';
import 'rxjs/add/operator/map';
import {Observable} from "rxjs/Rx";
import {SettingsService} from "./settings.service";
import {IContentType} from "../interfaces/IContentType";
import {HttpErrorHandlerService} from "./http-error-handler.service";
import {IResponse} from "../interfaces/IResponse";
import {IPagination} from "../interfaces/IPagination";
import {ICategory} from "../interfaces/ICategory";
import {HttpClient} from "@angular/common/http";
import {IContent} from "../interfaces/IContent";
import {IContentFilter} from "../interfaces/IContentFilter";
@Injectable()
export class ContentService {
  constructor(
    public http: HttpClient,
    private settings:SettingsService,
    private err_handler:HttpErrorHandlerService,
  ) {}

  getTypes():Observable<IContentType[]>{
    return this.http.get<IContentType[]>(this.settings.baseUrl + 'content-type')
      .catch(err => {
        return this.err_handler.handleError(err);
      })
  }
  getCategories():Observable<ICategory[]>{
    return this.http.get<ICategory[]>(this.settings.baseUrl + 'service/categories')
      .catch(err => {
        return this.err_handler.handleError(err);
      })
  }
  getFilteredContent(filters:IContentFilter):Observable<IPagination|IContent[]>{
    const url = this.settings.baseUrl + 'content/filter';
    return this.http.post<IPagination | IContent[]>(url,filters)
      .catch(err => {
        return this.err_handler.handleError(err);
      })
  }
  getContent(id:number):Observable<IContent>{
    return this.http.get<IContent>(this.settings.baseUrl + 'content/' + id)
      .catch(err => {
        return this.err_handler.handleError(err);
      })
  }
  save(content:IContent):Observable<IResponse>{
    return this.http.post<IResponse>(this.settings.baseUrl + 'content',content)
      .catch(err => {
        return this.err_handler.handleError(err,true,false);
      })
  }
  saveMany(content:any):Observable<IResponse>{
    const url = this.settings.baseUrl + 'content';
    return this.http.post<IResponse>(url,content)
      .catch(err => {
        return this.err_handler.handleError(err);
      })
  }
  update(content:IContent):Observable<IResponse>{
    return this.http.put<IResponse>(this.settings.baseUrl + 'content/' + content.id, content)
      .catch(err => {
        return this.err_handler.handleError(err,true);
      })
  }
}
