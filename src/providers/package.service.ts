import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {SettingsService} from "./settings.service";
import {Observable} from "rxjs/Observable";
import {IPagination} from "../interfaces/IPagination";

@Injectable()
export class  PackageService {
  constructor(private http: HttpClient,
              private settings:SettingsService){}

  index():Observable<IPagination>{
    return this.http.get<IPagination>(this.settings.baseUrl + 'package');
  }
}