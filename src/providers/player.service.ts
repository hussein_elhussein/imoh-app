import {Injectable} from "@angular/core";
import {interval} from "rxjs/observable/interval";
import {Observable, Subject} from "../../node_modules/rxjs";
import {MediaObject, MediaService} from "./media.service";
import {IPrivateMessage} from "../interfaces/IPrivateMessage";
import {HelpersService} from "./helpers.service";

@Injectable()
export class PlayerService {
  audioFile: MediaObject;
  playing: IPrivateMessage;
  playStatus: number;
  onPercentageUpdate: Subject<number>;
  onFinish: Subject<any>;
  constructor(private media: MediaService, private helpers: HelpersService){
    this.onPercentageUpdate = new Subject<number>();
    this.onFinish = new Subject<any>();
  }
  play(message:IPrivateMessage):Observable<boolean>{
    return new Observable<boolean>((observer) => {
      let statusSub, errorSub, successSub;
      this.playStatus = 0;
      const onError = (error) => {
        const err_str = this.getErrorText(error);
        observer.error(err_str);
      };
      const onStatusUpdate = (status) => {
        this.playStatus = status;
        if(status === 2){
          this.getPlayPercentage(message).subscribe(percentage => {
            this.onPercentageUpdate.next(percentage);
          },err => {
            console.log('percentage error:');
            console.log(err);
          });
        }else if(status === 4){
          this.onFinish.next();
        }
      };
      const onSuccess = () => {
        this.audioFile.release();
        this.audioFile = null;
        statusSub.unsubscribe();
        errorSub.unsubscribe();
        errorSub.unsubscribe();
        successSub.unsubscribe();
        this.playing = null;
        observer.next(true);
        observer.complete();
      };
      this.audioFile = this.media.create(message.attachment.local_path);
      statusSub = this.audioFile.onStatusUpdate.subscribe(onStatusUpdate);
      successSub = this.audioFile.onSuccess.subscribe(onSuccess);
      errorSub = this.audioFile.onError.subscribe(onError);
      this.audioFile.play();
      this.audioFile.setVolume(1);
      this.playing = message;
    });
  }
  stop(){
    this.audioFile.stop();
  }
  private  getErrorText(error: number){
    let err_str = null;
    switch (error){
      case 0:
        err_str  = "Error while playing audio file!";
        break;
      case 1:
        err_str = "Audio playback aborted!";
        break;
      case 2:
        err_str = "Error in network!";
        break;
      case 3:
        err_str = "Error decoding audio file!";
        break;
      case 4:
        err_str = "Audio file not supported!";
        break;
      default:
        err_str = "Unknown error!";
        break;
    }
  }
  getPlayPercentage(message:IPrivateMessage):Observable<number>{
    return new Observable<number>(observer => {
      let interval = setInterval(() => {
        if(this.playStatus === 3 || this.playStatus === 4){
          observer.complete();
          clearInterval(interval);
        }else{
          this.audioFile.getCurrentPosition().then((position) => {
            if(position > 0){
              let percentage = this.helpers.percent(position,100,message.attachment.duration);
              observer.next(percentage);
            }
          }).catch(err => {
            clearInterval(interval);
            observer.error(err);
          });
        }
      },50);
    });
  }
  isPaused(message: IPrivateMessage): boolean{
    if(this.audioFile && this.playing && this.playStatus === 3){
      return message.inAppID === this.playing.inAppID;
    }
    return false;
  }
  isPlaying(message?: IPrivateMessage): boolean{
    const isset = this.audioFile && this.playing && this.playStatus !== 3;
    if(message){
      if(isset){
        return message.inAppID === this.playing.inAppID;
      }
    }else{
      return isset;
    }
  }
  resume(){
    if(this.audioFile){
      this.audioFile.play();
    }
  }
  pause(){
    if(this.audioFile){
      this.audioFile.pause();
    }
  }
  getAudioDuration(audio_path: string):Observable<number>{
    return new Observable<number>(observer => {
      const mediaFile = this.media.create(audio_path);
      mediaFile.play();
      mediaFile.setVolume(1);
      let _interval = interval(100);
      let sub = _interval.subscribe(() => {
        let duration = mediaFile.getDuration();
        if(duration !== -1){
          mediaFile.stop();
          observer.next(duration);
          observer.complete();
          sub.unsubscribe();
        }
      })
    });
  }
  msToTime(duration: number) {
    //Get hours from milliseconds
    var hours = duration / (1000*60*60);
    var absoluteHours = Math.floor(hours);
    var h = absoluteHours > 9 ? absoluteHours : '0' + absoluteHours;

    //Get remainder from hours and convert to minutes
    var minutes = (hours - absoluteHours) * 60;
    var absoluteMinutes = Math.floor(minutes);
    var m = absoluteMinutes > 9 ? absoluteMinutes : '0' +  absoluteMinutes;

    //Get remainder from minutes and convert to seconds
    var seconds = (minutes - absoluteMinutes) * 60;
    var absoluteSeconds = Math.floor(seconds);
    var s = absoluteSeconds > 9 ? absoluteSeconds : '0' + absoluteSeconds;

    return absoluteHours > 0 ? h + ':' + m + ':' + s : m + ':' + s;
  }
}