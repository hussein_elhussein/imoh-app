import {Injectable} from "@angular/core";
import {IPrivateMessage} from "../interfaces/IPrivateMessage";
import {SettingsService} from "./settings.service";
import {DirectoryEntry, File, FileEntry} from "@ionic-native/file";
import {HelpersService} from "./helpers.service";
import {IToken} from "../interfaces/IToken";
import {FileTransfer, FileTransferObject, FileUploadOptions} from "@ionic-native/file-transfer";
import {Subject} from "rxjs/Subject";
import {IMState} from "../interfaces/IMState";

@Injectable()
export class AttachmentService {
  onProgress:Subject<IPrivateMessage>;
  onFinish: Subject<IPrivateMessage>;
  onStart: Subject<IPrivateMessage>;
  onError: Subject<IPrivateMessage>;
  constructor(
    private settings:SettingsService,
    private natFile:File,
    private helpers:HelpersService,
    private transfer:FileTransfer)
  {
    this.onProgress = new Subject<IPrivateMessage>();
    this.onFinish = new Subject<IPrivateMessage>();
    this.onStart = new Subject<IPrivateMessage>();
    this.onError = new Subject<IPrivateMessage>();
  }
  download(message: IPrivateMessage){
    console.log('downloading');
    const fileTransfer: FileTransferObject = this.transfer.create();
    let path = this.generatePath(message, true, false);
    let url = this.settings.baseUrl + "file/view/" + message.attachment.path;
    let state = <IMState>{};
    state.downloading = true;
    message.status.state = state;
    this.onStart.next(message);
    let token = <IToken>JSON.parse(localStorage.getItem('token'));
    let options: FileUploadOptions = {
      headers:{
        Authorization: 'Bearer ' + token.access_token,
      }
    };
    fileTransfer.onProgress((event) => {
      const percent = this.helpers.percent(event.loaded,100,event.total);
      message.attachment.progress = percent;
      this.onProgress.next(message);
    });
    fileTransfer.download(url, path['folder'],null,options)
      .then((entry) => {
        message.attachment.local_path = entry.toURL();
        message.attachment.finished = true;
        message.status.state.downloading = false;
        message.attachment.downloaded = true;
        this.onFinish.next(message);
        console.log('finished');
      }, (err) => {
        message.attachment.finished = false;
        message.error = {
          error_code: err.code,
          error_message: err.message,
          handled: true,
          retry:false
        };
        this.onError.next(message);
      });
  }
  upload(message: IPrivateMessage) {
    console.log('uploading');
    const host_path = message.attachment.generated_path;
    const token = <IToken>JSON.parse(localStorage.getItem('token'));
    const fileTransfer: FileTransferObject = this.transfer.create();
    const options: FileUploadOptions = {
      fileKey: 'file',
      fileName: host_path['file'],
      headers:{
        Authorization: 'Bearer ' + token.access_token,
      }
    };
    const destination = this.settings.baseUrl + 'file/upload';
    const mStatus = <IMState>{};
    mStatus.uploading = true;
    message.status.state = mStatus;
    this.onStart.next(message);
    fileTransfer.onProgress((event) => {
      const percent = this.helpers.percent(event.loaded,100,event.total);
      message.attachment.progress = percent;
      this.onProgress.next(message);
    });
    fileTransfer.upload(host_path['full'], destination, options)
      .then((data) => {
        message.attachment.path = JSON.parse(data.response);
        message.attachment.finished = true;
        message.attachment.uploaded = true;
        message.status.state.uploading = false;
        this.onFinish.next(message);
        console.log('finished');
      }, (err) => {
        console.log(err);
        message.error = {
          error_code: err.code,
          error_message: err.message,
          handled: true,
          retry: false
        };
        this.onError.next(message);
      })
  }
  async copyAttachment(message:IPrivateMessage): Promise<Array<string>>{
    const futPath = this.generatePath(message, true);
    const dirEntry = <DirectoryEntry> await this.natFile.resolveLocalFilesystemUrl(futPath['folder']).catch(console.log);
    if(!dirEntry){
      return;
    }
    const file = <FileEntry> message.attachment.blob;
    const doCopy = async():Promise<any> => {
      if(message.attachment.source_type === 2) {
        return new Promise<any>((resolve,reject) => {
          file.copyTo(dirEntry,futPath['file'],resolve,reject)
        })
      }else{
        return this.natFile.writeFile(futPath['folder'],futPath['file'],message.attachment.blob)
      }
    };
    const res = await doCopy().catch(console.log);
    if(!res){
      return;
    }
    return futPath;
  }
  generatePath(message: IPrivateMessage, separate: boolean  = false, sent: boolean = true):any{
    const date = new Date();
    const baseDir = this.settings.localStorageFolder;
    let dirPath = null;
    if(this.helpers.platform === 'ios'){
      dirPath = this.natFile.documentsDirectory;
    }else{
      dirPath = this.natFile.externalRootDirectory;
    }
    if(sent){
      dirPath += baseDir + "/Sent/";
    }else{
      dirPath += baseDir + "/" + baseDir + ' ';
    }
    let ext = message.attachment.blob_ext;
    if(ext == 'jpg' || ext == 'jpeg'){
      dirPath = dirPath + "Images";
    }else if(ext == 'mp3' || ext === 'm4a' || ext === 'wav'){
      dirPath = dirPath + "Audio";
    }else{
      dirPath = dirPath + "Files";
    }
    let fileName = date.getTime() + "." + ext;
    let path = [];
    if(separate){
      path['folder'] = dirPath;
      path['file'] = fileName;
      path['full'] = dirPath + "/"+ fileName;
    }else{
      let full = dirPath + "/"+ fileName;
      path.push(full);
    }

    return path;
  }
  private uploadBrowser(message:IPrivateMessage){
    // let options = <IVOptions>{};
    // options.autoUpload = false;
    // options.maxSize = 3;
    // options.url = this.settings.baseUrl + 'file/upload';
    // options.tokenHeader = this.auth_ser.getToken();
    // options.field = message.attachment.blob_type;
    // let files = <IVFile[]>{};
    // files = [];
    // let file = <IVFile>{
    //   name: message.id.toString(),
    //   blob: message.attachment.blob
    // };
    // files.push(file);
    // message.status.state.uploading = true;
    // this.conversation.messages.push(message);
    // this.up_ser.add(files,options);
    // this.up_ser.start();
    //
    // this.up_ser.onUpload.subscribe(res => {
    //   this.updateAttachState(res);
    // });
    // this.up_ser.onUploadError.subscribe(res => {
    //   this.updateAttachState(res);
    // });

  }
  isFile(message:IPrivateMessage):boolean{
    return !this.isImage(message) && !this.isAudio(message);
  }
  isAudio(message:IPrivateMessage): boolean{
    if(message.attachment){
      const ext = message.attachment.blob_ext;
      return ext === 'mp3' || ext === 'wav' || ext === 'm4a';
    }
    return false;
  }
  isImage(message:IPrivateMessage){
    if(message.attachment){
      return message.attachment.blob_ext === 'jpeg' ||
        message.attachment.blob_ext === 'jpg' ||
        message.attachment.blob_ext === 'png' ||
        message.attachment.blob_ext === 'bmp';
    }else{
      return false;
    }
  }
}