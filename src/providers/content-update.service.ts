import {Injectable} from '@angular/core';
import 'rxjs/Rx';
import 'rxjs/add/operator/map';
import {SettingsService} from "./settings.service";
import {HttpErrorHandlerService} from "./http-error-handler.service";
import {IResponse} from "../interfaces/IResponse";
import {HttpClient} from "@angular/common/http";
import {IContentUpdate} from "../interfaces/IContentUpdate";
import {Observable} from "rxjs/Observable";
import {IContentType} from "../interfaces/IContentType";
import {IProfile} from "../interfaces/IProfile";
import {ModalController, NavController} from "ionic-angular";
import {HelpersService} from "./helpers.service";
@Injectable()
export class ContentUpdateService {
  constructor(
    public http: HttpClient,
    private settings:SettingsService,
    private err_handler:HttpErrorHandlerService,
  ) {}
  save(id: number, update:IContentUpdate):Observable<IResponse>{
    return this.http.post<IResponse>(this.settings.baseUrl + 'content_update/' + id,update)
      .catch(err => {
        return this.err_handler.handleError(err,true,false);
      })
  }
  getItem(id: number): Observable<IContentUpdate>{
    return this.http.get<IContentUpdate>(this.settings.baseUrl + 'content_update/' + id)
      .catch(err => {
        return this.err_handler.handleError(err,true,false);
      })
  }
  getMany(ids:Array<number>): Observable<IContentUpdate[]>{
    return this.http.post<IContentUpdate[]>(this.settings.baseUrl + 'content_update/many',{ids: ids})
      .catch(err => {
        return this.err_handler.handleError(err,true,false);
      })
  }
  getStatusText(item:IContentUpdate,types:IContentType[],current_profile:IProfile){
    let text;
    switch (item.status){
      case 1:
        text = "would like to";
        break;
      case 2:
        text =  "accepted";
        break;
      case 3:
        text =  "canceled";
        break;
      case 4:
        text =  "rejected";
        break;
      case 5:
        text =  "made changes to the appointment";
        break;
      case 6:
        text =  "sent";
        break;
      case 7:
        text = "organization received";
        break;
      case 8:
        text = "delivered";
        break;
      case 9:
        text = "received";
        break;
      case 10:
        text = "resolved";
        break;
      case 12:
        text = "waiting";
        break;
      case 13:
        text = "accepted";
        break;
      case 14:
        text = "refused";
        break;
      case 15:
        text = "office scheduled a meeting with you for";
        break;
      case 16:
        text = "Accepted to attend the meeting";
        break;
      case 17:
        text = "Refused to attend the meeting";
        break;
    }
    const office_stat =
      item.status === 7  ||
      item.status === 12 ||
      item.status === 13 ||
      item.status === 14 ||
      item.status === 15 ||
      item.status === 16 ||
      item.status === 17;
    const ignore =
      item.status === 4 ||
      item.status === 6 ||
      item.status === 8  ||
      item.status === 9;
    if(item.status === 1){
      if(this.isDirect(item,types)){
        text += this.isOffered(item,types)?
          " meet you for the service you offered":
          " is offering help for the service you need";
      }else{
        text += this.isOffered(item,types)?
          " claim the service you offered through an organization":
          " is offering help through an organization for the service you need";
      }
    }else if(!ignore && !office_stat){
      if(this.isContentAuthor(current_profile, item)){
        text += this.isOffered(item,types)?
          " the service you are offering":
          " the service you are requiring";
      }else{
        text += this.isOffered(item,types)?
          " the service you required":
          " your help with the service";
      }
    }
    // office status:
    if(item.office_id || item.office){
      if(item.status === 7){
        if(this.isContentAuthor(current_profile,item)){
          text = "Confirmed the package is received for";
        }else{
          text = "Confirmed the package is sent for";
        }
      }
      if(item.status === 12){
        if(this.isContentAuthor(current_profile, item)){
          text += " the office to accept receiving your package for";
        }else{
          text += " you to accept receiving the package for";
        }
      }
      if(item.status === 13){
        if(this.isContentAuthor(current_profile, item)){
          text += " to receive your package for";
        }else{
          text += " to receive the package for";
        }
      }
      if(item.status === 14){
        if(this.isContentAuthor(current_profile, item)){
          text = " to receive your package for";
        }else{
          text += " to receive the package for";
        }
      }
    }
    if(item.status === 6 || item.status === 8 || item.status === 9){
      text += " the package for";
    }
    if(item.status === 4){
      text += " for the service: " + item.content.title;
    }else{
      text += ": " + item.content.title;
    }
    return text;
  }
  isDirect(item:IContentUpdate, types:IContentType[]){
    let type = types.filter((type) => {
      return type.id === item.content.content_type_id;
    });
    for(let section of type[0].sections){
      for(let val of item.content.values){
        for(let field of section.fields){
          if(val.field_id === field.id){
            let name = field.name.replace(' ','_').toLowerCase();
            if(name.match('direct')){
              return val.value === '1'
            }
          }
        }
      }
    }
    return null;
  }
  isAuthor(current_profile:IProfile,item:IContentUpdate):boolean{
    if(item.parent){
      return current_profile.id === item.parent.author.id;
    }else{
      return current_profile.id === item.author.id;
    }
  }
  isContentAuthor(current_profile,item:IContentUpdate){
    return current_profile.id === item.content.author.id
  }
  isOffered(item:IContentUpdate,types:IContentType[]):boolean{
    for(let type of types){
      if(type.id === item.content.content_type_id){
        return type.type.toLowerCase().match('offered').length > 0;
      }
    }
  }
  isAccepted(item:IContentUpdate):boolean{
    return item.status === 2;
  }
  isResolved(item:IContentUpdate):boolean{
    return item.status === 10;
  }
}
