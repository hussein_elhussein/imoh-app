
import { Injectable } from '@angular/core';
import {SettingsService} from "./settings.service";
import {Observable} from "rxjs";
import {ICountry} from "../interfaces/ICountry";
import {Response} from "@angular/http";
import {HttpErrorHandlerService} from "./http-error-handler.service";
import {HttpClient} from "@angular/common/http";

@Injectable()
export class CountryService {
  constructor(public http:HttpClient, public settings:SettingsService,public err_ser: HttpErrorHandlerService) {}

  getCountries(): Observable<ICountry[]>{
    return this.http.get<ICountry[]>(this.settings.guestUrl + 'country')
      .catch(err => {
        return this.err_ser.handleError(err);
      });
  }
}
