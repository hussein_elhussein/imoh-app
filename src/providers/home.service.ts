import {Injectable, Inject, EventEmitter} from '@angular/core';
import {Response} from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from "rxjs";
import {IHome} from "../interfaces/IHome";
import {SettingsService} from "./settings.service";
import {HttpErrorHandlerService} from "./http-error-handler.service";
import {HttpClient} from "@angular/common/http";

@Injectable()
export class HomeService {
  onError: EventEmitter<any> = new EventEmitter();
  constructor(private http:HttpClient, private settings: SettingsService, public err_ser:HttpErrorHandlerService) {}

  getHome():Observable<IHome>{
    return this.http.get<IHome>(this.settings.baseUrl + 'home')
      .catch(err => {
        this.onError.emit(true);
        return this.err_ser.handleError(err);
      });
  }
}
