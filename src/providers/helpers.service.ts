import {Injectable} from "@angular/core";
import Moment from 'moment';
import { extendMoment } from 'moment-range';
import {Observable} from "rxjs/Observable";
import * as momentTz from 'moment-timezone';
import {Platform} from "ionic-angular";
import {IGroup} from "../interfaces/IGroup";

const moment = extendMoment(Moment);
@Injectable()
export class HelpersService{
  platform: string;
  constructor(private platform_ser: Platform){
    this.platform = this.determinePlatform();
  }
  private determinePlatform():string{
    if(this.platform_ser.is('browser')){
      return 'browser';
    }else if(this.platform_ser.is('mobileweb')){
      return 'browser';
    }else if(this.platform_ser.is('core')){
      return 'browser';
    }else if(this.platform_ser.is('android')){
      return 'android';
    }else if(this.platform_ser.is('ios')){
      return 'ios';
    }else if(this.platform_ser.is('windows')){
      return 'windows';
    }
  }
  formatLocalDate(date:string):string{
    let utc = moment.utc(date, 'YYYY-MM-DD HH:mm:ss');
    let local = utc.local();
    let newDate = local.format("LLLL");
    return newDate;
  }
  parseUTC(_date: string): string{
    let date = moment(_date);
    let dateComponent = date.utc().format('YYYY-MM-DD');
    let timeComponent = date.utc().format('HH:mm:ss');
    return dateComponent + ' ' + timeComponent;
  }
  fromNow(date:string):string{
    let utc = moment.unix(date, 'YYYY-MM-DD HH:mm:ss');
    let local = utc.local();
    let dFromNow = moment(local).fromNow();
    return dFromNow;
  }
  unix():string{
    return moment().unix();
  }
  momentObj(date?:string):any{
    if(date){
      return moment(date);
    }else{
      return moment();
    }
  }
  replaceAt(str:string, index:number, replacement:string):string {
    return str.substr(0, index) + replacement+ str.substr(index + replacement.length);
  }
  toBoolean(value):boolean{
    if(typeof value === "number"){
      return value > 0;
    }else{
      return value;
    }
  }
  difference(a: number,b: number):any{
    let _a = moment(a);
    let _b = moment(b);
    return moment.duration(_a.diff(_b))
  }
  diffinDays(date: string){
    return moment().diff(date,'days');
  }
  isBefore(date: string):boolean{
    return moment(date).isBefore();
  }
  toTimeStamp(date: any):any{
    if(moment.isMoment(date)){
      return date.unix();
    }else{
      return new Date(date.split("-").reverse().join("-")).getTime();
    }
  }
  getTimeZoneName():string{
    return momentTz.tz.guess();
  }
  toUnix(date: string):any{
    return moment(date).unix();
  }
  ionDateToUnix(date: string){
    let _date = this.normalizeIso(date);
    return this.toUnix(_date);
  }
  fromUnix(date: string):any{
    return moment.unix(date).format('YYYY-MM-DD HH:mm:ss');
  }
  fromUnixNoFormat(date: string):any{
    return moment.unix(date);
  }
  toIso(date: string):any{
    return moment.unix(date).format();
  }
  isNew(date: any):boolean{
    const moment_obj = moment.unix(date);
    const days = moment().diff(moment_obj,'days');
    return days === 0;
  }
  normalizeIso(_date: string):any{
    let parts = _date.slice(0, -1).split('T');
    let dateComponent = parts[0];
    let timeComponent = parts[1];
    return dateComponent + " " + timeComponent;
  }

  public flatten(arr, result = []){
    for (let i = 0, length = arr.length; i < length; i++) {
      const value = arr[i];
      if (Array.isArray(value)) {
        this.flatten(value, result);
      } else {
        result.push(value);
      }
    }
    return result;
  }
  public flattenObjects(arr: Array<any>, target_arr?: string){
    return arr.reduce((a, b) => {
      if(target_arr && b[target_arr]){
        let children =  b[target_arr];
        delete b[target_arr];
        let res = this.flattenObjects(children,target_arr);
        let merged = res.concat(a);
        return merged.concat(b);
      }else{
        return a.concat(b);
      }
    }, []);
  }
  unflatten(arr: Array<any>, target_prop):Array<any>{
    let findParent = (child) => {
      for(let item of arr){
        if(item && child[target_prop] === item.id){
          return item;
        }
      }
      return null;
    };
    arr.forEach((item,index,arr) => {
      if(item[target_prop]){
        let parent = findParent(item);
        if(parent){
          if(parent.children){
            parent.children.push(item);
          }else{
            parent.children = [];
            parent.children.push(item);
          }
        }
      }
    });
    return arr.filter(item => !item[target_prop]);

  }
  groupBy(value: Array<any>,field):Array<IGroup>{
    const groupedObj = value.reduce((prev, cur)=> {
      if(!prev[cur[field]]) {
        prev[cur[field]] = [cur];
      } else {
        prev[cur[field]].push(cur);
      }
      return prev;
    }, {});
    return Object.keys(groupedObj).map(key => ({ key, value: groupedObj[key] }));
  }

  percent(number: number, percent = 100, total?: number):number{
    if(total){
      let res = (number / total) * percent;
      return Number(res.toFixed(1));
    }else{
      return (number / 100) * percent;
    }
  }
  randomString():string{
    return this.randomInt().toString();
  }
  randomInt():number{
    return Math.floor(Math.random() * 1000000000);
  }
  chunk(chunkSize: number, array: Array<any>):Array<any>{
    return array.reduce(function(previous, current) {
      var chunk;
      if (previous.length === 0 ||
        previous[previous.length -1].length === chunkSize) {
        chunk = [];   // 1
        previous.push(chunk);   // 2
      }
      else {
        chunk = previous[previous.length -1];   // 3
      }
      chunk.push(current);   // 4
      return previous;   // 5
    }, []);   // 6
  }
  sortBy(arr: Array<any>, key,direction = 'desc'): Array<any>{
    return  arr.sort((a,b) => {
      if(direction === 'desc'){
        return b[key] - a[key];
      }else{
        return a[key] - b[key];
      }
    });
  }
  clone(obj:any):any{
    return JSON.parse(JSON.stringify(obj))
  }
  copyToClipboard(provider:any, text: any){
    if(this.platform === 'browser'){
      const input = document.createElement('input');
      input.setAttribute('value', text);
      document.body.appendChild(input);
      input.select();
      document.execCommand('copy');
      document.body.removeChild(input);
    }else{
      provider.copy(text.toString()).catch(console.log);
    }
  }
}
