import {Injectable} from '@angular/core';
import 'rxjs/add/operator/map';
import {Observable} from "rxjs";
import {SettingsService} from "./settings.service";
import {AuthService} from "./auth.service";
import {HttpErrorHandlerService} from "./http-error-handler.service";
import {IPagination} from "../interfaces/IPagination";
import {HttpClient} from "@angular/common/http";
import {IResponse} from "../interfaces/IResponse";
import {Events, Platform} from "ionic-angular";
import {ILocalNotification, LocalNotifications} from '@ionic-native/local-notifications';
import 'rxjs/add/operator/take';
import {HelpersService} from "./helpers.service";
import {OneSignal} from "@ionic-native/onesignal";
import {Storage} from "@ionic/storage";
import {INotification} from "../interfaces/INotification";
@Injectable()
export class NotificationService {
  public count:number;
  constructor(
    public http: HttpClient,
    public settings:SettingsService,
    public auth_ser: AuthService,
    private err_handler:HttpErrorHandlerService,
    private events:Events,
    private platform:Platform,
    private localNotifications: LocalNotifications,
    private helpers: HelpersService,
    private oneSignal: OneSignal,
    private storage:Storage
    ) {
    events.subscribe('notifications:count',(res) => {
      this.count = res;
    })
  }
  getNotifications():Observable<IPagination>{
    return this.http.get<IPagination>(this.settings.baseUrl + 'notification')
      .catch(err => {
        return this.err_handler.handleError(err);
      });
  }
  setRead(ids:Array<string>):Observable<IResponse>{
    return this.http.post<IResponse>(this.settings.baseUrl + 'notification/read',{ids: ids})
      .catch(err => {
        return this.err_handler.handleError(err);
      });
  }
  getCount():Observable<number>{
    return this.http.get<number>(this.settings.baseUrl + 'notification/count')
      .catch(err => {
        return this.err_handler.handleError(err);
      });
  }
  async setUpPush():Promise<string>{
    if(this.helpers.platform !== "browser"){
      this.oneSignal.startInit(this.settings.onesignal_app_id, this.settings.fb_project_id);

      this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.InAppAlert);

      this.oneSignal.handleNotificationReceived().subscribe((not) => {
        console.log(not);
      });
      this.oneSignal.endInit();
      const ids = await this.oneSignal.getIds().catch(console.log);
      if(ids){
        return ids.userId;
      }
    }
  }
  async getSavedCode(profile_id: any, update_id):Promise<INotification>{
    const notes = <INotification[]>await this.storage.get('notifications').catch(console.log);
    if(notes){
      let res = null;
      for(let note of notes){
        if(note.data.id === update_id && note.type === 'confirmation_created'){
          res = note;
        }
      }
      if(!res){
        res = await this.getServerSaved(update_id,"ConfirmationCreated").toPromise().catch(console.log);
      }
      return res;
    }else{
      return;
    }
  }
  async getConfirmation(parent_update_id: number){
    const confirmations = <Array<any>>await this.storage.get('confirmations').catch(console.log);
    if(!confirmations){
      return;
    }
    return confirmations.filter(item => {
      return item.update_id === parent_update_id;
    })[0];
  }
  async saveConfirmation(parent_update_id: number){
    let confirmations = <Array<any>>await this.storage.get('confirmations').catch(console.log);
    if(!confirmations){
      confirmations = [];
    }
    const conf = {
      update_id: parent_update_id,
    };
    confirmations.push(conf);
    await this.storage.set('confirmations',confirmations).catch(console.log);
    return true;
  }
  private getServerSaved(update_id: number,type: string):Observable<INotification>{
    const url = this.settings.baseUrl + 'notification/saved/' + update_id + '/' + type;
    return this.http.get<INotification>(url)
  }
  sendLocalPush(title:string,data?:any){
    if(this.helpers.platform !== "browser"){
      console.log('local notification');
      let options:ILocalNotification = {
        title: title,
      };
      if(data){
        options.data = data;
      }
      this.localNotifications.schedule(options);
    }
  }
}
