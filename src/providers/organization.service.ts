import {Injectable} from '@angular/core';
import 'rxjs/Rx';
import 'rxjs/add/operator/map';
import {Observable} from "rxjs/Rx";
import {SettingsService} from "./settings.service";
import {HttpErrorHandlerService} from "./http-error-handler.service";
import {IResponse} from "../interfaces/IResponse";
import {IOrganization} from "../interfaces/IOrganization";
import {IPagination} from "../interfaces/IPagination";
import {HttpClient} from "@angular/common/http";
import {IAuthor} from "../interfaces/IAuthor";
import {IProfile} from "../interfaces/IProfile";
import {ILocationAuthor} from "../interfaces/ILocationAuthor";
@Injectable()
export class OrganizationService {
  constructor(public http: HttpClient, private settings:SettingsService,private err_handler:HttpErrorHandlerService) {}

  all():Observable<IPagination>{
    return this.http.get<IPagination>(this.settings.baseUrl + 'organization')
      .catch(err => {
        return this.err_handler.handleError(err);
      })
  }
  myOrganizations():Observable<IPagination>{
    return this.http.get<IPagination>(this.settings.baseUrl + 'organization/my_organizations')
      .catch(err => {
        return this.err_handler.handleError(err);
      })
  }
  getOrganization(id:number):Observable<IProfile>{
    return this.http.get<IProfile>(this.settings.baseUrl + 'organization/' + id)
      .catch(err => {
        return this.err_handler.handleError(err);
      })
  }
  save(organization:IOrganization):Observable<IResponse>{
    return this.http.post<IResponse>(this.settings.baseUrl + 'organization',organization)
      .catch(err => {
        return this.err_handler.handleError(err);
      })
  }
  getName(profile:IProfile|ILocationAuthor|IAuthor):string{
    if(profile.name && profile.name.length){
      return profile.name;
    }else{
      return profile.name
    }
  }
}
