import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import {SettingsService} from "./settings.service";
import {Observable} from "rxjs";
import {HttpErrorHandlerService} from "./http-error-handler.service";
import {Storage} from "@ionic/storage";
import {IConversation} from "../interfaces/IConversation";
import {HttpClient} from "@angular/common/http";
import {IPrivateMessage} from "../interfaces/IPrivateMessage";
import {IChatRes} from "../interfaces/IChatRes";
import {IPagination} from "../interfaces/IPagination";
import {IResponse} from "../interfaces/IResponse";

@Injectable()
export class ConversationService{

  constructor(
    public http: HttpClient,
    public settings:SettingsService,
    private err_handler:HttpErrorHandlerService,
    public storage:Storage,
  ) {}
  getConversations(pag?: boolean):Observable<IPagination|IConversation[]>{
    const target = pag? "conversation/index/true": 'conversation/index';
    return this.http.get<IPagination|IConversation[]>(this.settings.baseUrl + target)
      .catch(err => {
        return this.err_handler.handleError(err);
      });
  }
  deleteConversations(ids:Array<number>):Observable<IResponse>{
    return this.http.post<IResponse>(this.settings.baseUrl + 'conversation/delete',{ids:ids})
      .catch(err => {
        return this.err_handler.handleError(err);
      });
  }
  deleteConversation(id: number):Observable<IResponse>{
    return this.http.delete<IConversation>(this.settings.baseUrl + 'conversation/' + id)
      .catch(err => {
        return this.err_handler.handleError(err);
      });
  }
  sendMessage(message:IPrivateMessage):Observable<IChatRes>{
    return this.http.post<IChatRes>(this.settings.baseUrl + 'conversation/send',message)
      .catch(err => {
        return this.err_handler.handleError(err)
      });
  }
  getMessages(conversation_id:number):Observable<IPrivateMessage[]>{
    return this.http.get<IPrivateMessage[]>(this.settings.baseUrl + 'conversation/' + conversation_id)
      .catch(err => {
        return this.err_handler.handleError(err);
      });
  }
  setRead(ids: Array<number>):Observable<IResponse>{
    return this.http.post<IResponse>(this.settings.baseUrl + 'conversation/read',{ids: ids})
      .catch(err => {
        return this.err_handler.handleError(err);
      });
  }
  save(conversation:IConversation):Observable<IConversation>{
    return this.http.post<IConversation>(this.settings.baseUrl + 'conversation', conversation)
      .catch(err => {
        return this.err_handler.handleError(err);
      });
  }
}
