import {Injectable} from '@angular/core';
import 'rxjs/Rx';
import 'rxjs/add/operator/map';
import {Observable} from "rxjs/Rx";
import {SettingsService} from "./settings.service";
import {IProfile} from "../interfaces/IProfile";
import {IResponse} from "../interfaces/IResponse";
import {ILocation} from "../interfaces/ILocation";
import {IPagination} from "../interfaces/IPagination";
import {ILocationAuthor} from "../interfaces/ILocationAuthor";
import {IAuthor} from "../interfaces/IAuthor";
import {HttpClient} from "@angular/common/http";
import {HttpErrorHandlerService} from "./http-error-handler.service";
import {IRole} from "../interfaces/IRole";
import {IContent} from "../interfaces/IContent";
import {IMembershipType} from "../interfaces/IMembershipType";
import {Storage} from "@ionic/storage";
import {IContentType} from "../interfaces/IContentType";

@Injectable()
export class ProfileService {

  constructor(public http: HttpClient,
              private settings:SettingsService,
              private err_handler:HttpErrorHandlerService,
              private storage:Storage) {

  }
  save(profile:IContent):Observable<IResponse>{
    return this.http.post<IResponse>(this.settings.baseUrl + 'profile', profile)
      .catch(err => {
        return this.err_handler.handleError(err);
      });
  }
  changeType(profile:IContent):Observable<IResponse>{
    return this.http.post<IResponse>(this.settings.baseUrl + 'profile/change_type', profile)
      .catch(err => {
        return this.err_handler.handleError(err);
      });
  }
  addMember(profile:IProfile):Observable<IResponse>{
    return this.http.post<IResponse>(this.settings.baseUrl + 'profile/member', profile)
      .catch(err => {
        return this.err_handler.handleError(err);
      });
  }
  update(profile:IProfile):Observable<IResponse>{
    return this.http.put<IResponse>(this.settings.baseUrl + 'profile/' + profile.id, profile)
      .catch(err => {
        return this.err_handler.handleError(err);
      });
  }
  updateLocation(location:ILocation):Observable<IResponse>{
    return this.http.post<IResponse>(this.settings.baseUrl + 'profile/updateLocation', location)
      .catch(err => {
        return this.err_handler.handleError(err);
      });
  }
  updateSocket(socket_id){
    return this.http.post<IResponse>(this.settings.baseUrl + 'profile/socket', {socket_id: socket_id})
      .catch(err => {
        return this.err_handler.handleError(err);
      });
  }
  updateDevice(data: any){
    return this.http.post<IResponse>(this.settings.baseUrl + 'profile/device', data)
      .catch(err => {
        return this.err_handler.handleError(err);
      });
  }
  all():Observable<IPagination>{
    return this.http.get<IPagination>(this.settings.baseUrl + 'profile')
      .catch(err => {
        return this.err_handler.handleError(err);
      });
  }
  getProfile(id: number):Observable<IProfile>{
    return this.http.get<IProfile>(this.settings.baseUrl + 'profile/' + id)
      .catch(err => {
        return this.err_handler.handleError(err);
      });
  }
  getName(profile:IProfile|ILocationAuthor|IAuthor):string{
    if(profile.name && profile.name.length){
      return profile.name;
    }else{
      return profile.name
    }
  }
  getRoles():Observable<IRole[]>{
    return this.http.get<IRole[]>(this.settings.baseUrl + 'role')
      .catch(err => {
        return this.err_handler.handleError(err);
      });
  }
  getMembershipTypes():Observable<IMembershipType[]>{
    return this.http.get<IMembershipType[]>(this.settings.baseUrl + 'membership-types')
      .catch(err => {
        return this.err_handler.handleError(err);
      });
  }
  async getDefaultRole(profile: IProfile | IAuthor):Promise<IRole>{
    const contentTypes = <IContentType[]>await this.storage.get('content_types').catch(console.log);
    if(!contentTypes || !contentTypes.length){
      return;
    }
    const roles = <IRole[]> await this.storage.get('roles').catch(console.log);
    if(!roles || !roles.length){
      return;
    }
    const target_type = contentTypes.filter(item => {
      return item.id === profile.content_type_id;
    });
    if(!target_type.length){
      return;
    }
    const role = roles.filter(item => {
      return item.id === target_type[0].role_id;
    });
    if(!role.length){
      return;
    }
    return role[0];
  }

}
