import {IField} from "../interfaces/IField";

export class FieldPermissionsService{
  constructor(){}

  canAdd(field:IField):boolean{
    return field.add;
  }
  canRead(field:IField):boolean{
    return field.read;
  }
  canBrowse(field:IField):boolean{
    return field.browse;
  }
  canEdit(field:IField):boolean{
    return field.edit;
  }
  canDelete(field:IField):boolean{
    return field.delete;
  }
}