import {Injectable} from '@angular/core';
import 'rxjs/add/operator/map';
import {Observable} from "rxjs";
import {SettingsService} from "./settings.service";
import {HttpErrorHandlerService} from "./http-error-handler.service";
import {IResponse} from "../interfaces/IResponse";
import {IOffice} from "../interfaces/IOffice";
import {HttpClient} from "@angular/common/http";
import {IPagination} from "../interfaces/IPagination";
import {Subject} from "rxjs/Subject";
import {LocationHelperService} from "./location-helper.service";
import {IService} from "../interfaces/IService";
import {IContent} from "../interfaces/IContent";
import {ContentService} from "./content.service";
import {IContentFilter} from "../interfaces/IContentFilter";
import {ILocation} from "../interfaces/ILocation";
import {ICountry} from "../interfaces/ICountry";
import {ICategory} from "../interfaces/ICategory";
@Injectable()
export class OfficeService {

  constructor(
    private http: HttpClient,
    private settings:SettingsService,
    private err_handler:HttpErrorHandlerService,
    private location_ser:LocationHelperService,
    private content_ser:ContentService)
  {

  }
  officesForService(service:IContent):Observable<IContent[]>{
    return new Observable<IContent[]>(observer => {
      let cats = null;
      if(service.categories && service.categories.length){
        cats = [];
        for(let cat of service.categories){
          cats.push(cat.id);
        }
      }
      this.nearbyOffices(cats).subscribe(offices => {
        if(offices && offices.length){
          observer.next(offices);
          observer.complete();
        }else{
          observer.complete();
        }
      }, err => {
        observer.error(err);
      })
    });
  }
  private nearbyOffices(cats?:Array<number>):Observable<IContent[]>{
    let reqSub = new Subject<IContentFilter>();
    let last_address = null;
    const filter = <IContentFilter>{};
    filter.location = <ILocation>{};
    filter.relations = [
      'location'
    ];
    if(cats && cats.length){
      filter.categories = cats;
    }
    return new Observable<IContent[]>(observer => {
      this.location_ser.getLocation().subscribe(location => {
        let getSub = reqSub.subscribe((_filter) => {
          this.getOffices(_filter).subscribe(res => {
            if(res && res.length){
              observer.next(res);
              getSub.unsubscribe();
            }else{
              console.log('last tried location: ', last_address);
              // try other locations:
              switch (last_address){
                case "sub_administrative_area":
                  last_address = "administrative_area";
                  console.log('trying location: ', last_address);
                  filter.location = <ILocation>{};
                  filter.location.administrative_area = location.administrative_area;
                  reqSub.next(filter);
                  break;
                case "administrative_area":
                  last_address = "country";
                  console.log('trying location: ', last_address);
                  filter.location = <ILocation>{};
                  filter.location.country = <ICountry>{};
                  filter.location.country.name = location.country.name;
                  reqSub.next(filter);
                  break;
                case "country":
                  console.log("we've tried all locations, giving up");
                  //we tried all locations, give up:
                  last_address = null;
                  observer.next();
                  getSub.unsubscribe();
                  break;
              }
            }
          },err => {
            observer.error(err);
          });
        });
        last_address = "sub_administrative_area";
        console.log('trying location: ', last_address);
        filter.location.sub_administrative_area = location.sub_administrative_area;
        reqSub.next(filter);
      },err => {
        observer.error(err);
      });
    })
  }
  private getOffices(filter:IContentFilter):Observable<IContent[]>{
    const officeType = [this.settings.office_type_id];
    filter.contentTypes = officeType;
    filter.minimal = true;
    filter.pagination = false;
    filter.excludeLoggedIn = true;
    return new Observable<IContent[]>(observer => {
      this.content_ser.getFilteredContent(filter).subscribe((content:IContent[]) => {
        observer.next(content);
      })
    })
  }
  private matchByCategory(a: IContent, b: IContent):boolean{
    if(!a.categories || !b.categories){
      return false;
    }
    if(!a.categories.length || !b.categories.length){
      return false;
    }
    for(let a_cat of a.categories){
      for(let b_cat of b.categories){
        if(a_cat.id === b_cat.id){
          return true;
        }
      }
    }
    return false;
  }
}
