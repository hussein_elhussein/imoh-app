import {EventEmitter, Injectable} from '@angular/core';
import 'rxjs/add/operator/map';
import {Loading, LoadingController} from "ionic-angular";
import {catchError} from "rxjs/operators";

@Injectable()
export class LoadingService {
  private text: string;
  private loading:Loading;
  onDone:EventEmitter<any>;
  constructor(private loadingCtrl: LoadingController) {
    this.onDone = new EventEmitter<any>();
    this.text = 'Please wait...';
  }

  async showLoading(show:boolean = true,text:string = this.text) {
    if(show){
      if(this.loading){
        await this.loading.dismiss().catch(console.log);
      }
      this.loading = this.loadingCtrl.create({
        content: text,
      });
      this.loading.present().catch(console.log);
    }else if(this.loading){
      await this.loading.dismiss().catch(console.log)
      this.onDone.emit(true);
    }
  }
}
