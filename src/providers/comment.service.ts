import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {HttpErrorHandlerService} from "./http-error-handler.service";
import {SettingsService} from "./settings.service";
import {IFutureComment} from "../interfaces/IFutureComment";
import {Observable} from "rxjs/Observable";
import {IComment} from "../interfaces/IComment";
import {IPagination} from "../interfaces/IPagination";
import {IResponse} from "../interfaces/IResponse";

@Injectable()
export class CommentService{
  constructor(private http:HttpClient, private err_handler:HttpErrorHandlerService,private settings:SettingsService){}

  save(comment:IFutureComment):Observable<IResponse>{
    return this.http.post<IResponse>(this.settings.baseUrl + 'comment',comment)
      .catch(err => {
        return this.err_handler.handleError(err,true,true);
      });
  }
  index(profile_id):Observable<IPagination>{
    return this.http.get<IPagination>(this.settings.baseUrl + 'comment/profile/' + profile_id)
      .catch(err => {
        return this.err_handler.handleError(err,true,true);
      });
  }
}