import {Injectable} from "@angular/core";
import 'rxjs/add/operator/map';
import {Observable} from "rxjs";
import {SettingsService} from "./settings.service";
import {HttpErrorHandlerService} from "./http-error-handler.service";
import {IFutureRating} from "../interfaces/IFutureRating";
import {IRating} from "../interfaces/IRating";
import {IPagination} from "../interfaces/IPagination";
import {HttpClient} from "@angular/common/http";
@Injectable()
export class RatingService{
  constructor(private settings:SettingsService, private http:HttpClient, private err_handler:HttpErrorHandlerService) {
  }
  rate(rateable:string,rate_obj:IFutureRating):Observable<any>{
    return this.http.post<any>(this.settings.baseUrl + 'rating/' + rateable, rate_obj)
      .catch(err => {
        return this.err_handler.handleError(err,true,false);
      });
  }
  getRatings(rateable:string,id:number):Observable<IPagination>{
    return this.http.get<IPagination>(this.settings.baseUrl + 'rating/' + rateable + '/' + id)
      .catch(err => {
        return this.err_handler.handleError(err,true,false);
      });
  }

}