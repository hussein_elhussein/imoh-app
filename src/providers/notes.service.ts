import {EventEmitter, Injectable} from '@angular/core';
import {SettingsService} from "./settings.service";
import {Toast, ToastController} from "ionic-angular";
import {Subject} from "rxjs/Subject";
@Injectable()
export class NotesService {
  public onDismiss: EventEmitter<any>;
  private displaying_note:boolean;
  private notes_list: Array<any>;
  private display_next: Subject<any>;
  private last_message: string;
  private last_displayed: number;
  private listening_to_next: boolean;
  constructor(private settings: SettingsService, private toastCtrl: ToastController)
  {
    this.onDismiss = new EventEmitter<any>();
    this.displaying_note = false;
    this.notes_list = [];
    this.display_next = new Subject();
    this.listening_to_next = false;
    this.last_message = '';
    this.last_displayed = 0;
  }
  showNote(message:string,duration:number = null){
    console.log('showNote called');
    let isAlreadyDisplayed = () => {
      return message === this.last_message && (Date.now() - this.last_displayed) < 4000;
    };
    if(!isAlreadyDisplayed()){
      this.notes_list.push({message:message,duration:duration});
    }
    if(!this.listening_to_next){
      this.listening_to_next = true;
      let sub = this.display_next.subscribe(() => {
        if(this.notes_list.length){
          let note = this.notes_list[0];
          this.display(note.message,note.duration);
          this.notes_list.splice(0, 1);
        }else{
          this.listening_to_next = false;
          sub.unsubscribe();
        }
      });
      //trigger loop:
      console.log('display called');
      this.display_next.next();
    }else {
      console.log('display loop not triggered');
    }
  }
  private display(text: string, duration:number){
    if(!this.displaying_note){
      console.log('displaying');
      let toast = this.toastCtrl.create({
        message: text,
        duration: duration ? duration : this.settings.toast_duration
      });
      toast.onDidDismiss(() => {
        this.displaying_note = false;
        this.display_next.next();
        this.onDismiss.emit(true);
      });
      toast.present();
      this.displaying_note = true;
      this.last_message = text;
      this.last_displayed = Date.now();
    }else{
      console.log('not displaying');
    }
  }
}
