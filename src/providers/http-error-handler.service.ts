
import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {Response} from "@angular/http";
import {AlertController, Events} from "ionic-angular";
import {SettingsService} from "./settings.service";
import {NotesService} from "./notes.service";
import {isUndefined} from "ionic-angular/util/util";
import {IResponse} from "../interfaces/IResponse";
import {HttpErrorResponse} from "@angular/common/http";
import {ErrorObservable} from "rxjs/observable/ErrorObservable";
@Injectable()
export class HttpErrorHandlerService {
  private is_displaying_note: boolean;
  constructor(public events: Events, private note_ser:NotesService, private alertCtrl:AlertController ) {
    this.is_displaying_note = false;
  }
  handleError (error: HttpErrorResponse | any, showNote:boolean = true,showAlert: boolean = true){
    let that = this;
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    let err_code: number;
    let err_codes = [
      0,
      401,
      403,
      404,
      422,
      500,
    ];
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      //console.info('error object is not http response object');
      errMsg = error.message ? error.message : error.toString();
    }
    if(typeof error.status == 'number'){
      for (let code of err_codes){
        if(error.status === code){
          err_code = code;
        }
      }
    }else if(typeof error == 'string'){
      for (let code of err_codes){
        if(error.indexOf(code.toString())){
          err_code = code;
        }
      }
    }else{
     console.log('cannot resolve error code');
     console.log(error);
    }
    if(err_code === 401){
      let authError = (text) => {
        this.is_displaying_note = true;
        this.note_ser.showNote(text);
        setTimeout(() => {
          this.is_displaying_note = false;
        },1000);
        this.note_ser.onDismiss.subscribe(()=>{
          console.log('navigating from error handler');
          this.events.publish("user:logout");
          this.events.publish('goToLogin');
          this.events.publish('error:401');
        });
      };
      if(error._body != undefined && error.statusText != undefined){
        if(showNote && !this.is_displaying_note){
          const error_body_obj = <IResponse>JSON.parse(error._body);
          if(error.statusText == 'Unauthorized' || error.statusText === 'Unauthenticated'){
            authError(error_body_obj.details);
          }else{
            console.log('could not detect statusText');
            console.log(error);
          }
        }else{
          console.log('cannot navigate or show note');
          console.log('displaying note: ' + this.is_displaying_note);
          console.log('show note: ' + showNote);
        }
      }else{
        authError('Sorry, You need to login again!');
      }
    }else if(err_code === 422){
      let showErr = (text) => {
        this.is_displaying_note = true;
        that.note_ser.showNote(text);
        setTimeout(() => {
          this.is_displaying_note = false;
        },1000);
      };
      if(showNote && !this.is_displaying_note){
        if(error.error !== undefined && error.error.errors !== undefined){
          let props =  Object.keys(error.error.errors);
          for(let prop of props){
            let err_list = error.error.errors[prop];
            if(Array.isArray(err_list)){
              let sub_list_items = Object.keys(err_list);
              for(let sub_err of sub_list_items){
                let text = prop + ": " + err_list[sub_err];
                showErr(text);
              }
            }else{
              showErr(err_list);
            }
          }
        }else if(error.error.details !== undefined){
          showErr(error.error.details);
        }
      }
    }else if(err_code === 403){
      if(showNote && !this.is_displaying_note){
        //todo: try catch parse
        const error_body_obj = JSON.parse(error._body);
        this.is_displaying_note = true;
        this.note_ser.showNote(error_body_obj);
        setTimeout(() => {
          this.is_displaying_note = false;
        },1000);
      }
    }else if(err_code === 404){
      this.events.publish('error:404');
      if(showAlert){
        this.presentAlert('Resource not found',false,true);
      }
    }else if(err_code === 500){
      this.events.publish('error:500');
      if(showAlert){
        this.presentAlert(errMsg.substr(1,100) + '..',false,false);
      }
    }else if(err_code === 0){
      this.events.publish('error:0');
      if(showAlert){
        this.presentAlert('Please make sure you are connected to the internet.',true,false);
      }
    }else{
      console.log(err_code);
      if(showAlert){
        this.presentAlert('Something went wrong, please contact administrator, further more: <br>' + error);
      }
    }
    if(err_code || err_code !== 0){
      return new ErrorObservable(err_code)
    }else{
      return new ErrorObservable(errMsg)
    }
  }

  private presentAlert(text:string, reload_btn:boolean = false, refresh_btn:boolean = false,) {
    let btns = [];
    if(reload_btn){
      btns.push({
        text: 'Reload app',
        role: null,
        handler: () => {
          console.log('Reload app clicked');
          this.events.publish('app:reload');
        }
      });
    }
    if(refresh_btn){
      btns.push({
        text: 'Refresh',
        role: null,
        handler: () => {
          console.log('Refresh clicked');
          this.events.publish('page:refresh');
        }
      });
    }
    btns.push({
      text: 'Cancel',
      role: 'cancel',
      handler: () => {
        console.log('Cancel clicked');
      }
    });
    let alert = this.alertCtrl.create({
      title: 'Error',
      subTitle: text,
      buttons: btns
    });
    alert.present();
  }
}
