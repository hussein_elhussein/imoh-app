import {Injectable} from '@angular/core';
import 'rxjs/Rx';
import 'rxjs/add/operator/map';
import {Observable} from "rxjs/Rx";
import {SettingsService} from "./settings.service";
import {HttpErrorHandlerService} from "./http-error-handler.service";
import {ICategory} from "../interfaces/ICategory";
import {HttpClient} from "@angular/common/http";
@Injectable()
export class CategoryService {
  constructor(public http: HttpClient, private settings:SettingsService,private err_handler:HttpErrorHandlerService) {}

  all():Observable<ICategory[]>{
    return this.http.get<ICategory[]>(this.settings.baseUrl + 'category/min')
      .catch(err => {
        return this.err_handler.handleError(err);
      });
  }
}
