import {Injectable} from '@angular/core';
import {Response, ResponseContentType} from "@angular/http";
import 'rxjs/add/operator/map';
import {Observable} from "rxjs/Rx";
import {SettingsService} from "./settings.service";
import {HttpErrorHandlerService} from "./http-error-handler.service";
import {IResponse} from "../interfaces/IResponse";
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {ErrorObservable} from "rxjs/observable/ErrorObservable";
import {IFileFetch} from "../interfaces/IFileFetch";

@Injectable()
export class FileHelperService{
  constructor(public http: HttpClient, private settings:SettingsService,private err_handler:HttpErrorHandlerService) {}
  private fetchFile(name: string):Observable<any>{
    return this.http.get(this.settings.baseUrl + 'file/view/' + name,{ responseType: "blob"})
      .catch(err => {
        return this.err_handler.handleError(err)
      })
  }
  getFile(name:string):Observable<IFileFetch>{
    return new Observable<IFileFetch>(observer => {
      this.fetchFile(name).subscribe(res =>{
        observer.next({url:URL.createObjectURL(res),blob: res});
      },err => {
        observer.error(err);
      });
    })
  }
}