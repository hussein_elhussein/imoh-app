import {IAttachment} from "../interfaces/IAttachment";
import {MediaObject, MediaService} from "./media.service";
import {HelpersService} from "./helpers.service";
import {AttachmentService} from "./attachment.service";
import {Injectable} from "@angular/core";
import {IPrivateMessage} from "../interfaces/IPrivateMessage";
import {PlayerService} from "./player.service";
import {File} from "@ionic-native/file";
import {Subject} from "rxjs/Subject";

@Injectable()
export class RecorderService{
  onStart: Subject<any>;
  onStop: Subject<any>;
  onTimeUpdate: Subject<any>;
  private audioFile: MediaObject;
  private record_path: any;
  private recordCanceled: boolean;
  private recordingTime: string;
  private recording: boolean;
  private timerInterval: any;
  constructor(
    private media:MediaService,
    private helpers:HelpersService,
    private att_ser:AttachmentService,
    private player: PlayerService,
    private natFile: File)
  {
    this.onStart = new Subject<any>();
    this.onStop = new Subject<any>();
    this.onTimeUpdate = new Subject<any>();
  }
  recordAudio(message:IPrivateMessage):void{
    //if it's currently recording, stop the recording
    if(this.audioFile){
      this.cancelAudioRec();
      this.recordCanceled = true;
    }
    const onError = (error) => {
      console.log('record err', error);
    };
    const onStatusUpdate = (status) => {
      if(status === 2){
        this.onStart.next(message);
        this.recording = true;
        this.startTimer();
      }
    };
    const onSuccess = () => {
      if(!this.recordCanceled){
        if(this.audioFile && this.recordingTime != "00:0"){
          this.audioFile.release();
          this.recording = false;
          this.stopTimer();
          this.player.getAudioDuration(this.record_path['full']).subscribe(duration => {
            this.audioFile = null;
            message.attachment.duration = duration;
            message.attachment.duration_humanized = this.recordingTime;
            this.recordingTime = '00:0';
            this.onStop.next(message);
          });
        }else{
          this.cancelAudioRec();
        }
      }
    };
    if(!this.recordCanceled){
      let ext = this.helpers.platform === 'ios'? 'm4a': 'mp3';
      const attachment = <IAttachment>{
        blob: null,
        blob_type: 'audio',
        blob_ext: ext,
        source_type: 3,
        finished: false,
        progress: 0,
        duration: 0,
        duration_humanized: "",
      };
      message.attachment = attachment;
      const path = this.att_ser.generatePath(message,true);
      message.attachment.local_path = path['full'];
      message.attachment.generated_path = path;
      this.record_path = path;
      this.audioFile = this.media.create(path['full']);
      this.audioFile.onStatusUpdate.subscribe(onStatusUpdate);
      this.audioFile.onSuccess.subscribe(onSuccess);
      this.audioFile.onError.subscribe(onError);
      this.audioFile.startRecord();
    }
  }
  stopAudRec():void{
    if(this.audioFile){
      this.audioFile.stopRecord();
    }
  }
  cancelAudioRec(ev = null):void{
    let doCancel = () => {
      let sub = this.audioFile.onSuccess.subscribe(() => {
        sub.unsubscribe();
        this.audioFile.release();
        this.recording = false;
        this.audioFile = null;
        console.log('record path:', this.record_path);
        this.natFile.removeFile(this.record_path['folder'],this.record_path['file'])
          .then(() => {
            console.log('file removed');
            this.recordCanceled = false;
          })
          .catch(err => () => {
            console.log('removing error:', err);
          });
      });
      this.recordCanceled = true;
      this.recordingTime = '00:00';
      this.audioFile.stopRecord();
      this.stopTimer();
      this.onStop.next();
      //remove the file if exist
    };
    if(ev && ev.center){
      //if dragging distance is enough then cancel
      var cancel = false;
      if(ev && ev.center.x <= 300 || ev.center.x >= 370){
        cancel = true;
      }else if(ev == null){
        cancel = true;
      }else{
        cancel = false;
      }
      if(cancel && this.audioFile){
        doCancel();
      }
    }else if(this.audioFile){
      doCancel();
    }
  }
  private startTimer():void{
    let secs = 0;
    this.timerInterval = setInterval(() => {
      secs +=1;
      this.recordingTime = this.player.msToTime(secs * 1000);
      this.onTimeUpdate.next(this.recordingTime);
    },1000);
  }
  private stopTimer(){
    clearInterval(this.timerInterval);
  }
}