import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {SettingsService} from "./settings.service";
import {Observable} from "rxjs/Observable";
import {IConfirmation} from "../interfaces/IConfirmation";
import {Storage} from "@ionic/storage";
import {IPagination} from "../interfaces/IPagination";

@Injectable()
export class  ConfirmationService {
  constructor(private http: HttpClient,
              private settings:SettingsService,
              private storage:Storage){}

  index():Observable<IPagination>{
    return this.http.get<IPagination>(this.settings.baseUrl + 'confirmation');
  }
  async saveLocal(confirmation: IConfirmation){
    let confs = <IConfirmation[]> await this.storage.get('confirmations').catch(console.log);
    if(!confs){
      confs = [];
    }
    confs.push(confirmation);
    return await this.storage.set('confirmations',confs).catch(console.log);
  }
  filter(profiles: Array<number>,content_id?: number,update_id?:number):Observable<IConfirmation[]>{
    return new Observable<IConfirmation[]>(observer => {
      (async ()=>{
        let confs = <IConfirmation[]> await this.storage.get('confirmations').catch(console.log);
        let found = null;
        if(confs){
          const found_saved = confs.filter(item => {
            let include;
            const passed_rules = [];
            let profiles_found = [];
            if(item.profiles && item.profiles.length){
              for(let target_prof of profiles){
                for(let profile of item.profiles){
                  if(profile.id === target_prof){
                    profiles_found.push(target_prof);
                  }
                }
              }
            }
            passed_rules['profiles'] = profiles_found.length === profiles.length;
            if(content_id){
              passed_rules['content_id'] = item.content_id === content_id;
            }else{
              passed_rules['content_id'] = true;
            }
            if(update_id){
              passed_rules['update_id'] = item.content_update_id === update_id;
            }else{
              passed_rules['update_id'] = true;
            }
            for(let passed of passed_rules){
              include = passed;
            }
            return include;
          });
          if(found_saved.length){
            found = found_saved[0];
          }
        }
        if(found){
          observer.next([found]);
        }else{
          const confs = await this.filterServer(profiles,content_id,update_id).toPromise().catch(console.log);
          if(confs){
            observer.next(confs);
            observer.complete();
          }else{
            observer.error(confs);
          }
        }
      })().catch(console.log);
    });
  }
  private filterServer(profiles: Array<number>,content_id?: number,update_id?:number){
    const filter = {
      profiles: profiles,
    };
    if(content_id){
      filter['content_id'] = content_id;
    }
    if(update_id){
      filter['content_update_id'] = update_id;
    }
    return this.http.post<IConfirmation[]>(this.settings.baseUrl + 'confirmation',filter);
  }
}