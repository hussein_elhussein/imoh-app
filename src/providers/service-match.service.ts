import {Injectable} from "@angular/core";
import {AuthService} from "./auth.service";
import {ContentService} from "./content.service";
import {Observable} from "rxjs/Observable";
import {IService} from "../interfaces/IService";
import {LocationHelperService} from "./location-helper.service";
import {Subject} from "rxjs/Subject";

@Injectable()
export class ServiceMatchService{
  constructor(
    private auth:AuthService,
    private ser:ContentService,
    private location_ser:LocationHelperService,
    ){}

    //todo: respect content update status
  match():Observable<IService[]>{
      return new Observable<IService[]>(observer => {
        this.nearbyServices().subscribe(services => {
          if(services && services.length){
            this.auth.getCurrentProfile().then(profile => {
              let matches: IService[] = [];
              //todo: get profile services:
              // for(let profile_ser of profile.services){
              //   for(let service of services){
              //     if(this.matchByType(profile_ser,service)){
              //       if(this.matchByCategory(profile_ser,service)){
              //         matches.push(service);
              //       }
              //     }
              //   }
              // }
              observer.next(matches);
            })
          }else{
            observer.complete();
          }
        }, err => {
          console.log(err);
        })
      });
  }
  private matchByType(a: IService, b: IService):boolean{
    return a.offered !== b.offered;
  }
  private matchByCategory(a: IService, b: IService):boolean{
    for(let a_cat of a.service_type.categories){
      for(let b_cat of b.service_type.categories){
        if(a_cat.id === b_cat.id){
          return true;
        }
      }
    }
    return false;
  }
  nearbyServices():Observable<IService[]>{
    let reqSub = new Subject();
    let last_address = null;
    return new Observable<IService[]>(observer => {
      this.location_ser.getLocation().subscribe(location => {
        let getSub = reqSub.subscribe((details: {city: boolean, address: string}) => {
          this.getServices(details.city,details.address).subscribe(res => {
            if(res && res.length){
              observer.next(res);
              getSub.unsubscribe();
            }else{
              console.log('last tried location: ', last_address);
              // try other locations:
              switch (last_address){
                case "sub_administrative_area":
                  last_address = "administrative_area";
                  console.log('trying location: ', last_address);
                  reqSub.next({city:true,address: location.administrative_area});
                  break;
                case "administrative_area":
                  last_address = "country";
                  console.log('trying location: ', last_address);
                  reqSub.next({city:false,address: location.country.name});
                  break;
                case "country":
                  console.log("we've tried all locations, giving up");
                  //we tried all locations, give up:
                  last_address = null;
                  observer.next();
                  getSub.unsubscribe();
                  break;
              }
            }
          });
        });
        last_address = "sub_administrative_area";
        console.log('trying location: ', last_address);
        reqSub.next({city: true,address: location.sub_administrative_area})
      },err => {
        observer.error(err);
      });
    })
  }
  getServices(city: boolean = true, address: string):Observable<IService[]>{
      return new Observable<IService[]>(observer => {
        //todo convert service to content:
        // if(city){
        //   this.ser.getContentByCity(address,
        //     false,
        //     true,
        //     false).subscribe((res: IService[]) => {
        //     observer.next(res);
        //   });
        // }else{
        //   this.ser.getContentByCountry(address,
        //     false,
        //     true,
        //     false).subscribe((res: IService[]) => {
        //     observer.next(res);
        //   });
        // }
      })
  }
}