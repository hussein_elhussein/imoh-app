import {Subject} from "rxjs/Subject";
import {Injectable} from "@angular/core";
declare var Media;
export class MediaObject{
  private mediaInstance: any;
  public onSuccess: Subject<any>;
  public onError: Subject<any>;
  public onStatusUpdate: Subject<any>;
  constructor(src: string){
    this.onSuccess = new Subject<any>();
    this.onError = new Subject<any>();
    this.onStatusUpdate = new Subject<any>();
    this.mediaInstance = new Media(
      src,
      () => {
        this.onSuccess.next();
      },error => {
        this.onError.next(error);
      },status => {
        this.onStatusUpdate.next(status);
      });
  }
  startRecord(){
    this.mediaInstance.startRecord();
  }
  release(){
    this.mediaInstance.release();
    this.mediaInstance = null;
  }
  stopRecord(){
    this.mediaInstance.stopRecord();
  }
  play(){
    this.mediaInstance.play();
  }
  pause(){
    this.mediaInstance.pause();
  }
  setVolume(volume: number){
    this.mediaInstance.setVolume(volume);
  }
  stop(){
    this.mediaInstance.stop();
  }
  getCurrentPosition(): Promise<number>{
    return new Promise<number>((resolve, reject) => {
      try {
        this.mediaInstance.getCurrentPosition(resolve,reject);
      }catch (e) {
        reject(e);
      }
    });
  }
  getDuration(): number{
    try {
      return this.mediaInstance.getDuration();
    }catch (e) {

    }
  }
}

@Injectable()
export class MediaService{
  constructor(){}
  create(src: string): MediaObject{
    return new MediaObject(src);
  }
}