import {EventEmitter, Injectable, NgZone} from '@angular/core';
import {SettingsService} from "./settings.service";
import * as io from 'socket.io-client';
import {AuthService} from "./auth.service";
import {HttpErrorHandlerService} from "./http-error-handler.service";
import {IPrivateMessage} from "../interfaces/IPrivateMessage";
import {Events} from "ionic-angular";
import {Subject} from "rxjs/Subject";
import {NotificationService} from "./notification.service";
import {INotification} from "../interfaces/INotification";
import {HelpersService} from "./helpers.service";
import {Storage} from "@ionic/storage";
import {IConversation} from "../interfaces/IConversation";
import {LocalConversationService} from "./local-conversation.service";
import {ConfirmationService} from "./confirmation.service";

@Injectable()
export class SocketService {
  socket:any;
  chat:any;
  zone:any;
  public onConversationOpened:Subject<IConversation>;
  public onConversationJoined:Subject<any>;
  public onConversationEntered:Subject<any>;
  public onConversationLeft:Subject<any>;
  public onMessage:Subject<IPrivateMessage>;
  public onNotification:Subject<any>;
  public onError:Subject<any>;
  entered_conv: number;
  constructor(
    private settings:SettingsService,
    private auth_ser:AuthService,
    private http_err: HttpErrorHandlerService,
    private events:Events,
    private not_ser:NotificationService,
    private helpers:HelpersService,
    private storage: Storage,
    private local_conv:LocalConversationService,
    private conf_ser:ConfirmationService,
  ) {
    this.onConversationOpened = new Subject<IConversation>();
    this.onConversationJoined = new Subject<any>();
    this.onConversationEntered = new Subject<any>();
    this.onConversationLeft = new Subject<any>();
    this.onMessage = new Subject<IPrivateMessage>();
    this.onNotification = new Subject<any>();
    this.onError = new Subject<any>();
  }
  connect():void {
    var token = this.auth_ser.getToken();
    let options = {
      transportOptions: {
        polling: {
          extraHeaders: {
            'Authorization': 'Bearer ' + token
          }
        }
      },
      reconnect: true,
    };
    this.socket = io.connect(this.settings.socketHost, options);
    this.zone = new NgZone({enableLongStackTrace: false});
    this.listenToEvents();
    // this.socket.on('error', (err) => {
    //   this.onError.emit(err);
    //   this.http_err.handleError(err).subscribe(res => {},err => {});
    // });
  }
  disconnect(){
    if(this.socket){
      this.socket.close();
    }
  }
  private listenToEvents(){
    this.socket.on("notification:new", (not: INotification) => {
      this.zone.run(() => {
        if(not.data.visible){
          this.not_ser.count += 1;
          not.created_at = this.helpers.unix();
          this.events.publish('notifications:count',this.not_ser.count);
          this.events.publish('notifications:new', not);
        }else{
          this.events.publish('invisible_notifications:new', not);
          console.log('received invisible notification: ', not);
         this.storeNotification(not);
        }
      });

    });
    this.socket.on('conversation:opened', (res) => {
      this.addNewConversation(res).catch(console.log);
      this.onConversationOpened.next(res);
    });
    this.socket.on('conversation:joined', (res) => {
      this.onConversationJoined.next(res);
    });
    this.socket.on('conversation:entered', (res) => {
      this.onConversationEntered.next(res);
    });
    this.socket.on('conversation:left', (res) => {
      this.onConversationLeft.next(res);
    });
    this.socket.on("im:new", (msg: IPrivateMessage) => {
      console.log('received message:', msg);
      if(msg.conversation_id !== this.entered_conv){
        this.local_conv.addNewMessage(msg).catch(console.log);
      }
      this.onMessage.next(msg);
    });
  }
  sendMessage(message:IPrivateMessage){
    this.socket.emit('im:new',message);
  }
  private async addNewConversation(conv:IConversation){
    const profile = await this.auth_ser.getCurrentProfile().catch(console.log);
    if(!profile){
      return;
    }
    if(profile.id !== conv.opener.id){
      const initConv = await this.local_conv.initConversation(conv.receivers,conv.opener).catch(console.log);
      if(!initConv){
        return;
      }
      if(!initConv.initialized){
        initConv.id = conv.id;
        initConv.socket_id = conv.socket_id;
        initConv.initialized = true;
        initConv.opened = true;
        initConv.viewers.push(initConv.opener);
        await this.local_conv.save(initConv).catch(console.log);
      }
    }
  }
  entered(conversation:IConversation, profile_id:number){
    this.entered_conv = conversation.id;
    let view = {
      id: conversation.id,
      profile_id: profile_id,
      socket_id: conversation.socket_id
    };
    this.socket.emit('conversation:entered',view);
  }
  left(conversation:IConversation, profile_id:number){
    this.entered_conv = 0;
    let view = {
      id: conversation.id,
      profile_id: profile_id,
      socket_id: conversation.socket_id
    };
    this.socket.emit('conversation:left',view);
  }
  private storeNotification(note:INotification){
    if(note.type === 'confirmation_created'){
      this.conf_ser.saveLocal(note.data.original).catch(console.log);
    }else{
      let stored_notes = [];
      this.storage.get('notifications').then((notifications: INotification[]) => {
        if(notifications){
          stored_notes = notifications;
        }
        stored_notes.push(note);
        this.storage.set('notifications',stored_notes).catch(console.log);
      }).catch(console.log);
    }
  }
}
